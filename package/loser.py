import subprocess
import os
import matplotlib.pyplot as plt
import numpy as np
import time

"""
loser should be called with the following arguments:

loser binfile grpfile FSPSdir H_0 Omega Lambda Boxsize use_absmag use_cosmic_extinction use_nebular redlaw mcode viewdir COLORMAP [xbottom ybottom Limage Npixels] > spectrum_file

 * binfile: 
   	       tipsy binary file  
   	       When compiled with -DGADGET, use gadget bin
       When compiled with -DHDF5, use hdf5 file
 * grpfile = SKID-format .grp file
 * FSPSdir = root directory for FSPS models
 * H_0 = Hubble const in km/s/Mpc
 * Omega = Matter density
 * Lambda = Cosmological constant in omega units
 * Boxsize = Box size in comoving h^-1 Mpc
 * use_absmag = 1/0 means do/don't use absolute magnitudes
 * use_cosmic_extinction = 1/0 is do/don't use Madau (1995) IGM attenuation
 * use_nebular = 1/0 means do/don't include nebular emission lines
 * redlaw = choice of reddening law; see extinction.c
 * mcode = choice of SSP libraries; see readised.c
 * viewdir = viewing direction, choose: x,y,z=1,2,3  -x,-y,-z=-1,-2,-3
 * COLORMAP = 1/0 means do/don't create a colormap for one galaxy
 Last 4 params are only when COLORMAP=1
 * xbottom,ybottom,zbottom = lower corner of image in system units (-0.5,0.5)
 * Limage = size of image in system units
 * Npixels = # of pixels in image
"""

def call_loser_subprocess(outfile, loser, binfile, grpfile, FSPSdir, H_0, Omega, Lambda, Boxsize, use_absmag, use_cosmic_extinction, 
				use_nebular, redlaw, mcode, viewdir, colormap, xbottom=0, ybottom=0, zbottom=0, Limage=0, Npixels=0):
	if colormap == '0':
		call_loser = [loser, binfile, grpfile, FSPSdir, H_0, Omega, Lambda, Boxsize, use_absmag, use_cosmic_extinction, 
				use_nebular, redlaw, mcode, viewdir, colormap, '>>', outfile]
	elif colormap == '1':
		call_loser = [loser, binfile, grpfile, FSPSdir, H_0, Omega, Lambda, Boxsize, use_absmag, use_cosmic_extinction, 
				use_nebular, redlaw, mcode, viewdir, colormap, xbottom, ybottom, zbottom, Limage, Npixels, '>>', outfile]
	else:
		raise ValueError('Colormap must be 0 or 1')
	with open(outfile, "w+") as f:
		subprocess.Popen(call_loser, stdout=f)
	return

def call_loser(outfile, loser, binfile, grpfile, FSPSdir, H_0, Omega, Lambda, Boxsize, use_absmag, use_cosmic_extinction, 
				use_nebular, redlaw, mcode, viewdir, colormap, xbottom=0, ybottom=0, zbottom=0, Limage=0, Npixels=0):
	if colormap == '0':
		call_loser = (loser + ' ' + binfile + ' ' + grpfile + ' ' + FSPSdir + ' ' + H_0 + ' ' + Omega + ' ' + Lambda + ' ' +  
					Boxsize + ' ' + use_absmag + ' ' + use_cosmic_extinction + ' ' + use_nebular + ' ' + redlaw + ' ' + 
					mcode + ' ' + viewdir + ' ' + colormap + ' ' + '>>' + ' ' + outfile)
	elif colormap == '1':
		call_loser = (loser + ' ' + binfile + ' ' + grpfile + ' ' + FSPSdir + ' ' + H_0 + ' ' + Omega + ' ' + Lambda + ' ' + 
					Boxsize + ' ' + use_absmag + ' ' + use_cosmic_extinction + ' ' + use_nebular + ' ' + redlaw + ' ' +  
					mcode + ' ' + viewdir + ' ' + colormap + ' ' + xbottom + ' ' + ybottom + ' ' + zbottom + ' ' +  
					Limage + ' ' + Npixels + ' ' + '>>' + ' ' + outfile)
	else:
		raise ValueError('Colormap must be 0 or 1')
	os.system(call_loser)

def save_as_fits(array, out_file_name):
	import astropy.io.fits as fits
	hdu = fits.PrimaryHDU(array)
	hdulist = fits.HDUList([hdu])
	hdulist.writeto(out_file_name)
	hdulist.close()
	return

# load in color data with loser file open line by line
def colormap_load(file, color, Npixels):
	i = 0
	store = np.zeros((Npixels**2))
	with open(file) as infile:
		for line in infile:
			if not line.startswith("#"):
				results = line.split(' ')
				store[i] = map(float, results[:-1])[11+color]
				i +=1
	return np.array(np.split(store, Npixels))

def pandas_load(file, color, Npixels):
	table = pandas.read_table(out_file, comment='#', delim_whitespace=True, header=None)
	loser_data = np.array(table)
	return color_array(loser_data, color, Npixels)

def image_array(loser_data, color, Npixels):
	return np.array(np.split(loser_data[:, 11+color], int(Npixels)))

def colors_load(file, color, Npixels, colormap):
	loser_data = np.loadtxt(file)
	if colormap:
		return image_array(loser_data, color, Npixels)
	else:
		return loser_data[:, 11+color]

def save_loser_image(color_data, out_file_name):
	plt.imshow(color_data, cmap='magma_r', vmax=-0.1)
	plt.colorbar()
	plt.savefig(out_file_name)
	plt.clf()	

def run_loser(out_file, params, pos, Limage, ang_bins, color, image_name):
	if not os.path.exists(out_file):
				call_loser(out_file, params['loser_dir'], data_dir+params['snap'], params['grp'], params['fsps_dir'], 
							params['H_0'], params['omega_matter'], params['omega_lambda'], params['box'],
							params['use_absmag'], params['use_cosmic_extinction'], params['use_nebular'], 
							params['redlaw'], params['mcode'], params['j'], params['COLORMAP'], 
							xbottom=str(pos[0]), ybottom=str(pos[1]), zbottom=str(pos[2]), Limage=str(Limage), Npixels=str(len(ang_bins)))
			
	color_data = colormap_load(out_file, color, len(ang_bins))
	save_loser_image(color_data, image_name)
	return 