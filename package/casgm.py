import numpy as np 
from astropy.convolution import convolve, Gaussian2DKernel
from util import create_mask, masked_data, new_centers
from scipy.interpolate import interp1d

class Casgm(object):

	def __init__(self, array, rbins, center):
		self.array = array
		self.rbins = rbins
		self.center = center

	# C, A, S according to arXiv:1107.6045
	# G and M20 according to arXiv:astro-ph/0311352

	def petrosian_eta(self):
		q_in_r = np.zeros(len(self.rbins))
		q_at_r = np.zeros(len(self.rbins))
		q_tot_in_r = np.zeros(len(self.rbins))
		for i in range(len(self.rbins)):
			r = self.rbins[i]
			q_tot_in_r[i], q_in_r[i] = masked_data(self.array, self.center, r, 'circle')
			q_at_r[i] = masked_data(self.array, self.center, r, 'ring', threshold=0.5)[1]
		self.eta = q_at_r / q_in_r
		self.tot_at_r = q_tot_in_r
		return

	# find the petrosian eta, radius and total quantity at each radius
	def petrosian_r(self):
		self.petrosian_eta()
		f = interp1d(self.rbins, self.eta)
		r_new = np.arange(self.rbins[0], self.rbins[-1], 0.1)
		eta_new = f(r_new)
		self.r_eta = r_new[np.argmin(np.abs(eta_new - 0.2))]
		return

	def segmentation(self):
		sigma = int(0.2 * self.r_eta)
		kernel = Gaussian2DKernel(stddev=sigma)
		return convolve(self.array, kernel)

	def fraction_radius(self, frac):
		# needs to be total up to r_tot
		tot = np.sum(self.array)
		frac_q = self.tot_at_r / tot
		f = interp1d(self.rbins, frac_q)
		r_new = np.arange(self.rbins[0], self.rbins[-1], 0.1)
		frac_new = f(r_new)
		r_frac = float(r_new[np.argmin(np.abs(frac_new - frac))])
		return r_frac

	def concentration(self):
		r_20 = self.fraction_radius(0.2)
		r_80 = self.fraction_radius(0.8)
		ratio = r_80/r_20
		return  5* np.log10(ratio)

	def assymetry_min(self, axis=0):
		centers = new_centers(self.array.shape[0], self.array.shape[1], 2)
		assym_array = np.zeros(len(centers))
		for i in range(len(centers)):
			padx = [self.array.shape[1] - centers[i][0], centers[i][0]]
			pady = [self.array.shape[0] - centers[i][1], centers[i][1]]
			padded = np.pad(self.array, [pady, padx], 'constant')
			center_use = [(padded.shape[0]/2 -1), (padded.shape[1]/2 -1)]
			data = masked_data(padded, center_use, self.r_eta*1.5, 'circle', masked_only=True)
			flip = np.flip(data, axis)
			residual = data - flip
			assym_array[i] = (np.sum(np.abs(residual)))/(2*np.sum(np.abs(flip)))
		index = np.argmin(assym_array)
		self.center = centers[index]
		return assym_array[index]

	def assymetry(self, axis=0):
		data = masked_data(self.array, self.center, self.r_eta*1.5, 'circle', masked_only=True)
		flip = np.flip(data, axis)
		residual = data - flip
		return (np.sum(np.abs(residual)))/(2*np.sum(np.abs(flip)))

	def clumpiness(self):
		sigma = int(0.25 * self.r_eta)
		kernel = Gaussian2DKernel(stddev=sigma)
		conv = convolve(self.array, kernel)
		mask = create_mask(self.array.shape[0], self.array.shape[1], 'remove_circle', center=self.center, 
							radius=self.r_tot, remove_lower=(self.r_tot/5))
		data = self.array*mask; conv *= mask
		return np.sum(np.abs(data - conv))/np.sum(np.abs(data))

	def gini(self):
		conv = self.segmentation()
		m_eta_sum, m_eta_avg = masked_data(conv, self.center, self.r_eta, 'ring', threshold=0.5)
		mask = (abs(self.array) >= abs(m_eta_avg))
		data = mask*self.array
		pixels = data[np.where(data != 0)]
		pixels = pixels[np.argsort(np.abs(pixels))]
		mean_q = np.mean(pixels)
		n = len(pixels)
		g_sum = 0.
		for i in range(n):
			g_sum += (2*i - n -1) * pixels[i]
		return g_sum/(mean_q*n*(n-1))

	def m20(self):
		# find the center which minimises m_tot:
		w, h = self.array.shape
		y, x = np.ogrid[:h, :w]
		conv = self.segmentation()
		centers = new_centers(h, w, 2)
		m_tot_array = np.zeros(len(centers))
		for i in range(len(centers)):
			c = centers[i]                                                                                                                   
			m_i = conv*((x - c[0])**2 + (y-c[1])**2)
			m_tot_array[i] = np.sum(m_i)
		index = np.argmin(np.abs(m_tot_array))
		center = centers[index]
		m_tot = np.abs(m_tot_array[index])

		order = np.flip(np.argsort(np.abs(conv), axis=None), axis=0)
		order = np.transpose(np.unravel_index(order, conv.shape))

		f_lim = 0.2*np.sum(np.abs(conv))
		f_sum = 0
		i = 0
		m_sum = 0
		while f_sum < f_lim:
			place = order[i]
			f = abs(conv[place[0]][place[1]])
			if (f_sum + f > f_lim):
				break
			m_sum += f*((place[0] - center[0])**2 + (place[1]-center[1])**2)
			f_sum += f
			i += 1
		return np.log10(m_sum/m_tot)

	def find_casgm(self):
		# first guess at petrosian radius using image center as galaxy center
		self.petrosian_r()
		# running the assymetry gets a new center for minimised assymetry
		self.ass = self.assymetry_min()
		self.petrosian_r()
		self.r_tot = int(self.r_eta * 1.5)
		if self.r_eta >= 5.:
			self.half_r = self.fraction_radius(0.5)
			self.conc = self.concentration()
			self.clump = self.clumpiness()
			self.gini = self.gini()
			self.m20 = self.m20()