import caesar

class Caesar_utils():

	def __init__(self, caesar_file):
		self.caesar_file = caesar_file

	def load_caesar(self, ds=None, grp=None):
		obj = caesar.load(caesar_file)
		if ds: obj.yt_dataset=ds
		if grp: obj.member_search(skid_particle_file=grp)
		return obj
