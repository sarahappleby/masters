import numpy as np
 
def octants(pos_array, rad_array, boxsize):

	pos_x = (pos_array[:, 0] < boxsize*0.5); pos_y = (pos_array[:, 1] < boxsize*0.5); pos_z = (pos_array[:, 2] < boxsize*0.5)

	pos_1 = []; pos_2 = []; pos_3 = []; pos_4 = []; pos_5 = []; pos_6 = []; pos_7 = []; pos_8 = []
	rad_1 = []; rad_2 = []; rad_3 = []; rad_4 = []; rad_5 = []; rad_6 = []; rad_7 = []; rad_8 = []

	for i in range(len(pos_array)):
		if (pos_x[i] and pos_y[i] and pos_z[i]):
			pos_1.append(pos_array[i])
			rad_1.append(rad_array[i])
		elif (pos_x[i] and pos_y[i] and not pos_z[i]):
			pos_5.append(pos_array[i])
			rad_5.append(rad_array[i])
		elif (pos_x[i] and not pos_y[i] and pos_z[i]):
			pos_3.append(pos_array[i])
			rad_3.append(rad_array[i])
		elif (pos_x[i] and not pos_y[i] and not pos_z[i]):
			pos_7.append(pos_array[i])
			rad_7.append(rad_array[i])
		elif (not pos_x[i] and pos_y[i] and pos_z[i]):
			pos_2.append(pos_array[i])
			rad_2.append(rad_array[i])
		elif (not pos_x[i] and pos_y[i] and not pos_z[i]):
			pos_6.append(pos_array[i])
			rad_6.append(rad_array[i])
		elif (not pos_x[i] and not pos_y[i] and pos_z[i]):
			pos_4.append(pos_array[i])
			rad_4.append(rad_array[i])
		elif (not pos_x[i] and not pos_y[i] and not pos_z[i]):
			pos_8.append(pos_array[i])
			rad_8.append(rad_array[i])

	positions = np.array((np.array(pos_1), np.array(pos_2), np.array(pos_3), np.array(pos_4), 
							np.array(pos_5), np.array(pos_6), np.array(pos_7), np.array(pos_8)))
	radii = np.array((np.array(rad_1), np.array(rad_2), np.array(rad_3), np.array(rad_4), 
								np.array(rad_5), np.array(rad_6), np.array(rad_7), np.array(rad_8)))

	return positions, radii	

def variance_jk(samples, mean):
	n = len(samples)
	factor = (n-1.)/n
	x = np.nansum((np.subtract(samples, mean))**2, axis=0)
	x *= factor
	return x