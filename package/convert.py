import numpy as np 
import math

def rad_to_arcsec(theta):
	return theta *648000 / math.pi

def arcsec_to_rad(theta):
	return theta * math.pi / 648000

def theta_to_sa(theta):
	# needs to be in radians
	return 2*math.pi*(1 - math.cos(theta)) 

def magab_to_f(array):
	mab0 = 3631e-26
	tens = np.ones_like(array)*10
	power = -0.4*array
	flux = np.power(tens, power)
	mask = flux != 1
	return mask*flux*mab0

def magab_to_l(array):
	mab0 = 3631e-26
	pc = 3.085e16
	const = 4*math.pi*((pc*10)**2)*mab0
	tens = np.ones_like(array)*10
	power = -0.4*array
	lum = np.power(tens, power)
	mask = lum != 1
	return const*mask*lum

def magab_to_i(array, theta):
	flux = magab_to_f(array)
	sa = theta_to_sa(theta)
	return flux / sa

def magab_to_intens(r, z, H_0, color_data):
	theta = r_to_theta(r, z, H_0)
	sa = theta_to_sa(theta)
	flux = magab_to_f(color_data)
	return flux / sa

def angular_distance(z, H_0):
	c = 2.997e8
	if z == 0:
		d = 150*1000 # in kpc
	else:
		frac = 1/(1+z)
		# gives d in kpc
		d = (1 - frac**0.5)*frac*2*c/H_0
	return d

def r_to_theta(r, z, H_0):
	d = angular_distance(z, H_0)
	return r / d

def theta_to_r(theta, z, H_0):
	d = angular_distance(z, H_0)
	return theta * d

def theta_bins(r, z, dtheta, H_0):
	theta = r_to_theta(r, z, H_0)
	theta = rad_to_arcsec(theta)
	return np.arange(0, theta, dtheta)


