import numpy as np 
from photutils import CircularAperture, CircularAnnulus, aperture_photometry

def circular_aperture(array, center, r):
	n = np.pi*r**2
	aperture = CircularAperture(center, r=r)
	phot = aperture_photometry(array, aperture, method='exact')
	aperture_sum = phot['aperture_sum']
	aperture_avg = aperture_sum / n
	return aperture_sum, aperture_avg

def ring_aperture(array, center, r, thresh=0.2):
	lower = r - thresh; upper = r + thresh
	n = np.pi*(upper**2 - lower**2)
	aperture = CircularAnnulus(center, r_in=lower, r_out=upper)
	phot = aperture_photometry(array, aperture, method='exact')
	aperture_sum = phot['aperture_sum']
	aperture_avg = aperture_sum / n
	return aperture_sum, aperture_avg

def create_mask(w, h, method, center=None, radius=None, remove_lower=None, threshold=None):
	if center is None:
		center = [int(w/2), int(h/2)]
	if radius is None:
		radius = min(center[0], center[1], w-center[0], h-center[1])
	y, x = np.ogrid[:h, :w]
	dist = np.sqrt((x - center[0])**2 + (y-center[1])**2)
	if method == 'circle':
		mask = (dist <= radius)
	elif method == 'ring':
		if threshold:
			mask = (dist <= radius + threshold) * (dist >= radius - threshold)
		else:
			mask = (dist == radius)
	elif method == 'remove_circle':
		mask = (dist <= radius) * (dist >= remove_lower)
	return mask

# if masked_only, return masked data
# else, return sum and average inside mask
def masked_data(array, center, r, method, masked_only=False, threshold=None):
	Npixels = array.shape
	if threshold:
		mask = create_mask(Npixels[0], Npixels[1], method, center=center, radius=r, threshold=threshold)
	else:
		mask = create_mask(Npixels[0], Npixels[1], method, center=center, radius=r)
	data = mask*array
	if masked_only:
		return data
	else:
		N_mask = len(np.where(mask == 1)[0])
		masked_sum = np.sum(data)
		masked_average = masked_sum / N_mask
		return masked_sum, masked_average

def new_centers(h, w, size):
	# size : the number of points either side of center to consider
	x = np.arange(0, w, 1); y = np.arange(0, h, 1)
	center = [int(w*0.5 -1), int(h*0.5 -1)]
	a = x[center[0]-size : center[0]+size +1]
	b = y[center[1]-size : center[1]+size +1]
	n = a.size*b.size
	out = np.zeros([n, 2])
	out[:,0] = np.repeat(a, a.size)
	out[:,1] = np.tile(b, b.size)
	return out.astype(int)

# new method that gives centers around a given center