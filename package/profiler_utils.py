import os
from profiler.profiler import *
from util import create_mask, masked_data, ring_aperture
import numpy as np

# changes param file
def change_input(param_file, param, change_input):
	with open(param_file,'r') as f:
		filedata = f.readlines()
	with open(param_file,'w') as f:
		for line in filedata:
			if param in line:
				line = line.replace(line, change_input)
				f.write(line)
			else: f.write(line)

# give dictionary of params and changes
def change_data(param_file, changes):
	for change in changes:
		change_input(param_file, change, changes[change])

def call_profiler(param_file, sersic=True, corsic=False, exp=False, 
				gaussian=False, psf=False, ferrer=False, td=False, id=False):
	profiler_path = '/home/sarah/masters/profiler/profiler.py'
	command = (profiler_path + ' --params=' + param_file + ' --sersic=' +str(sersic)
				+ ' --corsic=' + str(corsic) + ' --exponential=' + str(exp)+ ' --gaussian'
				+ str(gaussian) + ' --psf=' + str(psf) + ' --ferrer=' + str(ferrer)
				+ ' --tdisk=' + str(td) + ' --idisk=' + str(id))
	os.system(command)

def get_profile(array, rbins, center):
	profile = np.zeros(len(rbins))
	for i in range(len(rbins)):
		profile[i] = masked_data(array, center, rbins[i], 'ring', threshold=1.0)[1]
	return profile

def get_profile_phot(array, rbins, center):
	profile = np.zeros(len(rbins))
	for i in range(len(rbins)):
		profile[i] = ring_aperture(array, center, rbins[i])[1]
	return profile

def sersic_func(r, L_0, a, n):
	exponent = (r/a)**(1./n)
	return L_0*np.exp(-1*exponent)