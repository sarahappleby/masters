import yt, caesar
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axes_grid1 import AxesGrid

def axis_projection(ds, ax, field, center, radius, title, save=False, save_dir=None, weight_field='density', vel_arrows=False):
	plot = yt.ProjectionPlot(ds, ax, field, weight_field=weight_field, center=center)
	if vel_arrows:
		plot.annotate_velocity()
	if save:
		plot.set_width(int(radius*2), 'kpc')
		plot.set_unit('particle_mass', 'Msun')
		plot.annotate_title(title)
		plot.save(save_dir)
	return plot

def off_axis_projection(ds, ax, field, center, radius, title, save=False, save_dir=None, north_vector=None, weight_field='density'):
	# annotate velocities doesnt work for off axis projections
	plot = yt.OffAxisProjectionPlot(ds, ax, field, weight_field='density', center=center, north_vector=north_vector)
	if save:
		plot.set_width(int(radius*2), 'kpc')
		plot.set_unit('particle_mass', 'Msun')
		plot.annotate_title(title)
		plot.save(save_dir)
	return plot

def particle_plot(ds, field_x, field_y, field_z, center, radius, title, save=False, save_dir=None):
	plot = yt.ParticlePlot(ds, field_x, field_y, field_z, center=center)
	if save:
		plot.set_width(int(radius*2), 'kpc')
		plot.set_unit('particle_mass', 'Msun')
		plot.annotate_title(title)
		plot.save(save_dir)
	return plot

def add_to_plot(fig, p, grid, field, j, radius):
	p.set_width(2*radius)
	plot = p.plots[field]
	plot.figure = fig
	plot.axes = grid[j].axes
	plot.cax = grid.cbar_axes[j]
	p._setup_plots()
	return

def multi_panel_plot(images, labels, radius, save_dir):
	fig = plt.figure()
	grid = AxesGrid(fig, (0.075,0.075,0.85,0.85),
                nrows_ncols = (2, 2),
                axes_pad = 1.0,
                label_mode = "all",
                share_all = True,
                cbar_location="right",
                cbar_mode="edge",
                cbar_size="5%",
                cbar_pad="0%")
	for i in range(len(images)):
		add_to_plot(fig, images[i], grid, labels[i], i, radius)
	plt.savefig(save_dir)
	plt.clf()

