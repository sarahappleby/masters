# 2017.11.18 16:19:40 GMT
#Embedded file name: /Users/bciambur/mega/profiler_alpha/components.py
from scipy.special import gammainc, k1
from math import cosh
import numpy as np

def gammadif(a, x):
    out = gammainc(a, x) - 0.5
    return out


def get_bn(n):
    precision = 1e-06
    a = 2.0 * n
    bn = 2.0 * n - 0.333
    if bn < 0:
        bn = 0.0
    d = gammadif(a, bn)
    inc = 1.0
    while abs(d) > precision:
        d = gammadif(a, bn)
        bnplus = bn + inc
        bnminus = bn - inc
        if bnminus < 0:
            bnminus = 0.0
        dplus = gammadif(a, bnplus)
        dminus = gammadif(a, bnminus)
        if abs(dplus) < precision:
            bn = bnplus
            break
        elif abs(dminus) < precision:
            bn = bnminus
            break
        else:
            if abs(dplus) < abs(d):
                bn = bnplus
            elif abs(dminus) < abs(d):
                bn = bnminus
            inc = inc / 2.0

    return bn


def Sersic(x, mu_e, r_e, n_Ser):
    b_n = get_bn(n_Ser)
    x = np.array(x)
    return -1.0 * (mu_e + 1.0857362 * b_n * ((x / r_e) ** (1.0 / n_Ser) - 1))


def Corsic(x, mu_pr, al, ga, r_b, r_e, n_Ser):
    b_n = get_bn(n_Ser)
    x = np.array(x)
    if x[0] == 0.0:
        x[0] = 1e-10
    y = -1.0 * (mu_pr - 2.5 * ga / al * np.log10(1.0 + (r_b / x) ** al) + 1.0857362 * b_n * ((x ** al + r_b ** al) / r_e ** al) ** (1 / (n_Ser * al)))
    y[0] = -1.0 * (mu_pr - 2.5 * ga / al * np.log10(1.0 + (r_b / (x[1] / 10.0)) ** al) + 1.0857362 * b_n * (((x[1] / 10.0) ** al + r_b ** al) / r_e ** al) ** (1 / (n_Ser * al)))
    return y


def exponential(x, mu_0, h):
    x = np.array(x)
    return -1.0 * (mu_0 + 1.0857362 * (x / h))


def Gauss_ring(x, mu_0r, r0r, fwhm_r):
    x = np.array(x)
    return -1.0 * (mu_0r + 1.0857362 * ((x - r0r) ** 2 / (2.0 * (fwhm_r / 2.355) ** 2)))


def Gauss_nuc(x, mu_0ng, fwhm_ng):
    x = np.array(x)
    return -1.0 * (mu_0ng + 1.0857362 * (x ** 2 / (2.0 * (fwhm_ng / 2.355) ** 2)))


def Vector_nuc(mu_0n, PSF_func):
    return -1.0 * (mu_0n - 2.5 * np.log10(PSF_func))


def Moffat_nuc(x, mu_0nm, fwhm_nm, alpha, beta):
    x = np.array(x)
    y = (1.0 + (x / alpha) ** 2.0) ** (-1.0 * beta)
    y = y / max(y)
    return -1.0 * (mu_0nm - 2.5 * np.log10(y))


def Ferrer(x, mu_0f, r_out, alpha_F, beta_F):
    sz = len(x)
    x = np.array(x)
    fprof = [0.0] * sz
    for i in range(0, sz):
        if x[i] >= r_out:
            fprof[i] = -99.0
        else:
            fprof = -1.0 * (mu_0f - alpha_F * np.log10(1.0 - (x / r_out) ** (2.0 - beta_F)))

    return fprof


def id(x, mu_0z, z0, case):
    x = np.array(x)
    s = np.array([0.0] * len(x))
    for i in range(0, len(x)):
        if case == 'r0':
            s[i] = -1.0 * (mu_0z - 5.0 * np.log10(1.0 / cosh(x[i] / z0)))
        if case == 'z0':
            s[i] = -1.0 * (mu_0z - 2.5 * np.log10(x[i] / z0 * k1(x[i] / z0)))

    if x[0] == 0.0:
        s[0] = -1.0 * mu_0z
    return s


def td(x, mu_0, r_b, h1, h2):
    x = np.array(x)
    mu_b = mu_0 + 1.0857362 * (r_b / h1)
    i = len(x) - 1
    y = mu_0 + 1.0857362 * (x / h1)
    while x[i] > r_b:
        y[i] = mu_b + 1.0857362 * ((x[i] - r_b) / h2)
        i = i - 1

    return -1.0 * y