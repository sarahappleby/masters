from matplotlib import pyplot as plt
from matplotlib.ticker import FuncFormatter
from lmfit import Parameters, minimize, fit_report, Parameter
from matplotlib import rc
import construct_model
from numpy import random, linspace, sqrt, genfromtxt, log10, vstack, array, linspace
import total_magnitude
import add_component
import datetime
from pylab import rcParams
from indices import datamin, datamax, fitmin, fitmax
from scipy.interpolate import interp1d
import integrator
from format_logax import log_axis
import argparse
import ConfigParser

def readEllipseOutput(data):
	data = open(data)
	datatab = genfromtxt(data, dtype={'names': ('sma', 'intens', 'intens_err', 'pix_var', 'rms', 'ellip', 'ellip_err', 'PA', 'PA_err', 'X0', 'X0_ERR', 'Y0', 'Y0_ERR', 'GRAD', 'GRAD_ERR', 'GRAD_R_ERR', 'RSMA', 'MAG', 'MAG_LERR', 'MAG_UERR', 'TFLUX_E', 'TFLUX_C', 'TMAG_E', 'TMAG_C', 'NPIX_E', 'NPIX_C', 'A3', 'A3_ERR', 'B3', 'B3_ERR', 'A4', 'A4_ERR', 'B4', 'B4_ERR'),
	'formats': ('f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4')}, skiprows=0, missing_values=('INDEF'), filling_values=0.0)
	data.close()
	return datatab

def readEllipseOutput3col(data):
	data = open(data)
	datatab = genfromtxt(data, dtype={'names': ('sma', 'intens', 'ellip'),'formats': ('i4', 'f4', 'f4')}, missing_values=('INDEF'), filling_values=0.0)
	data.close()
	return datatab
	
def read2cols(data):
	data = open(data)
	datatab = genfromtxt(data, dtype={'names': ('sma', 'intens'),'formats': ('f4', 'f4')}, missing_values=('INDEF'), filling_values=0.0)[1:]
	data.close()
	return datatab

def readPSFFile(data):
	data = open(data)
	datatab = genfromtxt(data, dtype={'names': ('sma', 'intens'),'formats': ('f4', 'f4')}, missing_values=('INDEF'), filling_values=0.0)
	data.close()
	return datatab
	
def readtab_ellipsi(data):
	data = open(data)
	datatab2 = genfromtxt(data, dtype={'names': ('sma', 'intens', 'intens_err', 'pix_var', 'rms', 'ellip',
	 											'ellip_err', 'PA', 'PA_err', 'X0', 'X0_ERR', 'Y0', 'Y0_ERR',
	 											'GRAD', 'GRAD_ERR', 'GRAD_R_ERR', 'RSMA', 'MAG', 'MAG_LERR', 
	 											'MAG_UERR', 'TFLUX_E', 'TFLUX_C', 'TMAG_E', 'TMAG_C', 'NPIX_E', 
	 											'NPIX_C', 'A3', 'A3_ERR', 'B3', 'B3_ERR', 'A4', 'A4_ERR', 'B4', 
	 											'B4_ERR', 'NDATA', 'NFLAG', 'NITER', 'STOP', 'A_BIG', 'SAREA', 
	 											'AI2', 'AI2_ERR', 'BI2', 'BI2_ERR', 'AI3', 'AI3_ERR',
	 											'BI3', 'BI3_ERR', 'AI4', 'AI4_ERR', 'BI4', 'BI4_ERR'),
									  'formats': ('f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 
									              'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 
									              'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 
									              'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4',
									              'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4',
									              'f4', 'f4')}, 
									  skiprows=0, missing_values=('INDEF'), filling_values=0.0)
	data.close()
	return datatab2
	
def readtab_ellipsi_ext(data):
	data = open(data)
	datatab2 = genfromtxt(data, dtype={'names': ('sma', 'intens', 'intens_err', 'pix_var', 'rms', 'ellip',
	 											'ellip_err', 'PA', 'PA_err', 'X0', 'X0_ERR', 'Y0', 'Y0_ERR',
	 											'GRAD', 'GRAD_ERR', 'GRAD_R_ERR', 'RSMA', 'MAG', 'MAG_LERR', 
	 											'MAG_UERR', 'TFLUX_E', 'TFLUX_C', 'TMAG_E', 'TMAG_C', 'NPIX_E', 
	 											'NPIX_C', 'A3', 'A3_ERR', 'B3', 'B3_ERR', 'A4', 'A4_ERR', 'B4', 
	 											'B4_ERR', 'NDATA', 'NFLAG', 'NITER', 'STOP', 'A_BIG', 'SAREA', 
	 											'AI2', 'AI2_ERR', 'BI2', 'BI2_ERR', 'AI3', 'AI3_ERR',
	 											'BI3', 'BI3_ERR', 'AI4', 'AI4_ERR', 'BI4', 'BI4_ERR', 'AI8',
	 											'AI8_ERR', 'BI8', 'BI8_ERR', 'AI10', 'AI10_ERR', 'BI10',
	 											'BI10_ERR', 'AI12', 'AI12_ERR', 'BI12', 'BI12_ERR'),
									  'formats': ('f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 
									              'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 
									              'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 
									              'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4',
									              'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4',
									              'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4', 'f4',
									              'f4', 'f4', 'f4', 'f4')}, 
									  skiprows=0, missing_values=('INDEF'), filling_values=0.0)
	data.close()
	return datatab2

def fitcomm(sersic_entry, exp_entry, id_entry, gauss_entry, psf_entry, ferrer_entry, corsic_entry, td_entry):
	global ns_a,ne_a,ng_a,np_a,nf_a,nc_a,nid_a,ntd_a
	ns_a=0
	ne_a=0
	ng_a=0
	np_a=0
	nf_a=0
	nc_a=0
	nid_a=0
	ntd_a=0

	fit_parameters = Parameters()

	if (ns == 1):

		if (sersic_entry[0] == 1): 
			add_component.add_s(ns-1,sersic_entry[1],sersic_entry[2],sersic_entry[3],sersic_entry[5],sersic_entry[6],sersic_entry[7],
								fit_parameters)
			ns_a=ns_a+1
	"""
	else: 
		for i in range(0,ns):
			if (sersic_entry[i,0] == 1): 
				add_component.add_s(i,sersic_entry[i,1],sersic_entry[i,2],sersic_entry[i,3],sersic_entry[i,5],sersic_entry[i,6],
									sersic_entry[i,7],fit_parameters)
				ns_a = ns_a + 1
	"""		
	if (nc == 1):

		if (corsic_entry[0] == 1): 
			add_component.add_c(nc-1,corsic_entry[1],corsic_entry[2],corsic_entry[3],corsic_entry[4],corsic_entry[5],corsic_entry[6],
								corsic_entry[8],corsic_entry[9],corsic_entry[10],corsic_entry[11],corsic_entry[12],corsic_entry[13],
								fit_parameters)
			nc_a=nc_a+1	
	else: 
		for i in range(0,nc):
			if (corsic_entry[i,0] == 1): 
				add_component.add_c(i,corsic_entry[i,1],corsic_entry[i,2],corsic_entry[i,3],corsic_entry[i,4],corsic_entry[i,5],
									corsic_entry[i,6],corsic_entry[i,8],corsic_entry[i,9],corsic_entry[i,10],corsic_entry[i,11],
									corsic_entry[i,12],corsic_entry[i,13],fit_parameters)
				nc_a = nc_a + 1

	if (ne == 1):

		if (exp_entry[0] == 1): 
			add_component.add_e(ne-1,exp_entry[1],exp_entry[2],exp_entry[4], exp_entry[5],fit_parameters)
			ne_a=ne_a+1
	else: 
		for i in range(0,ne):
			if (exp_entry[i,0] == 1): 
				add_component.add_e(i,exp_entry[i,1],exp_entry[i,2],exp_entry[i,4], exp_entry[i,5],fit_parameters)
				ne_a=ne_a+1
				
	if (nid == 1):

		if (id_entry[0] == 1): 
			add_component.add_id(nid-1,id_entry[1],id_entry[2],id_entry[4],id_entry[5],fit_parameters)
			nid_a=nid_a+1
	else: 
		for i in range(0,nid):
			if (id_entry[i,0] == 1): 
				add_component.add_id(i,id_entry[i,1],id_entry[i,2],id_entry[i,4],id_entry[i,5],fit_parameters)
				nid_a=nid_a+1
	
	if (ng == 1):

		if (gauss_entry[0] == 1): 
			add_component.add_g(ng-1,gauss_entry[1],gauss_entry[2],gauss_entry[3],gauss_entry[5],gauss_entry[6],gauss_entry[7],
								fit_parameters)
			ng_a=ng_a+1
	else: 
		for i in range(0,ng):
			if (gauss_entry[i,0] == 1): 
				add_component.add_g(i,gauss_entry[i,1],gauss_entry[i,2],gauss_entry[i,3],gauss_entry[i,5],gauss_entry[i,6],
									gauss_entry[i,7],fit_parameters)
				ng_a=ng_a+1
			
	if (np == 1):

		if (psf_entry[0] == 1): 
			add_component.add_p(np-1,psf_entry[1],psf_entry[3],fit_parameters)
			np_a=np_a+1
	else: 
		for i in range(0,np):
			if (psf_entry[i,0] == 1): 
				add_component.add_p(i,psf_entry[i,1],psf_entry[i,3],fit_parameters)
				np_a=np_a+1
			
	if (nf == 1):

		if (ferrer_entry[0] == 1): 
			add_component.add_f(nf-1,ferrer_entry[1],ferrer_entry[2],ferrer_entry[3],ferrer_entry[4],ferrer_entry[6],ferrer_entry[7],
								ferrer_entry[8],ferrer_entry[9],fit_parameters)
			nf_a=nf_a+1
	else: 
		for i in range(0,nf):
			if (ferrer_entry[i,0] == 1): 
				add_component.add_f(i,ferrer_entry[i,1],ferrer_entry[i,2],ferrer_entry[i,3],ferrer_entry[i,4],ferrer_entry[i,6],
									ferrer_entry[i,7],ferrer_entry[i,8],ferrer_entry[i,9],fit_parameters)
				nf_a=nf_a+1
				
	if (ntd == 1):

		if (td_entry[0] == 1): 
			add_component.add_td(ntd-1,td_entry[1],td_entry[2],td_entry[3],td_entry[4],td_entry[6],td_entry[7],td_entry[8],
								td_entry[9],fit_parameters)
			ntd_a=ntd_a+1
	else: 
		for i in range(0,ntd):
			if (td_entry[i,0] == 1): 
				add_component.add_td(i,td_entry[i,1],td_entry[i,2],td_entry[i,3],td_entry[i,4],td_entry[i,6],td_entry[i,7],td_entry[i,8],
									td_entry[i,9],fit_parameters)
				ntd_a=ntd_a+1

	ellipticity = float(config.get('basic', 'ellipticity'))
	axis = config.get('basic', 'axistype')
	pxscale	= float(config.get('basic', 'pxscale'))
	sky_m = float(config.get('basic', 'sky_m'))
	mzp = float(config.get('basic', 'mzp'))
	xmin = float(config.get('basic', 'xmin'))
	xmax = float(config.get('basic', 'xmax'))
	fit_mini = float(config.get('basic', 'fit_mini'))
	fit_maxi = float(config.get('basic', 'fit_maxi'))
	itt = config.get('basic', 'input_type')
	table_dir = config.get('basic', 'prof_dir')
	table_input = config.get('basic', 'data')
	
	if itt=='ell': table=readEllipseOutput(table_dir + table_input)
	if itt=='iso': table=readtab_ellipsi(table_dir + table_input)
	if itt=='ir': table=read2cols(table_dir + table_input)
	
	sizearr=len(table['sma'])
	x=[0.]*sizearr
	xt=pxscale*table['sma']
	if (axis =='equ'):
		if (itt != 'ir'):
			print 'Data R column should be the semi-major axis (R_maj). Converting to R_eq...' 
			ell=table['ellip']
			xt[1:len(x)] = xt[1:len(x)]*sqrt(1.-ell[1:len(x)])
		else: print 'Cannot generate equivalent axis without ellipticity column. Assuming the data R column is R_eq.'
	x=x+xt
	data=-2.5*log10(table['intens']/(pxscale**2.)) + mzp
	x_original = array(xt)
	data_original = array(data)
	sampling = 'linear'
	
	xmino = datamin(xmin,x_original)
	xmaxo = datamax(xmax,x_original)
	fit_mino = fitmin(fit_mini,x_original)
	fit_maxo = fitmax(fit_maxi,x_original)
	if (fit_mino < xmino): fit_mino=xmino
	if (fit_maxo > xmaxo): fit_maxo=xmaxo
	if (fit_mino < 0): fit_mino = 0
	x_original = x_original[xmino:xmaxo]
	data_original = data_original[xmino:xmaxo]
	
	#print (x[4]-x[3]), (x[3]-x[2]), (x[2]-x[1]), (x[1]-x[0])
	if ((x[2]-x[1]) != (x[1]-x[0]) and (x[2]-x[1]) != (x[3]-x[2])):
		print 'The radial sampling step is not linear (uniform). Interpolating...'
		f = interp1d(x,data,kind='slinear')
		xn = linspace(0.,max(x),(max(x)-min(x))/((x[2]-x[1])))		
		yn = f(xn)
		x = array(xn)
		data = array(yn)
		sampling = 'nonlinear'
		
	print 'The sampling of the data is :', sampling
 
	xmin = datamin(xmin,x)

	if (xmin != 0.): print 'WARNING: Data does not start at R=0. Convolution might not be correct!'	
	xmax = datamax(xmax,x)
	fit_min = fitmin(fit_mini,x)
	fit_max = fitmax(fit_maxi,x)
	
	if (fit_min < xmin): fit_min=xmin
	if (fit_max > xmax): fit_max=xmax
	if (fit_min < 0): fit_min = 0
	
	x=x[xmin:xmax]
	if (x[len(x)-1] != x_original[len(x_original)-1]): x[len(x)-1] = x_original[len(x_original)-1]
	if (x[0] != x_original[0]): x[0] = x_original[0]
	nxt = 2
	extsize = nxt*len(x)
	xx = array(linspace(x[0],1.*nxt*x[len(x)-1],extsize))
	data=data[xmin:xmax]
	PSF_fft_ext = 0.
	PSF_w = 0.
	elle = [0.]*extsize

	if (itt=='ell' or itt=='iso'):
		ell=table['ellip']
		pan=table['PA']
		if itt=='iso': b4=table['BI4']
		if itt=='ell': b4=table['B4']
		grad=table['GRAD']
		sma=table['sma']
		b4=-1.*b4/grad/sma
		if (axis == 'equ'):
			b4=b4*sqrt(1. - ell)	
		b4=b4[xmino:xmaxo]
		ell=ell[xmino:xmaxo]
		pan=pan[xmino:xmaxo]
		elle[0:len(ell)] = ell[0:len(ell)]
	else:
		ell=[0.]*len(data)
		b4 =[0.]*len(data)
		pan=[0.]*len(data)
	
	n_all=[ns,ne,ng,np,nf,nc,nid,ntd]

	psf_type = config.get('basic', 'ps')
	psf_fwhm = float(config.get('basic', 'psf_fwhm'))*pxscale
	PSF_ext=[1e-99]*extsize
	PSF_o = 0.
	if psf_type == 'moffat':
		psf_beta = float(config.get('basic', 'psf_beta'))
		psf_alpha=psf_fwhm/(2.*sqrt((2.**(1./psf_beta))-1.))
		PSF = ['moffat',psf_fwhm,psf_alpha,psf_beta]
		PSF_o = PSF
		PSF_ext = PSF
	elif psf_type == 'gaussian':
		PSF = ['gaussian',psf_fwhm]
		PSF_o = PSF
		PSF_ext = PSF
	#:::::::::::::::::handle numerical PSF here:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	elif psf_type == 'numerical':
		psf_file = config.get('basic', 'psf_file')
		sizepsf=len(psf_file['sma'])
		xp=[0.]*sizearr
		xt=float(pxscale)*psf_file['sma']
		yp=log10(psf_file['intens']/pxscale**2)
		yp = 10.**(yp)
		
		interp_psf = interp1d(xt,yp,kind='slinear')
		PSF=interp_psf(x)					#used for convolution
		PSF_o = interp_psf(x_original)		#used to build nuclear component and compute residual on original data vector
		#PSF_ext=[1e-99]*extsize				#used to build nuclear component in the extended, nice-looking profile
		#PSF_ext = interp_psf(x)
		PSF_ext[0:len(x)] = PSF[0:len(x)]
		PSF = PSF/max(PSF)
		PSF_ext = PSF_ext/max(PSF_ext)
		PSF_o = PSF_o/max(PSF_o)
	#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	out = minimize(construct_model.residual, fit_parameters, args=(x,n_all,sersic_entry,exp_entry,gauss_entry,psf_entry,ferrer_entry,corsic_entry,id_entry,td_entry,PSF, fit_min,fit_max,ellipticity), kws={'data':data})
	freepar = fit_max-fit_min-out.nfree
	modelf = open(table_dir + 'modelf_'+table_input,'w')
	modelf.write ('#SMA'+ ' '+ 'INTENS'+' '+'INTEND_data'+'\n')
	sfact = 2.5*log10(pxscale**2)
	if sampling=='linear':
		fit = construct_model.residual(fit_parameters, x, n_all, sersic_entry,exp_entry,gauss_entry,psf_entry,ferrer_entry,corsic_entry,id_entry,td_entry,PSF,fit_min,fit_max,ellipticity)
		for i in range(0,len(x)): modelf.write (str(round(x[i]/pxscale,2))+ '  '+str(10.**((mzp - fit[i] + sfact)/2.5))+ '  '+str(10.**((mzp - data_original[i] + sfact)/2.5))+'\n')
		modelf.close()
		residu=data-fit
		resexx=residu[fit_min:fit_max]
		deltarms=sqrt(sum((resexx)**2)/(len(resexx)-freepar+1))
	elif sampling=='nonlinear':
		fit = construct_model.residual(fit_parameters, x, n_all, sersic_entry,exp_entry,gauss_entry,psf_entry,ferrer_entry,corsic_entry,id_entry,td_entry,PSF,fit_min,fit_max,ellipticity)
		if max(x) < max(x_original): x[len(x)-1] = max(x_original)
		modelint = interp1d(x,fit,kind='slinear')
		fit_original = modelint(x_original)
		for i in range(0,len(x_original)): 
			modelf.write (str(round(x_original[i]/pxscale,2))+ '  '+str(10.**((mzp - fit_original[i] + sfact)/2.5)) + '  '+str(10.**((mzp - data_original[i] + sfact)/2.5))+ '\n')
		residu = data_original - fit_original
        resexx=residu[fit_mino:fit_maxo]
        deltarms=sqrt(sum((resexx)**2)/(len(resexx)-freepar+1))
        modelf.close()
	fit_nice = construct_model.residual(fit_parameters, xx, n_all, sersic_entry,exp_entry,gauss_entry,psf_entry,ferrer_entry,corsic_entry,id_entry,td_entry,PSF_ext,fit_min,fit_max,ellipticity)
	print fit_report(fit_parameters, show_correl=False)
	#----------------------------------------------------------
	logf = open(table_dir + 'logfile_'+table_input,'a+')
	logf.write ('-------------------------------- Fit results: ------------------------------------\n')
	logf.write ('----------------------------------------------------------------------------------\n')
	tstamp=str(datetime.datetime.now())
	logf.write ('Time stamp of fit: ' + tstamp + '\n')
	logf.write ('Input file: '+table_dir + table_input +'\n')
	if (psf_type != 'numerical'):
		logf.write ('PSF: ' + str(PSF) + '\n')
	else: logf.write ('PSF: ' + psf_file+'\n')
	logf.write ('Central ellipticity: '+str(ellipticity)+'\n')
	logf.write ('Zero-point magnitude: '+str(mzp)+'\n')
	logf.write ('Pixel scale ["/px]: '+str(pxscale)+'\n')
	logf.write ('Radial axis: '+str(axis)+'\n')
	logf.write ('\n')
	logf.write ('Components [generated, active]: \n')
	logf.write ('Sersic: '+str(ns)+' created, '+str(ns_a)+' active \n')
	logf.write ('Exponential: '+str(ne)+' created, '+str(ne_a)+' active \n')
	logf.write ('Inclined disc: '+str(nid)+' created, '+str(nid_a)+' active \n')
	logf.write ('Truncated disc: '+str(ntd)+' created, '+str(ntd_a)+' active \n')
	logf.write ('Gaussian: '+str(ng)+' created, '+str(ng_a)+' active \n')
	logf.write ('PSF: '+str(np)+' created, '+str(np_a)+' active \n')
	logf.write ('Ferrer: '+str(nf)+' created, '+str(nf_a)+' active \n')
	logf.write ('\n')
	logf.write ('Free parameters = '+str(freepar)+'\n')
	logf.write ('Delta rms = '+str(deltarms))
	logf.write ('\n')
	logf.write ('-----------------------------Detailed fit report:-------------------------------- \n')
	logf.write ('----------------------------------------------------------------------------------\n')
	logf.write ('\n')
	
	pan_el = float(config.get('basic', 'pan_el'))
	pan_pa = float(config.get('basic', 'pan_pa'))
	pan_b4 = float(config.get('basic', 'pan_b4'))
	plottype = config.get('basic', 'plottype')

	if (itt!='ir'): n_panels=5+2*(pan_el+pan_pa+pan_b4)
	else: n_panels=5
	rcParams['figure.figsize'] = 7.5, 0.78*n_panels+1.08
	fig=plt.figure()
	ax1 = plt.subplot2grid((n_panels,3), (0, 0), colspan=3, rowspan=4)
	#----------------------------------------------------------	
	if (fit_min > xmin): ax1.plot(x_original[xmino:fit_mino],data_original[xmino:fit_mino], marker='o', color='white', markersize=7., linestyle='None')
	if (fit_max < xmax): ax1.plot(x_original[fit_maxo:xmaxo],data_original[fit_maxo:xmaxo], marker='o', color='white', markersize=7., linestyle='None')
	ax1.plot(x_original[fit_mino:fit_maxo],data_original[fit_mino:fit_maxo], marker='o', color='red', markersize=7., linestyle='None', label='Data', markeredgecolor='darkred')
	if config.get('basic', 'title'):
		plt.title(config.get('basic', 'title'))
	#----------------------------------------------------------	
	
	if (ns == 1):
		type = 'sersic'
		i=0
		comp=construct_model.component(fit_parameters, xx, ns, i, type, sersic_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
		plt.plot(xx,comp, color=sersic_entry[4], linewidth=1.6)
	
		if (sersic_entry[0]==1):
			
			magtot=integrator.magnitude(xx,comp,mzp)
			logf.write ('+ Integrated total magnitude = '+str(magtot)+'\n')
			logf.write ('------------------------------------------------\n')
			logf.write ('\n')
	elif (ns > 1):
		type = 'sersic'
		for i in range(0,ns):
			comp=construct_model.component(fit_parameters, xx, ns, i, type, sersic_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
			plt.plot(xx,comp, color=sersic_entry[i,4], linewidth=1.6)
		
			if (sersic_entry[i,0]==1):
				magtot=integrator.magnitude(xx,comp,mzp)
				logf.write ('+ Integrated total magnitude mag = '+str(magtot)+'\n')
				logf.write ('------------------------------------------------\n')
				logf.write ('\n')
	if (nc == 1):
		type = 'corsic'
		i=0
		comp=construct_model.component(fit_parameters, xx, nc, i, type, corsic_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
		plt.plot(xx,comp, color=corsic_entry[7], linewidth=1.6)
		if (corsic_entry[0]==1):
			magtot=total_magnitude.compute(xx,comp,elle)
			logf.write ('+ Integrated total magnitude = '+str(magtot)+'\n')
			logf.write ('------------------------------------------------\n')
			logf.write ('\n')
	elif (nc > 1):
		type = 'corsic'
		for i in range(0,ns):
			comp=construct_model.component(fit_parameters, xx, nc, i, type, corsic_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
			plt.plot(xx,comp, color=corsic_entry[i,7], linewidth=1.6)
			if (corsic_entry[i,0]==1):
				magtot=total_magnitude.compute(xx,comp,elle)
				logf.write ('+ Integrated total magnitude mag = '+str(magtot)+'\n')
				logf.write ('------------------------------------------------\n')
				logf.write ('\n')
	if (ne == 1):
		type = 'exponential'
		i=0
		comp=construct_model.component(fit_parameters, xx, ne, i, type, exp_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
		plt.plot(xx,comp, color=exp_entry[3], linewidth=1.6)
		vals = fit_parameters.valuesdict()
		
		if (exp_entry[0]==1):
			magtot=integrator.magnitude(xx,comp,mzp)
			logf.write ('+ Integrated total magnitude = '+str(magtot)+'\n')
			logf.write ('------------------------------------------------\n')
			logf.write ('\n')
	elif (ne > 1):
		type = 'exponential'
		for i in range(0,ne):
			comp=construct_model.component(fit_parameters, xx, ne, i, type, exp_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
			plt.plot(xx,comp, color=exp_entry[i,3], linewidth=1.6)
			if (exp_entry[i,0]==1):
				magtot=integrator.magnitude(xx,comp,mzp)
				logf.write ('+ Integrated total magnitude = '+str(magtot)+'\n')
				logf.write ('------------------------------------------------\n')
				logf.write ('\n')
	if (nid == 1):
		type = 'id'
		i=0
		comp=construct_model.component(fit_parameters, xx, nid, i, type, id_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
		plt.plot(xx,comp, color=id_entry[3], linewidth=1.6)
		vals = fit_parameters.valuesdict()
		

	elif (nid > 1):
		type = 'id'
		for i in range(0,nid):
			comp=construct_model.component(fit_parameters, xx, nid, i, type, id_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
			plt.plot(xx,comp, color=id_entry[i,3], linewidth=1.6)
			
	if (ng == 1):
		type = 'gaussian'
		i=0
		comp=construct_model.component(fit_parameters, xx, ng, i, type, gauss_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
		plt.plot(xx,comp, color=gauss_entry[4], linewidth=1.6)
		if (gauss_entry[0]==1):
		       	magtot=total_magnitude.compute(xx,comp,elle)
		       	logf.write ('+ Integrated total magnitude = '+str(magtot)+'\n')
			logf.write ('------------------------------------------------\n')
			logf.write ('\n')
	elif (ng > 1):
		type = 'gaussian'
		for i in range(0,ng):
			comp=construct_model.component(fit_parameters, xx, ng, i, type, gauss_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
			plt.plot(xx,comp, color=gauss_entry[i,4], linewidth=1.6)
			if (gauss_entry[i,0]==1):
				magtot=total_magnitude.compute(xx,comp,elle)
				logf.write ('+ Integrated total magnitude = '+str(magtot)+'\n')
				logf.write ('------------------------------------------------\n')
				logf.write ('\n')
	if (np == 1):
		type = 'psf'
		i=0
		comp=construct_model.component(fit_parameters, xx, np, i, type, psf_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
		plt.plot(xx,comp, color=psf_entry[2], linewidth=1.6)
		if (psf_entry[0]==1):
		       	magtot=total_magnitude.compute(xx,comp,elle)
		       	logf.write ('+ Integrated total magnitude = '+str(magtot)+'\n')
		       	logf.write ('------------------------------------------------\n')
		       	logf.write ('\n')
	elif (np > 1):
		type = 'psf'
		for i in range(0,np):
			comp=construct_model.component(fit_parameters, xx, np, i, type, psf_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
			plt.plot(xx,comp, color=psf_entry[i,2], linewidth=1.6)
			if (psf_entry[i,0]==1):
				magtot=total_magnitude.compute(xx,comp,elle)
				logf.write ('+ Integrated total magnitude = '+str(magtot)+'\n')
				logf.write ('------------------------------------------------\n')
				logf.write ('\n')
	if (nf == 1):
		type = 'ferrer'
		i=0
		comp=construct_model.component(fit_parameters, xx, nf, i, type, ferrer_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
		plt.plot(xx,comp, color=ferrer_entry[5], linewidth=1.6)
		if (ferrer_entry[0]==1):
		       	magtot=total_magnitude.compute(xx,comp,elle)
		       	logf.write ('+ Integrated total magnitude = '+str(magtot)+'\n')
		       	logf.write ('------------------------------------------------\n')
		       	logf.write ('\n')
	elif (nf > 1):
		type = 'ferrer'
		for i in range(0,nf):
			comp=construct_model.component(fit_parameters, xx, nf, i, type, ferrer_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
			plt.plot(xx,comp, color=ferrer_entry[i,5], linewidth=1.6)
			if (ferrer_entry[i,0]==1):
				magtot=total_magnitude.compute(xx,comp,elle)
				logf.write ('+ Integrated total magnitude = '+str(magtot)+'\n')
				logf.write ('------------------------------------------------\n')
				logf.write ('\n')
	if (ntd == 1):
		type = 'td'
		i=0
		comp=construct_model.component(fit_parameters, xx, ntd, i, type, td_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
		plt.plot(xx,comp, color=td_entry[5], linewidth=1.6)
		if (td_entry[0]==1):
		       	magtot=total_magnitude.compute(xx,comp,elle)
		       	logf.write ('+ Integrated total magnitude = '+str(magtot)+'\n')
		       	logf.write ('------------------------------------------------\n')
		       	logf.write ('\n')
	elif (ntd > 1):
		type = 'td'
		for i in range(0,ntd):
			comp=construct_model.component(fit_parameters, xx, ntd, i, type, td_entry, PSF_ext, PSF_fft_ext, PSF_w, logf, elle,ellipticity)
			plt.plot(xx,comp, color=td_entry[i,5], linewidth=1.6)
			if (td_entry[i,0]==1):
				magtot=total_magnitude.compute(xx,comp,elle)
				logf.write ('+ Integrated total magnitude = '+str(magtot)+'\n')
				logf.write ('------------------------------------------------\n')
				logf.write ('\n')
	
	logf.write ('Raw fit report (all parameters[numbered from 0-n] and their correlations): \n')
	logf.write ('------------------------------------------------------------------------------------\n')
	logf.write (fit_report(fit_parameters))
	logf.write ('\n----------------------------------------------------------------------------------\n')
	logf.write ('-----------------------------------------END----------------------------------------\n')
	logf.write ('\n')
	logf.close()
	
	if float(config.get('basic', 'plot')):
		ax1.plot(xx,fit_nice, color='black', label='Model', linewidth=1.6)
		plt.gca().invert_yaxis()
		plt.ylabel('$\mu$ [mag arcsec$^{-2}$]', fontsize=20, labelpad=28)
		plt.tick_params(axis='x', labelbottom='off')
		plt.tick_params(axis='y', labelsize=19)
		plt.minorticks_on()
		plt.tick_params('both', length=5, width=1.2, which='major')
		plt.tick_params('both', length=3, width=1, which='minor')
		if (plottype == 'log'): plt.xscale('log')
		
		y_max=max(data)+0.9
		y_min=min(data)-0.9
		
		if (y_max - int(y_max) < 0.2): y_max = y_max+0.3
		if (y_min - int(y_min) < 0.2): y_min = y_min-0.3
		
		if (1 + int(y_max) - y_max < 0.2): y_max = y_max-0.3
		if (1 + int(y_min) - y_min < 0.2): y_min = y_min+0.3

		plt.ylim([y_max,y_min])
		
		if ((plottype == 'log')):
			if min(x)==0.: xminplt=0.75*x[1]
			else: xminplt = 0.75*min(x) 
			plt.xlim(xminplt,1.25*max(x))
			xsky=linspace(min(x[1:len(x)])-0.5*min(x[1:len(x)]),max(x)+0.3,len(x))
			skyline=[sky_m]*len(xsky)
			plt.plot(xsky,skyline, linewidth=1., color='black', ls = ':')
		else: 
			plt.xlim(min(x)-0.25*max(x),1.09*max(x))
			xsky=linspace(min(x)-10.05,1.5*max(x),len(x))
			skyline=[sky_m]*len(xsky)
			plt.plot(xsky,skyline, linewidth=1., color='black', ls = ':')
		
		ax2 = plt.subplot2grid((n_panels,3), (4, 0), colspan=3, sharex=ax1)
		resex=residu
		ax2.plot(x_original,resex, marker='o', color='black', markersize=5.5, linestyle='None', label='Delta')
		ax2.plot(x_original[xmino:fit_mino],resex[xmino:fit_mino], marker='o', color='white', markersize=5.5, linestyle='None')
		ax2.plot(x_original[fit_maxo:xmaxo],resex[fit_maxo:xmaxo], marker='o', color='white', markersize=5.5, linestyle='None')
		ax2.plot(x_original[fit_mino:fit_maxo],resex[fit_mino:fit_maxo], marker='o', color='black', markersize=5.5, linestyle='None')
		plt.ylabel('$\Delta\mu$', fontsize=20, labelpad=7)
		if (n_panels-4-1 > 0): plt.tick_params(axis='x', labelbottom='off')
		plt.tick_params(axis='y', labelsize=19)
		plt.yticks([-0.1,0,0.1])
		if (plottype == 'log'): plt.xscale('log')
		plt.ylim(0.18,-0.18)
		if ((plottype == 'log')): 
			xz=linspace(1e-11,max(x)+0.5*max(x),len(x))
			zer=[0.0]*len(xz)
			plt.plot(xz,zer, linewidth=1.5, color='red')
		else: 
			plt.xlim(min(x)-0.05*max(x),1.09*max(x))
			xz=linspace(min(x)-0.5*max(x),max(x)+0.5*max(x),len(x))
			zer=[0.0]*len(xz)
			plt.plot(xz,zer, linewidth=1.5, color='red')
		deltarms="{:10.4f}".format(deltarms)	
		plt.text(0.05,0.0, r'$\Delta_{rms} = $' + deltarms, fontsize=19, horizontalalignment='left', verticalalignment='bottom', transform = ax2.transAxes)
		plt.minorticks_on()
		plt.tick_params('both', length=5, width=1.2, which='major')
		plt.tick_params('both', length=3, width=1, which='minor')
		
		npan_extra = 5
		
		if pan_el>0 and itt!='ir':
			ax3 = plt.subplot2grid((n_panels,3), (npan_extra, 0), colspan=3, rowspan=2, sharex = ax1)
			ax3.plot(x_original,ell, marker='o', color='white', markersize=5.5, linestyle='None')
			ax3.plot(x_original[fit_mino:fit_maxo],ell[fit_mino:fit_maxo], marker='o', color='black', markersize=5.5, linestyle='None')
			plt.ylabel('$\epsilon$', fontsize=21, labelpad=20)
			if (n_panels-npan_extra-2 > 0): plt.tick_params(axis='x', labelbottom='off')
			plt.tick_params(axis='y', labelsize=19)
			plt.ylim([-0.04,0.92])
			plt.yticks([0.,0.2, 0.4, 0.6, 0.8])
			plt.minorticks_on()
			plt.tick_params('both', length=5, width=1.2, which='major')
			plt.tick_params('both', length=3, width=1, which='minor')
			npan_extra = npan_extra+2
			
		if pan_pa>0 and itt!='ir':
			ax4 = plt.subplot2grid((n_panels,3), (npan_extra, 0), colspan=3, rowspan=2, sharex=ax1)
			ax4.plot(x_original,pan, marker='o', color='white', markersize=6, linestyle='None', label='Delta')
			ax4.plot(x_original[fit_mino:fit_maxo],pan[fit_mino:fit_maxo], marker='o', color='black', markersize=6, linestyle='None', label='Delta')
			plt.ylabel('$PA$ [deg]', fontsize=20)
			if (n_panels-npan_extra-2 > 0): plt.tick_params(axis='x', labelbottom='off')
			plt.tick_params(axis='y', labelsize=19)
			ax4.set_ylim([-90.,90.])
			ax4.set_yticks([-50,0,50])
			plt.minorticks_on()
			plt.tick_params('both', length=5, width=1.2, which='major')
			plt.tick_params('both', length=3, width=1, which='minor')
			npan_extra=npan_extra+2
		
		if pan_b4>0 and itt!='ir':
			ax5 = plt.subplot2grid((n_panels,3), (npan_extra, 0), colspan=3, rowspan=2, sharex=ax1)
			ax5.plot(x_original,b4, marker='o', color='white', markersize=5.5, linestyle='None', label='Delta')
			ax5.plot(x_original[fit_mino:fit_maxo],b4[fit_mino:fit_maxo], marker='o', color='black', markersize=5.5, linestyle='None', label='Delta')
			zer=[0.0]*len(xz)
			ax5.plot(xz,zer, linewidth=1.5, color='red')
			ax5.set_ylabel('$B_{4}$', fontsize=20, labelpad=-10)
			ax5.set_ylim([-0.14,0.14])
			ax5.set_yticks([-0.1,-0.05,0.,0.05,0.1])
			plt.tick_params(axis='y', labelsize=19)
			npan_extra = npan_extra+2
			
		if (axis == 'maj'): plt.xlabel(r'$R_{\rm maj}$ [arcsec]', fontsize=20)
		if (axis == 'equ'): plt.xlabel(r'$R_{\rm eq}$ [arcsec]', fontsize=20)
		plt.tick_params(axis='x', labelsize=19)	
		if (plottype == 'log'): plt.xscale('log')
		if ((plottype == 'log')):
			if min(x_original)==0.: xminplt=0.45*x_original[1]
			else: xminplt = 0.45*min(x_original) 
			plt.xlim(xminplt,1.25*max(x))
			ax1.xaxis.set_major_formatter(FuncFormatter(log_axis))
		else: 
				plt.xlim(min(x)-0.05*max(x),1.09*max(x))
		plt.minorticks_on()
		plt.tick_params('both', length=5, width=1.2, which='major')
		plt.tick_params('both', length=3, width=1, which='minor')
		
		fig.subplots_adjust(hspace=0,left=0.18, bottom=0.09*(9./n_panels), right=0.97, top=(1.-0.03*(9./n_panels)))
		fig.tight_layout()
		plot_name = config.get('basic', 'plot_name')
		plot_dir = config.get('basic', 'plot_dir')
		plt.savefig(plot_dir + plot_name)
	plt.clf()

	return fit_parameters, deltarms

def add_sersic():
	global ns, sersic_entry
	se=[0.0]*8
	se[0] = float(config.get('sersic', 'sersic'))
	se[1] = float(config.get('sersic', 'sersic_n'))
	se[2] = float(config.get('sersic', 'mu_e'))
	se[3] = float(config.get('sersic', 'R_e'))
	se[4] = config.get('sersic', 's_r')
	se[5] = float(config.get('sersic', 'fix_n'))
	se[6] = float(config.get('sersic', 'fix_mu'))
	se[7] = float(config.get('sersic', 'fix_R_e'))
	if (ns == 0): sersic_entry=se
	else: sersic_entry=vstack([sersic_entry,se])
	ns=ns+1

def add_corsic():
	global nc, corsic_entry
	ce=[0.0]*14
	ce[0] = float(config.get('corsic', 'corsic'))
	ce[1] = float(config.get('corsic', 'mu_pr'))
	ce[2] = float(config.get('corsic', 'alpha_cs'))
	ce[3] = float(config.get('corsic', 'gamma_cs'))
	ce[4] = float(config.get('corsic', 'R_b'))
	ce[5] = float(config.get('corsic', 'R_e_cs'))
	ce[6] = float(config.get('corsic', 'sersic_n_cs'))
	ce[7] = config.get('corsic', 'cs_r')
	ce[8] = float(config.get('corsic', 'fix_mu_cs'))
	ce[9] = float(config.get('corsic', 'fix_al_cs'))
	ce[10] = float(config.get('corsic', 'fix_gamma'))
	ce[11] = float(config.get('corsic', 'fix_R_b'))
	ce[12] = float(config.get('corsic', 'fix_R_e_cs'))
	ce[13] = float(config.get('corsic', 'fix_n_cs'))
	if (nc == 0): corsic_entry=ce
	else: corsic_entry=vstack([corsic_entry,ce])
	nc=nc+1

def add_exponential():
	global ne, exp_entry
	ee=[0.0]*6
	ee[0] = float(config.get('exponential', 'exp'))
	ee[1] = float(config.get('exponential', 'h'))
	ee[2] = float(config.get('exponential', 'mu_0'))
	ee[3] = config.get('exponential', 'exp_b')
	ee[4] = float(config.get('exponential', 'fix_h'))
	ee[5] = float(config.get('exponential', 'fix_mu_0'))
	if (ne == 0): exp_entry=ee
	else: exp_entry=vstack([exp_entry,ee])
	ne=ne+1	

def add_gaussian():
	global ng, gauss_entry
	ge=[0.0]*8
	ge[0] = float(config.get('gaussian', 'gaussian'))
	ge[1] = float(config.get('gaussian', 'R0r'))
	ge[2] = float(config.get('gaussian', 'mu_0r'))
	ge[3] = float(config.get('gaussian', 'fwhmr'))
	ge[4] = config.get('gaussian', 'gauss_c')
	ge[5] = float(config.get('gaussian', 'fix_R0r'))
	ge[6] = float(config.get('gaussian', 'fix_mu_0r'))
	ge[7] = float(config.get('gaussian', 'fix_fwhmr'))
	if (ng == 0): gauss_entry=ge
	else: gauss_entry=vstack([gauss_entry,ge])
	ng=ng+1

def add_psf():
	global np, psf_entry
	pe=[0.0]*4
	pe[0] = float(config.get('psf', 'psf'))
	pe[1] = float(config.get('psf', 'mu0p'))
	pe[2] = config.get('psf', 'psf_l')
	pe[3] = float(config.get('psf', 'fix_mu0p'))
	if (np == 0): psf_entry=pe
	else: psf_entry=vstack([psf_entry,pe])
	np=np+1
	
def add_ferrer():
	global nf, ferrer_entry
	fe=[0.0]*10
	fe[0] = float(config.get('ferrer', 'ferrer'))
	fe[1] = float(config.get('ferrer', 'R_out'))
	fe[2] = float(config.get('ferrer', 'mu_0f'))
	fe[3] = float(config.get('ferrer', 'alpha_F'))
	fe[4] = float(config.get('ferrer', 'beta_F'))
	fe[5] = config.get('ferrer', 'ferrer_o')
	fe[6] = float(config.get('ferrer', 'fix_R_out'))
	fe[7] = float(config.get('ferrer', 'fix_mu_0f'))
	fe[8] = float(config.get('ferrer', 'fix_alpha'))
	fe[9] = float(config.get('ferrer', 'fix_beta'))
	if (nf == 0): ferrer_entry=fe
	else: ferrer_entry=vstack([ferrer_entry,fe])
	nf=nf+1
	
def add_td():
	global ntd, td_entry
	td=[0.0]*10
	td[0] = float(config.get('td', 'td'))
	td[1] = float(config.get('td', 'mu_0t'))
	td[2] = float(config.get('td', 'r_B'))
	td[3] = float(config.get('td', 'h1'))
	td[4] = float(config.get('td', 'h2'))
	td[5] = config.get('td', 'td_b')
	td[6] = float(config.get('td', 'fix_mu_0t'))
	td[7] = float(config.get('td', 'fix_r_b'))
	td[8] = float(config.get('td', 'fix_h1'))
	td[9] = float(config.get('td', 'fix_h2'))
	if (ntd == 0): td_entry=td
	else: td_entry=vstack([td_entry,td])
	ntd=ntd+1
	
def add_id():
	global nid, id_entry
	es=[0.0]*7
	es[0] = float(config.get('id', 'id'))
	es[1] = float(config.get('id', 'z0'))
	es[2] = float(config.get('id', 'mu_0z'))
	es[3] = config.get('id', 'id_b')
	es[4] = float(config.get('id', 'fix_z0'))
	es[5] = float(config.get('id', 'fix_mu_0z'))
	es[6] = config.get('id', 'zero')
	if (nid == 0): id_entry=es
	else: id_entry=vstack([id_entry,es])
	nid=nid+1

def run_profiler(parameters, sersic=True, corsic=False, exponential=False, gaussian=False, psf=False, ferrer=False, 
			tdisk=False, idisk=False):
	
	rc('font', **{'family':'serif','serif':['Palatino']})
	rc('text', usetex=True)

	global config 
	config = ConfigParser.ConfigParser()
	config.read(parameters) 

	global ns,ne,ng,np,nf,nc,nid,ntd,ns_a,ne_a,ng_a,np_a,nf_a,nc_a,nid_a,ntd_a #the '_a' stands for 'active' 
	global sersic_entry,exp_entry,gauss_entry,psf_entry,ferrer_entry,corsic_entry,sinh_entry
	sersic_entry=[0.0]*8 #structure == var_sersic,mue,re,n,colour,varymue,varyre,varyn
	exp_entry=[0.0]*6 #structure == var_exp,h,mu0
	id_entry=[0.0]*7 #structure == var_id,z,mu0,zero_coord
	gauss_entry=[0.0]*8 #structure == var_gauss,r0r,mu0r,fwhmr
	psf_entry=[0.0]*4 #structure == var_psf, mu0p
	ferrer_entry=[0.0]*10 #structure == var_ferrer, r_out, mu_0f, alpha_F, beta_F
	corsic_entry=[0.0]*14
	td_entry=[0.0]*10
	ns=0; ne=0; ng=0; np=0; nf=0; nc=0; nid=0; ntd=0

	if sersic: add_sersic()
	if corsic: add_corsic()
	if exponential: add_exponential()
	if gaussian: add_gaussian()
	if psf: add_psf()
	if ferrer: add_ferrer()
	if tdisk: add_td()
	if idisk: add_id()

	fit, delta = fitcomm(sersic_entry, exp_entry, id_entry, gauss_entry, psf_entry, ferrer_entry, corsic_entry, td_entry)
	vals = fit.valuesdict()
	sersic_n = vals['n_Ser0']
	r_e = vals['r_e0']
	mu_e = vals['mu_e0']

	return [sersic_n, r_e, mu_e], delta

if __name__ == '__main__':

	parser = argparse.ArgumentParser(description='Run \'profiler\' for Sersic indices of galaxies.')

	parser.add_argument('--params', help='Parameter file for \'profiler\'')
	parser.add_argument('--results', help='Results file for \'profiler\'')
	parser.add_argument('--sersic', help='Sersic profile component will be fitted')
	parser.add_argument('--corsic', help='Core Sersic profile component will be fitted')
	parser.add_argument('--exponential', help='Exponential component will be fitted')
	parser.add_argument('--gaussian', help='Gaussian component will be fitted')
	parser.add_argument('--psf', help='PSF component will be fitted')
	parser.add_argument('--ferrer', help='Ferrer component will be fitted')
	parser.add_argument('--tdisk', help='Truncated disk component will be fitted')
	parser.add_argument('--idisk', help='Inclined disk component will be fitted')
	args = parser.parse_args()

	fit, delta = run_profiler(args.params, sersic=args.sersic, corsic=args.corsic, exponential=args.exponential, gaussian=args.gaussian, 
			psf=args.psf, ferrer=args.ferrer, tdisk=args.tdisk, idisk=args.idisk)

	if args.results:
		with open(args.results, 'w') as f:
			f.write(str(fit[0]) + ' ' + str(fit[1]) + ' ' + str(fit[2]) + ' ' + str(delta))