from numpy import *
from scipy.signal import fftconvolve
from scipy.interpolate import interp1d

def gaussian(x, mu, sig):
    gauss = exp(-(x - mu) ** 2.0 / (2.0 * sig ** 2.0))
    return gauss / sum(gauss)


def moffat(x, mu, alpha, beta):
    moffat = (1.0 + (x - mu) ** 2.0 / alpha ** 2.0) ** (-1.0 * beta)
    return moffat / sum(moffat)


def GaussianKernel(x, y, e, f):
    x = array(x)
    ten_alpha = int(15.0 * f / (x[2] - x[1]))
    if ten_alpha > len(x):
        ten_alpha = len(x)
    five_alpha = int(11.0 * f / (x[2] - x[1]))
    if five_alpha > len(x):
        five_alpha = len(x)
    size = 2 * ten_alpha - 1
    q1 = ten_alpha - 1
    q2 = q1 - 1
    z = [[0.0] * size] * size
    z = array(z)
    zg = [[0.0] * size] * size
    zg = array(zg)
    g = gaussian(x, 0.0, f / 2.3548)
    for i in range(0, q1 + 1):
        for j in range(0, q1 + 1):
            idx = int(sqrt((i / (1.0 - e)) ** 2 + j ** 2))
            idxg = int(sqrt(i ** 2 + j ** 2))
            if idx > q1:
                idx = q1
            if idxg > q1:
                idxg = q1
            z[i + q1][j + q1] = y[idx]
            z[q2 - i + 1][q2 - j + 1] = y[idx]
            z[i + q1][q2 - j + 1] = y[idx]
            z[q2 - i + 1][j + q1] = y[idx]
            zg[i + q1][j + q1] = g[idxg]
            zg[q2 - i + 1][q2 - j + 1] = g[idxg]
            zg[i + q1][q2 - j + 1] = g[idxg]
            zg[q2 - i + 1][j + q1] = g[idxg]

    zg = zg / sum(zg)
    z_conv = fftconvolve(z, zg, mode='same')
    yc = y
    for i in range(0, five_alpha - 1):
        yc[i] = z_conv[q1][i + q1]

    return yc


def MoffatKernel(x, y, e, alpha, beta):
    x = array(x)
    f = 2.0 * alpha * sqrt(2 ** (1 / beta) - 1.0)
    ten_alpha = int(15.0 * alpha / (x[2] - x[1]))
    if ten_alpha > len(x):
        ten_alpha = len(x)
    five_alpha = int(11.0 * alpha / (x[2] - x[1]))
    if five_alpha > len(x):
        five_alpha = len(x)
    size = 2 * ten_alpha - 1
    q1 = ten_alpha - 1
    q2 = q1 - 1
    z = [[0.0] * size] * size
    z = array(z)
    zm = [[0.0] * size] * size
    zm = array(zm)
    m = moffat(x, 0.0, alpha, beta)
    for i in range(0, q1 + 1):
        for j in range(0, q1 + 1):
            idx = int(sqrt((i / (1.0 - e)) ** 2 + j ** 2))
            idxg = int(sqrt(i ** 2 + j ** 2))
            if idx > q1:
                idx = q1
            if idxg > q1:
                idxg = q1
            z[i + q1][j + q1] = y[idx]
            z[q2 - i + 1][q2 - j + 1] = y[idx]
            z[i + q1][q2 - j + 1] = y[idx]
            z[q2 - i + 1][j + q1] = y[idx]
            zm[i + q1][j + q1] = m[idxg]
            zm[q2 - i + 1][q2 - j + 1] = m[idxg]
            zm[i + q1][q2 - j + 1] = m[idxg]
            zm[q2 - i + 1][j + q1] = m[idxg]

    zm = zm / sum(zm)
    z_conv = fftconvolve(z, zm, mode='same')
    yc = y
    for i in range(0, five_alpha - 1):
        yc[i] = z_conv[q1][i + q1]

    return yc


def NumericalKernel(x, y, e, ker):
    x = array(x)
    k = array(ker)
    size = 2 * len(x) - 1
    q1 = len(x) - 1
    q2 = q1 - 1
    z = [[0.0] * size] * size
    z = array(z)
    zk = [[0.0] * size] * size
    zk = array(zk)
    for i in range(0, q1 + 1):
        for j in range(0, q1 + 1):
            idx = int(sqrt((i / (1.0 - e)) ** 2 + j ** 2))
            idxg = int(sqrt(i ** 2 + j ** 2))
            if idx > q1:
                idx = q1
            if idxg > q1:
                idxg = q1
            z[i + q1][j + q1] = y[idx]
            z[q2 - i + 1][q2 - j + 1] = y[idx]
            z[i + q1][q2 - j + 1] = y[idx]
            z[q2 - i + 1][j + q1] = y[idx]
            zk[i + q1][j + q1] = k[idxg]
            zk[q2 - i + 1][q2 - j + 1] = k[idxg]
            zk[i + q1][q2 - j + 1] = k[idxg]
            zk[q2 - i + 1][j + q1] = k[idxg]

    zk = zk / sum(zk)
    z_conv = fftconvolve(z, zk, mode='same')
    yc = [0.0] * len(x)
    for i in range(0, q1):
        yc[i] = z_conv[q1][i + q1]

    yc[len(x) - 5:len(x)] = y[len(x) - 5:len(x)]
    return yc