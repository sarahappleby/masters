# 2017.11.20 17:45:27 GMT
#Embedded file name: /Users/bciambur/mega/profiler_alpha/indices.py


def datamin(dm, x):
    i = 0
    while i < len(x) and x[i] <= dm:
        idmin = i
        i = i + 1

    return idmin


def datamax(dma, x):
    i = 0
    while i < len(x) and x[i] <= dma:
        idmax = i
        i = i + 1

    return idmax + 1


def fitmin(fm, x):
    i = 0
    while i < len(x) and x[i] <= fm:
        idfmin = i
        i = i + 1

    return idfmin


def fitmax(fma, x):
    i = 0
    while i < len(x) and x[i] <= fma:
        idfmax = i
        i = i + 1

    return idfmax + 1
