# 2017.11.18 16:20:28 GMT
#Embedded file name: /Users/bciambur/mega/profiler_beta_new/construct_model.py
from lmfit import Parameters, fit_report
import components
import bulgemag, barmag
import total_magnitude
import PSF_Convolution_2D
from numpy import log10, array

def residual(pars, x, n_all, sersic_entry, exp_entry, gauss_entry, psf_entry, ferrer_entry, corsic_entry, id_entry, td_entry, PSF, fit_min, fit_max, el, data = None):
    vals = pars.valuesdict()
    model = array([0.0] * len(x))
    if n_all[0] > 1:
        for i in range(0, n_all[0]):
            if sersic_entry[i, 0] == 1:
                s = str(i)
                mu_e = vals['mu_e' + s]
                r_e = vals['r_e' + s]
                n_Ser = vals['n_Ser' + s]
                model = model + 10.0 ** (components.Sersic(x, mu_e, r_e, n_Ser) / 2.5)

    elif n_all[0] == 1:
        if sersic_entry[0] == 1:
            mu_e = vals['mu_e0']
            r_e = vals['r_e0']
            n_Ser = vals['n_Ser0']
            model = model + 10.0 ** (components.Sersic(x, mu_e, r_e, n_Ser) / 2.5)
    if n_all[1] > 1:
        for i in range(0, n_all[1]):
            if exp_entry[i, 0] == 1:
                s = str(i)
                mu_0 = vals['mu_0' + s]
                h = vals['h' + s]
                model = model + 10.0 ** (components.exponential(x, mu_0, h) / 2.5)

    elif n_all[1] == 1:
        if exp_entry[0] == 1:
            mu_0 = vals['mu_00']
            h = vals['h0']
            model = model + 10.0 ** (components.exponential(x, mu_0, h) / 2.5)
    if n_all[2] > 1:
        for i in range(0, n_all[2]):
            if gauss_entry[i, 0] == 1:
                s = str(i)
                mu_0r = vals['mu_0r' + s]
                r0r = vals['r0r' + s]
                fwhm_r = vals['fwhm_r' + s]
                model = model + 10.0 ** (components.Gauss_ring(x, mu_0r, r0r, fwhm_r) / 2.5)

    elif n_all[2] == 1:
        if gauss_entry[0] == 1:
            mu_0r = vals['mu_0r0']
            r0r = vals['r0r0']
            fwhm_r = vals['fwhm_r0']
            model = model + 10.0 ** (components.Gauss_ring(x, mu_0r, r0r, fwhm_r) / 2.5)
    if n_all[4] > 1:
        for i in range(0, n_all[4]):
            if ferrer_entry[i, 0] == 1:
                s = str(i)
                mu_0f = vals['mu_0f' + s]
                alpha_F = vals['alpha_F' + s]
                beta_F = vals['beta_F' + s]
                r_out = vals['r_out' + s]
                model = model + 10.0 ** (components.Ferrer(x, mu_0f, r_out, alpha_F, beta_F) / 2.5)

    elif n_all[4] == 1:
        if ferrer_entry[0] == 1:
            mu_0f = vals['mu_0f0']
            alpha_F = vals['alpha_F0']
            beta_F = vals['beta_F0']
            r_out = vals['r_out0']
            model = model + 10.0 ** (array(components.Ferrer(x, mu_0f, r_out, alpha_F, beta_F)) / 2.5)
    if n_all[5] > 1:
        for i in range(0, n_all[5]):
            if corsic_entry[i, 0] == 1:
                s = str(i)
                mu_pr = vals['mu_pr' + s]
                al = vals['al' + s]
                ga = vals['ga' + s]
                r_eCS = vals['r_eCS' + s]
                r_b = vals['r_b' + s]
                n_SerCS = vals['n_SerCS' + s]
                model = model + 10.0 ** (components.Corsic(x, mu_pr, al, ga, r_b, r_eCS, n_SerCS) / 2.5)

    elif n_all[5] == 1:
        if corsic_entry[0] == 1:
            mu_pr = vals['mu_pr0']
            al = vals['al0']
            ga = vals['ga0']
            r_eCS = vals['r_eCS0']
            r_b = vals['r_b0']
            n_SerCS = vals['n_SerCS0']
            model = model + 10.0 ** (components.Corsic(x, mu_pr, al, ga, r_b, r_eCS, n_SerCS) / 2.5)
    if n_all[6] > 1:
        for i in range(0, n_all[6]):
            if id_entry[i, 0] == 1:
                s = str(i)
                mu_0z = vals['mu_0z' + s]
                z0 = vals['z0' + s]
                case = id_entry[i, 6]
                model = model + 10.0 ** (components.id(x, mu_0z, z0, case) / 2.5)

    elif n_all[6] == 1:
        if id_entry[0] == 1:
            mu_0z = vals['mu_0z0']
            z0 = vals['z00']
            case = id_entry[6]
            model = model + 10.0 ** (components.id(x, mu_0z, z0, case) / 2.5)
    if n_all[7] > 1:
        for i in range(0, n_all[7]):
            if td_entry[i, 0] == 1:
                s = str(i)
                mu_0td = vals['mu_0td' + s]
                r_b = vals['r_b' + s]
                h1 = vals['h1' + s]
                h2 = vals['h2' + s]
                model = model + 10.0 ** (components.td(x, mu_0td, r_b, h1, h2) / 2.5)

    elif n_all[7] == 1:
        if td_entry[0] == 1:
            mu_0td = vals['mu_0td0']
            r_b = vals['r_b0']
            h1 = vals['h10']
            h2 = vals['h20']
            model = model + 10.0 ** array(components.td(x, mu_0td, r_b, h1, h2) / 2.5)
    if PSF[0] == 'gaussian':
        model_c = PSF_Convolution_2D.GaussianKernel(x, model, el, PSF[1])
        if n_all[3] > 1:
            for i in range(0, n_all[3]):
                if psf_entry[i, 0] == 1:
                    s = str(i)
                    mu_0n = vals['mu_0p' + s]
                    model_c = model_c + 10.0 ** (components.Gauss_nuc(x, mu_0n, PSF[1]) / 2.5)

        elif n_all[3] == 1:
            if psf_entry[0] == 1:
                mu_0n = vals['mu_0p0']
                model_c = model_c + 10.0 ** (components.Gauss_nuc(x, mu_0n, PSF[1]) / 2.5)
        model = 2.5 * log10(model_c)
    if PSF[0] == 'moffat':
        model_c = PSF_Convolution_2D.MoffatKernel(x, model, el, PSF[2], PSF[3])
        if n_all[3] > 1:
            for i in range(0, n_all[3]):
                if psf_entry[i, 0] == 1:
                    s = str(i)
                    mu_0n = vals['mu_0p' + s]
                    model_c = model_c + 10.0 ** (components.Moffat_nuc(x, mu_0n, PSF[1], PSF[2], PSF[3]) / 2.5)

        elif n_all[3] == 1:
            if psf_entry[0] == 1:
                mu_0n = vals['mu_0p0']
                model_c = model_c + 10.0 ** (components.Moffat_nuc(x, mu_0n, PSF[1], PSF[2], PSF[3]) / 2.5)
        model = 2.5 * log10(model_c)
    if len(PSF) > 4:
        model_c = PSF_Convolution_2D.NumericalKernel(x, model, el, PSF)
        if n_all[3] > 1:
            for i in range(0, n_all[3]):
                if psf_entry[i, 0] == 1:
                    s = str(i)
                    mu_0n = vals['mu_0p' + s]
                    model_c = model_c + 10.0 ** (components.Vector_nuc(mu_0n, PSF[0:len(model_c)]) / 2.5)

        elif n_all[3] == 1:
            if psf_entry[0] == 1:
                mu_0n = vals['mu_0p0']
                model_c = model_c + 10.0 ** (components.Vector_nuc(mu_0n, PSF[0:len(model_c)]) / 2.5)
        model = 2.5 * log10(model_c)
    if data is None:
        return -1.0 * model
    return -1.0 * model[fit_min:fit_max] - data[fit_min:fit_max]


def component(pars, x, n, i, type, entry, PSF, PSF_ext, overlap, logf, ell, el, data = None):
    vals = pars.valuesdict()
    model = array([0.0] * len(x))
    if type == 'sersic':
        if n > 1:
            if entry[i, 0] == 1:
                s = str(i)
                mu_e = vals['mu_e' + s]
                r_e = vals['r_e' + s]
                n_Ser = vals['n_Ser' + s]
                logf.write('Sersic component: ' + str(i + 1) + '\n')
                logf.write('\n')
                logf.write('mu_e = ' + str(mu_e) + '\n')
                logf.write('r_e = ' + str(r_e) + '\n')
                logf.write('n = ' + str(n_Ser) + '\n')
                logf.write('\n')
                logf.write('+ [3*]  Total magnitude = ' + str(bulgemag.bmag(mu_e, r_e, n_Ser, x, ell, logf)) + '\n')
                model = model + 10.0 ** (components.Sersic(x, mu_e, r_e, n_Ser) / 2.5)
        if n == 1:
            if entry[0] == 1:
                mu_e = vals['mu_e0']
                r_e = vals['r_e0']
                n_Ser = vals['n_Ser0']
                logf.write('Sersic component: \n')
                logf.write('\n')
                logf.write('mu_e = ' + str(mu_e) + '\n')
                logf.write('r_e = ' + str(r_e) + '\n')
                logf.write('n = ' + str(n_Ser) + '\n')
                logf.write('\n')
                logf.write('+ [3*]  Total magnitude = ' + str(bulgemag.bmag(mu_e, r_e, n_Ser, x, ell, logf)) + '\n')
                model = model + 10.0 ** (components.Sersic(x, mu_e, r_e, n_Ser) / 2.5)
    if type == 'corsic':
        if n > 1:
            if entry[i, 0] == 1:
                s = str(i)
                mu_pr = vals['mu_pr' + s]
                al = vals['al' + s]
                ga = vals['ga' + s]
                r_eCS = vals['r_eCS' + s]
                r_b = vals['r_b' + s]
                n_SerCS = vals['n_SerCS' + s]
                logf.write('Core-Sersic component: ' + str(i + 1) + ' \n')
                logf.write('\n')
                logf.write('mu_p = ' + str(mu_pr) + '\n')
                logf.write('r_e = ' + str(r_eCS) + '\n')
                logf.write('r_b = ' + str(r_b) + '\n')
                logf.write('n = ' + str(n_SerCS) + '\n')
                logf.write('alpha = ' + str(al) + '\n')
                logf.write('gamma = ' + str(ga) + '\n')
                logf.write('\n')
                model = 10.0 ** (components.Corsic(x, mu_pr, al, ga, r_b, r_eCS, n_SerCS) / 2.5)
        elif n == 1:
            if entry[0] == 1:
                mu_pr = vals['mu_pr0']
                al = vals['al0']
                ga = vals['ga0']
                r_eCS = vals['r_eCS0']
                r_b = vals['r_b0']
                n_SerCS = vals['n_SerCS0']
                logf.write('Core-Sersic component: \n')
                logf.write('\n')
                logf.write('mu_p = ' + str(mu_pr) + '\n')
                logf.write('r_e = ' + str(r_eCS) + '\n')
                logf.write('r_b = ' + str(r_b) + '\n')
                logf.write('n = ' + str(n_SerCS) + '\n')
                logf.write('alpha = ' + str(al) + '\n')
                logf.write('gamma = ' + str(ga) + '\n')
                logf.write('\n')
                model = 10.0 ** (components.Corsic(x, mu_pr, al, ga, r_b, r_eCS, n_SerCS) / 2.5)
    if type == 'exponential':
        if n > 1:
            if entry[i, 0] == 1:
                s = str(i)
                mu_0 = vals['mu_0' + s]
                h = vals['h' + s]
                logf.write('Exponential component: ' + str(i + 1) + '\n')
                logf.write('\n')
                logf.write('mu_0 = ' + str(mu_0) + '\n')
                logf.write('h = ' + str(h) + '\n')
                logf.write('\n')
                model = 10.0 ** (components.exponential(x, mu_0, h) / 2.5)
        if n == 1:
            if entry[0] == 1:
                mu_0 = vals['mu_00']
                h = vals['h0']
                logf.write('Exponential component: \n')
                logf.write('\n')
                logf.write('mu_0 = ' + str(mu_0) + '\n')
                logf.write('h = ' + str(h) + '\n')
                logf.write('\n')
                model = 10.0 ** (components.exponential(x, mu_0, h) / 2.5)
    if type == 'id':
        if n > 1:
            if entry[i, 0] == 1:
                s = str(i)
                mu_0z = vals['mu_0z' + s]
                z0 = vals['z0' + s]
                case = entry[i, 6]
                logf.write('Sech component: ' + str(i + 1) + '\n')
                logf.write('\n')
                logf.write('mu_0z = ' + str(mu_0z) + '\n')
                logf.write('z0 = ' + str(z0) + '\n')
                logf.write('\n')
                model = 10.0 ** (components.id(x, mu_0z, z0, case) / 2.5)
        if n == 1:
            if entry[0] == 1:
                mu_0z = vals['mu_0z0']
                z0 = vals['z00']
                case = entry[6]
                logf.write('Sech component: \n')
                logf.write('\n')
                logf.write('mu_0z = ' + str(mu_0z) + '\n')
                logf.write('z0 = ' + str(z0) + '\n')
                logf.write('\n')
                model = 10.0 ** (components.id(x, mu_0z, z0, case) / 2.5)
    if type == 'gaussian':
        if n > 1:
            if entry[i, 0] == 1:
                s = str(i)
                mu_0r = vals['mu_0r' + s]
                r0r = vals['r0r' + s]
                fwhm_r = vals['fwhm_r' + s]
                logf.write('Gaussian component: ' + str(i + 1) + '\n')
                logf.write('\n')
                logf.write('mu_0 = ' + str(mu_0r) + '\n')
                logf.write('r_0 = ' + str(r0r) + '\n')
                logf.write('FWHM = ' + str(fwhm_r) + '\n')
                logf.write('\n')
                model = 10.0 ** (components.Gauss_ring(x, mu_0r, r0r, fwhm_r) / 2.5)
        if n == 1:
            if entry[0] == 1:
                mu_0r = vals['mu_0r0']
                r0r = vals['r0r0']
                fwhm_r = vals['fwhm_r0']
                logf.write('Gaussian component: \n')
                logf.write('\n')
                logf.write('mu_0 = ' + str(mu_0r) + '\n')
                logf.write('r_0 = ' + str(r0r) + '\n')
                logf.write('FWHM = ' + str(fwhm_r) + '\n')
                logf.write('\n')
                model = 10.0 ** (components.Gauss_ring(x, mu_0r, r0r, fwhm_r) / 2.5)
    if type == 'psf':
        if n > 1:
            if entry[i, 0] == 1:
                s = str(i)
                mu_0n = vals['mu_0p' + s]
                logf.write('PSF [nuclear] component: ' + str(i + 1) + '\n')
                logf.write('\n')
                logf.write('mu_0 = ' + str(mu_0n) + '\n')
                logf.write('\n')
                if PSF[0] == 'moffat':
                    model = 10.0 ** (components.Moffat_nuc(x, mu_0n, PSF[1], PSF[2], PSF[3]) / 2.5)
                elif PSF[0] == 'gaussian':
                    model = 10.0 ** (components.Gauss_nuc(x, mu_0n, PSF[1]) / 2.5)
                else:
                    model = 10.0 ** (components.Vector_nuc(mu_0n, PSF) / 2.5)
        if n == 1:
            if entry[0] == 1:
                mu_0n = vals['mu_0p0']
                logf.write('PSF [nuclear] component: \n')
                logf.write('\n')
                logf.write('mu_0 = ' + str(mu_0n) + '\n')
                logf.write('\n')
                if PSF[0] == 'moffat':
                    model = 10.0 ** (components.Moffat_nuc(x, mu_0n, PSF[1], PSF[2], PSF[3]) / 2.5)
                elif PSF[0] == 'gaussian':
                    model = 10.0 ** (components.Gauss_nuc(x, mu_0n, PSF[1]) / 2.5)
                else:
                    model = 10.0 ** (components.Vector_nuc(mu_0n, PSF) / 2.5)
    if type == 'ferrer':
        if n > 1:
            if entry[i, 0] == 1:
                s = str(i)
                mu_0f = vals['mu_0f' + s]
                alpha_F = vals['alpha_F' + s]
                beta_F = vals['beta_F' + s]
                r_out = vals['r_out' + s]
                logf.write('Ferrer component: ' + str(i + 1) + '\n')
                logf.write('\n')
                logf.write('mu_0 = ' + str(mu_0f) + '\n')
                logf.write('r_0 = ' + str(r_out) + '\n')
                logf.write('alpha = ' + str(alpha_F) + '\n')
                logf.write('beta = ' + str(beta_F) + '\n')
                logf.write('\n')
                model = 10.0 ** (components.Ferrer(x, mu_0f, r_out, alpha_F, beta_F) / 2.5)
                logf.write('+ [3*]  Total magnitude (analytical) = ' + str(barmag.barmagn(mu_0f, r_out, alpha_F, beta_F, x, logf)) + '\n')
        if n == 1:
            if entry[0] == 1:
                mu_0f = vals['mu_0f0']
                alpha_F = vals['alpha_F0']
                beta_F = vals['beta_F0']
                r_out = vals['r_out0']
                logf.write('Ferrer component: ' + str(i + 1) + '\n')
                logf.write('\n')
                logf.write('mu_0 = ' + str(mu_0f) + '\n')
                logf.write('r_0 = ' + str(r_out) + '\n')
                logf.write('alpha = ' + str(alpha_F) + '\n')
                logf.write('beta = ' + str(beta_F) + '\n')
                logf.write('\n')
                model = 10.0 ** (components.Ferrer(x, mu_0f, r_out, alpha_F, beta_F) / 2.5)
                logf.write('+ [3*]  Total magnitude (analytical) = ' + str(barmag.barmagn(mu_0f, r_out, alpha_F, beta_F, x, logf)) + '\n')
    if type == 'td':
        if n > 1:
            if entry[i, 0] == 1:
                s = str(i)
                mu_0td = vals['mu_0td' + s]
                r_b = vals['r_b' + s]
                h1 = vals['h1' + s]
                h2 = vals['h2' + s]
                logf.write('Truncated disc component: ' + str(i + 1) + '\n')
                logf.write('\n')
                logf.write('mu_0td = ' + str(mu_0td) + '\n')
                logf.write('r_b = ' + str(r_b) + '\n')
                logf.write('h1 = ' + str(h1) + '\n')
                logf.write('h2 = ' + str(h2) + '\n')
                logf.write('\n')
                model = 10.0 ** (components.td(x, mu_0td, r_b, h1, h2) / 2.5)
        if n == 1:
            if entry[0] == 1:
                mu_0td = vals['mu_0td0']
                r_b = vals['r_b0']
                h1 = vals['h10']
                h2 = vals['h20']
                logf.write('Truncated disc component: ' + str(i + 1) + '\n')
                logf.write('\n')
                logf.write('mu_0td = ' + str(mu_0td) + '\n')
                logf.write('r_b = ' + str(r_b) + '\n')
                logf.write('h1 = ' + str(h1) + '\n')
                logf.write('h2 = ' + str(h2) + '\n')
                logf.write('\n')
                model = 10.0 ** (components.td(x, mu_0td, r_b, h1, h2) / 2.5)
    if PSF[0] == 'gaussian':
        if type != 'psf':
            model_c = PSF_Convolution_2D.GaussianKernel(x, model, el, PSF[1])
            model = 2.5 * log10(model_c)
        else:
            model = 2.5 * log10(model)
    if PSF[0] == 'moffat':
        if type != 'psf':
            model_c = PSF_Convolution_2D.MoffatKernel(x, model, el, PSF[2], PSF[3])
            model = 2.5 * log10(model_c)
        else:
            model = 2.5 * log10(model)
    if len(PSF) > 4:
        if type != 'psf':
            model_c = PSF_Convolution_2D.NumericalKernel(x, model, el, PSF)
            model = 2.5 * log10(model_c)
        else:
            model = 2.5 * log10(model)
    return -1.0 * model