"""
A script to calculate non-parametric properties from LOSER images in 
different wavebands.

"""
import numpy as np
import os
import glob
import re
from package.convert import *
from package.loser import colormap_load
from package.casgm import Casgm

regex = re.compile(r'\d+')

data_dir = "/home/sarah/masters/data/m25n512/old_caesar/"
results_dir = '/home/sarah/masters/test/u_i/'
colormaps_dir = '/home//sarah/masters/sdss/central_galaxies/colormaps/view_1/'

masses = np.load(data_dir+'central_galaxies/masses_msun_m25n512_135.npy')
s_mass = np.load(data_dir+'central_galaxies/stellar_mass_msun_m25n512_135.npy')
r_array = np.load(data_dir+'central_galaxies/half_tot_r_kpch_m25n512_135.npy')
pos_array = np.load(data_dir+'central_galaxies/pos_kpch_m25n512_135.npy')
sfr_array = np.load(data_dir+'central_galaxies/sfr_msun_gyr_m25n512_135.npy')

sfr_array[(sfr_array < 1e-12)] = 1e-12
ssfr_array = sfr_array / s_mass

z = 0.0
h = 0.68
H_0 = 68.
dtheta = 0.5

colors = [1, 2, 3, 4, 5]

maps = sorted(glob.glob(colormaps_dir+'*'))

r_eta_array = np.zeros(len(masses))
conc_array = np.zeros(len(masses))
assym_array = np.zeros(len(masses))
clump_array = np.zeros(len(masses))
gini_array = np.zeros(len(masses))
m20_array = np.zeros(len(masses))
r_half = np.zeros(len(masses))

i = 0
for out_file in maps:
	pos = pos_array[i]
	Limage = np.float(8*r_array[i])
	radius = (r_array[i]*8)/h
	ang_bins = theta_bins(radius, z, dtheta, H_0)
	out_file = colormaps_dir + "gal_" + str(i) + '_'+ str(len(ang_bins)) + ".dat"

	u_band = colormap_load(out_file, 1, len(ang_bins))
	i_band = colormap_load(out_file, 4, len(ang_bins))

	color_data = u_band - i_band
	flux_data = magab_to_f(color_data)
	center = [(flux_data.shape[0]/2 -1), (flux_data.shape[1]/2 -1)]
	casgm_params = Casgm(flux_data, ang_bins, center)
	Casgm.find_casgm(casgm_params)

	if casgm_params.r_eta >= 5.:
		r_eta_array[i] = casgm_params.r_eta
		conc_array[i] = casgm_params.conc
		assym_array[i] = casgm_params.ass
		clump_array[i] = casgm_params.clump
		gini_array[i] = casgm_params.gini
		m20_array[i] = casgm_params.m20
		r_half[i] = casgm_params.half_r

	np.save(results_dir + 'r_eta_array.npy', r_eta_array)
	np.save(results_dir + 'conc_array.npy', conc_array)
	np.save(results_dir + 'assym_array.npy', assym_array)
	np.save(results_dir + 'clump_array.npy', clump_array)
	np.save(results_dir + 'gini_array.npy', gini_array)
	np.save(results_dir + 'm20_array.npy', m20_array)
	np.save(results_dir + 'half_r_kpch.npy', r_half)

	i +=1