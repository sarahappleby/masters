import yaml
import numpy as np
import os
from package.convert import *

hlr = 1
casgm = 1
loser = 0
profiler = 1
save_data = 1
plots = 1
candels = 0
images = 0

param_file = '/home/sarah/masters/scripts/params.dat'

with open(param_file) as p:
	params = yaml.load(p)

data_dir = params['data_dir']
results_dir = params['results_dir']
color_dir = results_dir

masses = np.load(data_dir+'central_galaxies/masses_msun_m25n512_135.npy')
s_mass = np.load(data_dir+'central_galaxies/stellar_mass_msun_m25n512_135.npy')
r_array = np.load(data_dir+'central_galaxies/half_tot_r_kpch_m25n512_135.npy')
pos_array = np.load(data_dir+'central_galaxies/pos_kpch_m25n512_135.npy')
sfr_array = np.load(data_dir+'central_galaxies/sfr_msun_gyr_m25n512_135.npy')
skid_id = np.load(data_dir+'central_galaxies/skid_id_m25n512_135.npy')

sfr_array[(sfr_array < 1e-12)] = 1e-12
ssfr_array = sfr_array / s_mass

if profiler: profiler_params = results_dir + '/sersic_params.dat'

color_index = 0
for color in [12]:

	if candels:
		deltaf = np.load(data_dir+'colors/m25n512_deltaf_'+params['candels_filters'][color_index] + '.npy')[skid_id]

	color_dir = results_dir + 'color_' + str(color) + '/'
	if not os.path.exists(color_dir): os.makedirs(color_dir)

	if profiler:
		from package.profiler_utils import *
		profile_plot_dir = color_dir +'profile_plots/'
		if not os.path.exists(profile_plot_dir): os.makedirs(profile_plot_dir)
		change_input(profiler_params, 'plot_dir', ('plot_dir = ' + profile_plot_dir +'\n'))
		profile_dir = color_dir +'profiles/'
		if not os.path.exists(profile_dir): os.makedirs(profile_dir)
		change_input(profiler_params, 'prof_dir', ('prof_dir = ' + profile_dir +'\n'))
		sersic_n_array = np.zeros_like(masses)

	if hlr: 
		half_r_array = np.zeros_like(masses)

	if casgm:
		r_eta_array = np.zeros_like(masses)
		conc_array = np.zeros_like(masses)
		assym_array = np.zeros_like(masses)
		clump_array = np.zeros_like(masses)
		gini_array = np.zeros_like(masses)
		m20_array = np.zeros_like(masses)

	for i in range(len(masses)):
		try:
			pos = pos_array[i]
			Limage = np.float(8*r_array[i])
			radius = (r_array[i]*8)/params['h']
			ang_bins = theta_bins(radius, params['z'], params['dtheta'][color_index], params['H_0'])
			out_file = params['colormaps_dir'] + "gal_" + str(i) + '_'+ str(len(ang_bins)) + ".dat"

			if loser: 
				from package.loser import run_loser
				image_name = color_dir+'images/gal_'+str(i)+'.png'
				run_loser(out_file, params, pos, Limage, ang_bins, color, image_name)

			if casgm or profiler or candels:
				from package.loser import colormap_load
				color_data = colormap_load(out_file, color, len(ang_bins))			
				flux_data = magab_to_f(color_data)

			if candels:
				noise = np.random.normal(0, deltaf[i], size=color_data.shape)
				if images:
					import matplotlib.pyplot as plt
					plt.imshow(flux_data, cmap='magma')
					plt.colorbar()
					plt.savefig(results_dir+ 'flux_images/flux_gal_'+str(i)+'.png')
					plt.clf()	
				flux_data += noise
				if images:
					import matplotlib.pyplot as plt
					plt.imshow(flux_data, cmap='magma')
					plt.colorbar()
					plt.savefig(results_dir+ 'noise_images/noise_flux_gal_'+str(i)+'.png')
					plt.clf()	

			if casgm or profiler:
				theta = r_to_theta(Limage, params['z'], float(params['H_0']))
				sa = theta_to_sa(theta)
				intens = flux_data / sa

			if casgm:
				from package.casgm import Casgm
				center = [(intens.shape[0]/2 -1), (intens.shape[1]/2 -1)]
				casgm_params = Casgm(intens, ang_bins, center)
				Casgm.find_casgm(casgm_params)
				if casgm_params.r_eta >= 5.:
					r_eta_array[i] = casgm_params.r_eta
					conc_array[i] = casgm_params.conc
					assym_array[i] = casgm_params.ass
					clump_array[i] = casgm_params.clump
					gini_array[i] = casgm_params.gini
					m20_array[i] = casgm_params.m20
					theta_half_r = arcsec_to_rad(casgm_params.half_r*params['dtheta'][color_index])
					if hlr: half_r_array[i] = theta_to_r(theta_half_r, params['z'], float(params['H_0']))

			if profiler:
				from astropy.io import ascii
				from photutils import centroid_1dg
				center = np.flip(centroid_1dg(intens), axis=0)
				profile_file = 'gal_' + str(i) + '_nbins_' + str(len(ang_bins)) + '_com'
				if not os.path.exists(profile_dir+profile_file + '.dat'):

					# closest fit using just total radius (1.5* r_petrosian)
					if casgm: 
						r_max = casgm_params.r_tot
					else:
						r_max = rad_to_arcsec(r_to_theta((r_array[i]*4)/h, z, H_0))
					bins_max = int(ang_bins[np.argmin(np.abs(ang_bins - r_max))])
					prof = get_profile(intens, ang_bins[:bins_max], center)
					ascii.write([ang_bins[:bins_max], prof], 
								(profile_dir+profile_file + '.dat'), names=['sma', 'intens'])

				change_input(profiler_params, 'data', ('data = ' + profile_file + '.dat\n'))
				change_input(profiler_params, 'plot_name', ('plot_name = ' + profile_file + '\n'))
				change_input(profiler_params, 'title', ('title = ' + 'Galaxy ' + str(i) +'\n'))
				fit, delta = run_profiler(profiler_params, sersic=True)
				if delta >= 0.2: sersic_n_array[i] = fit[0]
		except OverflowError:
			pass
	if save_data:
		from astropy.table import Table
		data = Table([ssfr_array, masses, conc_array, assym_array, clump_array, gini_array, m20_array], 
						names=('ssfr', 'mass', 'concentration', 'asymmetry', 'clumpiness', 'gini', 'm20'))
		if hlr:
			data = Table([ssfr_array, masses, conc_array, assym_array, clump_array, gini_array, m20_array, half_r_array], 
						names=('ssfr', 'mass', 'concentration', 'asymmetry', 'clumpiness', 'gini', 'm20', 'hlr'))
		elif profiler:
			data = Table([ssfr_array, masses, conc_array, assym_array, clump_array, gini_array, m20_array, sersic_n_array], 
						names=('ssfr', 'mass', 'concentration', 'asymmetry', 'clumpiness', 'gini', 'm20', 'sersic_n_array'))
		elif profiler and hlr:
			data = Table([ssfr_array, masses, conc_array, assym_array, clump_array, gini_array, m20_array, half_r_array, sersic_n_array], 
						names=('ssfr', 'mass', 'concentration', 'asymmetry', 'clumpiness', 'gini', 'm20', 'hlr', 'sersic_n_array'))
		data.write(results_dir + 'color_'+str(color)+'_data.dat')

	if plots:
		import matplotlib
		matplotlib.use('agg')
		from matplotlib import cm
		plt = matplotlib.pyplot

		if profiler:

			masses_new = masses[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)]
			r_array_new = r_array[(r_eta_array != 0) & (masses != 0) &  (sersic_n_array != 0)]
			ssfr_array_new = np.log(ssfr_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)])

		else:
			masses_new = masses[(r_eta_array != 0) & (masses != 0)]
			r_array_new = r_array[(r_eta_array != 0) & (masses != 0)]
			ssfr_array_new = np.log10(ssfr_array[(r_eta_array != 0) & (masses != 0)]*1e9)

		plt.rc('text', usetex=True)
		plt.rc('font', family='serif')

		if profiler:

			fig = plt.figure()
			ax = plt.gca()
			plt.scatter(masses_new, sersic_n_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
			plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
			plt.clim(-2, )
			ax.set_xscale('log')
			plt.xlabel(r'Mass ($M_{\odot}$)')
			plt.ylabel(r'Sersic index')
			plt.savefig(color_dir+'mass_sersic_n.png')
			plt.close()

			if hlr:

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(masses_new, half_r_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				ax.set_yscale('log')
				plt.xlabel(r'Mass ($M_{\odot}$)')
				plt.ylabel(r'Half light radius (kpc/h)')
				plt.savefig(color_dir+'mass_hlr.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(r_array_new, half_r_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				ax.set_yscale('log')
				plt.xlabel(r'Radius (kpc/h)')
				plt.ylabel(r'Half light radius (kpc/h)')
				plt.savefig(color_dir+'r_hlr.png')
				plt.close()

			if casgm:

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(masses_new, conc_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Mass ($M_{\odot}$)')
				plt.ylabel(r'Concentration index')
				plt.savefig(color_dir+'mass_conc.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(masses_new, assym_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Mass ($M_{\odot}$)')
				plt.ylabel(r'Assymetry index')
				plt.savefig(color_dir+'mass_assym.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(masses_new, clump_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Mass ($M_{\odot}$)')
				plt.ylabel(r'Clumpiness index')
				plt.savefig(color_dir+'mass_clump.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(masses_new, gini_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Mass ($M_{\odot}$)')
				plt.ylabel(r'Gini coefficient')
				plt.savefig(color_dir+'mass_gini.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(masses_new, m20_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Mass ($M_{\odot}$)')
				plt.ylabel(r'$M_{20}$')
				plt.savefig(color_dir+'mass_m20.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(r_array_new, conc_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Radius (kpc/h)')
				plt.ylabel(r'Concentration index')
				plt.savefig(color_dir+'r_conc.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(r_array_new, assym_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Radius (kpc/h)')
				plt.ylabel(r'Assymetry index')
				plt.savefig(color_dir+'r_assym.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(r_array_new, clump_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Radius (kpc/h)')
				plt.ylabel(r'Clumpiness index')
				plt.savefig(color_dir+'r_clump.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(r_array_new, gini_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Radius (kpc/h)')
				plt.ylabel(r'Gini coefficient')
				plt.savefig(color_dir+'r_gini.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(r_array_new, m20_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Radius (kpc/h)')
				plt.ylabel(r'$M_{20}$')
				plt.savefig(color_dir+'r_m20.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(m20_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], gini_array[(r_eta_array != 0) & (masses != 0) & (sersic_n_array != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				plt.gca().invert_xaxis()
				plt.xlim(0, -3)
				plt.xlabel(r'$M_{20}$')
				plt.ylabel(r'Gini coefficient')
				plt.savefig(color_dir+'m20_gini.png')
				plt.close()

		else:
				
			if hlr:

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(masses_new, half_r_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				ax.set_yscale('log')
				plt.xlabel(r'Mass ($M_{\odot}$)')
				plt.ylabel(r'Half light radius (kpc/h)')
				plt.savefig(color_dir+'mass_hlr.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(r_array_new, half_r_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				ax.set_yscale('log')
				plt.xlabel(r'Radius (kpc/h)')
				plt.ylabel(r'Half light radius (kpc/h)')
				plt.savefig(color_dir+'r_hlr.png')
				plt.close()

			if casgm:

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(masses_new, conc_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Mass ($M_{\odot}$)')
				plt.ylabel(r'Concentration index')
				plt.savefig(color_dir+'mass_conc.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(masses_new, assym_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Mass ($M_{\odot}$)')
				plt.ylabel(r'Assymetry index')
				plt.savefig(color_dir+'mass_assym.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(masses_new, clump_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Mass ($M_{\odot}$)')
				plt.ylabel(r'Clumpiness index')
				plt.savefig(color_dir+'mass_clump.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(masses_new, gini_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Mass ($M_{\odot}$)')
				plt.ylabel(r'Gini coefficient')
				plt.savefig(color_dir+'mass_gini.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(masses_new, m20_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Mass ($M_{\odot}$)')
				plt.ylabel(r'$M_{20}$')
				plt.savefig(color_dir+'mass_m20.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(r_array_new, conc_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Radius (kpc/h)')
				plt.ylabel(r'Concentration index')
				plt.savefig(color_dir+'r_conc.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(r_array_new, assym_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Radius (kpc/h)')
				plt.ylabel(r'Assymetry index')
				plt.savefig(color_dir+'r_assym.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(r_array_new, clump_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Radius (kpc/h)')
				plt.ylabel(r'Clumpiness index')
				plt.savefig(color_dir+'r_clump.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(r_array_new, gini_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Radius (kpc/h)')
				plt.ylabel(r'Gini coefficient')
				plt.savefig(color_dir+'r_gini.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(r_array_new, m20_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				ax.set_xscale('log')
				plt.xlabel(r'Radius (kpc/h)')
				plt.ylabel(r'$M_{20}$')
				plt.savefig(color_dir+'r_m20.png')
				plt.close()

				fig = plt.figure()
				ax = plt.gca()
				plt.scatter(m20_array[(r_eta_array != 0) & (masses != 0)], gini_array[(r_eta_array != 0) & (masses != 0)], s=20,c=ssfr_array_new, marker = 'o', cmap = cm.jet_r)
				plt.colorbar(label=r'log(sSFR) (Gyr$^{-1}$)')
				plt.clim(-2, )
				plt.gca().invert_xaxis()
				plt.xlim(0, -3)
				plt.xlabel(r'$M_{20}$')
				plt.ylabel(r'Gini coefficient')
				plt.savefig(color_dir+'m20_gini.png')
				plt.close()
					
	color_index +=1