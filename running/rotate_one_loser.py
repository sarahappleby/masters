"""
A script to run LOSER colormap face-on for one galaxy from a MUFASA snapshot.

"""
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from package.loser import call_loser, colormap_load
from package.convert import theta_bins
import numpy as np
import caesar
import sys
import os

num = sys.argv[1]
z = sys.argv[2]
i = int(sys.argv[3])

results_dir = '/home/sapple/rotated_'+num+'/'
data_dir = '/home/rad/data/m25n512/fh_qr/'
caesar_dir='/home/sapple/data/m25n512/'
fspsdir = "/home/rad/closer/fsps_ssp/SSP"
loser = '/home/sapple/closer/loser'
grp_file = data_dir + '/Groups/SKID/gals_0'+num+'_z'+z+'.grp'
snap = data_dir + 'snap_m25n512_'+num

obj = caesar.load(caesar_dir + 'caesar_obj_m25n512_'+num+'.hdf5')

H_0 = obj.simulation.hubble_constant * 100
omega_m = obj.simulation.omega_matter
omega_l = obj.simulation.omega_lambda
z = obj.simulation.redshift
box = '25.0'
use_absmag = '1'
use_cosmic_extinction = '1'
use_nebular = '0'
redlaw = '1'
mcode = '2'
COLORMAP = '1'
viewdir = '1'

gal = obj.central_galaxies[i]
pos = np.array(gal.pos.in_units('kpccm/h'))
#radius = float(8*gal.radii['total_half_mass'].in_units('kpccm'))
#limage = float(8*gal.radii['total_half_mass'].in_units('kpccm/h'))
limage = 4*float(gal.radii['total'].in_units('kpccm/h'))

alpha = str(gal.rotation_angles['ALPHA'])
beta = str(gal.rotation_angles['BETA'])

#npixels = str(len(theta_bins(radius, z, 0.5, H_0)))
npixels = str(int(limage * 0.68)) # a resolution of 1 kpc per pixel 

outfile = results_dir + 'colormaps/gal_'+str(i)+'_'+npixels+'.dat'

# call loser
if not os.path.exists(outfile):
    call_loser(outfile, loser, snap, grp_file, fspsdir, str(H_0), str(omega_m), str(omega_l), box, use_absmag, use_cosmic_extinction, use_nebular, redlaw, mcode, viewdir, COLORMAP, xbottom=str(pos[0]), ybottom=str(pos[1]), zbottom=str(pos[2]), Limage=str(limage), Npixels=npixels, alpha=alpha, beta=beta)

#image = colormap_load(outfile, 1, int(npixels))
#plt.imshow(image, cmap='magma_r', vmax=-0.1)
#plt.savefig(results_dir+'images/gal_'+str(i) + '_' + npixels+'.png')
#plt.clf()
