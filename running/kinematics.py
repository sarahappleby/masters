"""
A script to calculate kinematic properties from CAESAR outputs of 
Mufasa snapshots. 

Calculates bulge/total from orbital circularity parameter and from angular
momentum histogram. Also calculates velocity dispersion, transverse velocity, 
rotational kinetic energy and specific angular momentum.

"""
import caesar
import numpy as np
from readgadget import readsnap
import yt

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from matplotlib import cm
#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

size = '25'

results_dir = '/home/sapple/bulge_total/m'+size+'n512/half_r_j/'
data_dir = '/home/rad/data/m'+size+'n512/fh_qr/'
caesar_dir = '/home/sapple/data/'

grp_dir = data_dir+ 'Groups/SKID/'

snap_nos = ['095','115']
redshifts = [1.5,0.5]

for z in range(len(snap_nos)):

        snap_no = snap_nos[z]
        obj = caesar.load(caesar_dir+'caesar_obj_m'+size+'n512_'+snap_no+'.hdf5')
        h = obj.simulation.hubble_constant
        G = obj.simulation.G.in_units('km**3 / (Msun*s**2)')
        
        print "Completed caesar object linking"

        snap = data_dir + 'snap_m'+size+'n512_'+snap_no
        
        star_positions = readsnap(snap, 'pos', 'star', suppress=1, units=1)
        # note to me: i changed this mistake (multiply instead of divide)
        star_positions /= (1+redshifts[z])
        star_vels = readsnap(snap, 'vel', 'star', suppress=1, units=0)
        star_mass = readsnap(snap, 'mass', 'star', suppress=1, units=1) / h
        
        print "star properties from pygadgetreader"

        gal_mass = np.array([i.masses['total'].in_units('Msun') for i in obj.central_galaxies])
        gal_stellar_mass = np.array([i.masses['stellar'].in_units('Msun') for i in obj.central_galaxies])
        gal_sfr = np.array([i.sfr.in_units('Msun/Gyr') for i in obj.central_galaxies])
        gal_ssfr = gal_sfr / gal_mass
        gal_sigma_vel = np.array([i.velocity_dispersions['stellar'] for i in obj.central_galaxies])
        gal_star_k_rot = 0.5* gal_stellar_mass * (gal_sigma_vel**2)
        
        b_t_array_j_prj = np.zeros(len(gal_mass))
        b_t_array_j_cir = np.zeros(len(gal_mass))
        k_rot_array = np.zeros(len(gal_mass))
        star_k_rot_total = np.zeros(len(gal_mass))
        vel_t_mean_array = np.zeros(len(gal_mass))
        gal_j_array = np.zeros(len(gal_mass))
        
        for i in range(len(obj.central_galaxies)):
                gal = obj.galaxies[i]
                
                gal_pos = gal.pos.in_units('kpc/h')
                gal_vel = gal.vel.in_units('km/s')
                gal_r = gal.radii['total_half_mass'].in_units('km')
        
                slist = gal.slist
                if len(slist) >= 64:
                        gal_star_pos = star_positions[slist]
                        gal_star_vel = star_vels[slist]
                        gal_star_mass = star_mass[slist]
                        
                        rel_star_pos = gal_star_pos - np.array(gal_pos)
                        rel_star_vel = gal_star_vel - np.array(gal_vel)
                        # get position in km since velocity is km/s
                        rel_star_pos *= (h *1e3*3.08*1e13)
                        pos_mag = np.linalg.norm(rel_star_pos, axis=1)
                        mask = (pos_mag <= gal_r)
                        #rel_star_pos = rel_star_pos[mask]
                        #rel_star_vel = rel_star_vel[mask]
                        #gal_star_mass = gal_star_mass[mask]
                        #pos_mag = pos_mag[mask]
                        
                        rel_star_j = np.cross(rel_star_pos, rel_star_vel)
                        gal_star_j = np.sum(rel_star_j[mask], axis=0)
                        gal_j_array[i] = np.linalg.norm(gal_star_j)
                        gal_star_j /= gal_j_array[i]
                        
                        # B / T from projection of star's j on total j for stars
                        j_prj = np.dot(rel_star_j, gal_star_j)
                        dev = np.std(j_prj)
                        mass_mask = (j_prj > -2.5*dev) & (j_prj < 0.)
                        bulge_mass = 2*np.sum(gal_star_mass*mass_mask)
                        b_t_array_j_prj[i] = bulge_mass / np.sum(gal_star_mass)
                        
                        # K_rotational
                        star_k_rot_total[i] = np.sum(0.5 * gal_star_mass * np.power((j_prj / pos_mag), 2))
                        k_rot_array[i] = star_k_rot_total[i] / gal_star_k_rot[i]
                        
                
                        # bulge to total from j circular and j star:
                        mass_within_r = np.zeros_like(pos_mag)
                        for k in range(len(pos_mag)):
                                mass_within_r[k] = np.sum(gal_star_mass[(pos_mag <= pos_mag[k])])
                        v_cir = np.sqrt(np.float(G) * mass_within_r / pos_mag)
                        j_cir = v_cir * pos_mag
                        epsilon = j_prj / j_cir
                        cos_a = np.array(j_prj) / np.linalg.norm(rel_star_j, axis=1)
                        bulge_mass = np.sum(gal_star_mass[(epsilon < 0.5) & (cos_a < 0.7)])
                        b_t_array_j_cir[i] = bulge_mass / np.sum(gal_star_mass)

                else:
                        print 'Galaxy ' + str(i) + ': not enough particles'
       
                # find mean tangential velocity:
                # get tangential velocity of galaxy at star's position:
                gal_vel_t = np.cross(gal_star_j, rel_star_pos)
                # get unit vector in this direction:
                gal_vel_t_mag = np.linalg.norm(gal_vel_t, axis=1)
                gal_vel_t_mag = np.array((gal_vel_t_mag, gal_vel_t_mag, gal_vel_t_mag))
                unit_vel_t = gal_vel_t / np.transpose(gal_vel_t_mag)
                # magnitude of star's velocity in this direction * unit direction:
                star_vel_t_mag = np.zeros(len(unit_vel_t))
                for k in range(len(star_vel_t_mag)):
                        star_vel_t_mag[k] = np.dot(unit_vel_t[k], rel_star_vel[k])
                vel_t_mean_array[i] = np.sum(np.abs(star_vel_t_mag)) / len(star_vel_t_mag)
                
        np.save(results_dir+'sigma_vel_kms_m'+size+'n512_'+snap_no+'.npy', gal_sigma_vel)
        np.save(results_dir+'bulge_disk_j_dot_m'+size+'512_'+snap_no+'.npy', b_t_array_j_prj)
        np.save(results_dir+'bulge_disk_j_cir_m'+size+'n512_'+snap_no+'.npy', b_t_array_j_cir)
        np.save(results_dir+ 'v_t_mean_m'+size+'n512_'+snap_no+'.npy', vel_t_mean_array)
        np.save(results_dir+'k_rot_m'+size+'n512_'+snap_no+'.npy', k_rot_array)
        np.save(results_dir+'star_k_rot_total_m'+size+'n512_'+snap_no+'.npy', star_k_rot_total)
        np.save(results_dir+'j_total_stars_km2s-1_m'+size+'n512_'+snap_no+'.npy', gal_j_array)