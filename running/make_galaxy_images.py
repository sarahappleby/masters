import yt, caesar
from yt import YTArray
import package.galaxy_images
import numpy as np

data_dir = "/home/sarah/masters/caesar/test_128_bh/"
snap = "snap_m12.5n128_135.hdf5"

ds = data_dir + snap
ds = yt.load(ds)

obj = caesar.load(data_dir+'caesar_'+snap)

for i in range(10):
	gal = obj.galaxies[i]
	radius = gal.radius.in_units('kpc')
	pos = gal.pos.in_units('unitary')
	mass = gal.masses['total']
	title = 'Mass: '+'{:0.3e}'.format(int(mass)) + ' Msun;	Radius: '+ str(round(radius, 2)) + ' kpc'
	save_dir = data_dir+'gal_'+str(i)

	L = gal.angular_momentum_vector
	L_y = yt.YTArray([-1*L[1], L[0], 0])
	L_z = yt.YTArray(np.cross(L, L_y))

	# off axis projection demo
	"""
	field = 'density'
	north_vector= -1*L
	galaxy_images.off_axis_projection(ds, L, field, pos, radius, title, save_dir)
	save_dir = data_dir+'gal_'+str(i)+'_perp_north'
	galaxy_images.off_axis_projection(ds, L_y, field, pos, radius, title, save_dir, north_vector=north_vector)
	"""

	# particle plot demo
	"""
	fields_1 = ['particle_position_x', 'particle_position_y', 'particle_position_z']
	fields_2 = ['particle_position_y', 'particle_position_z', 'particle_position_x']
	for ax in range(3):
		particle_plot(ds, fields_1[ax], fields_2[ax], ('PartType4', 'particle_mass'), pos, radius, title, save_dir)
	"""

	# multi-panel plot demo
	axes = [L_y, L_z, L]
	norths = [L, L_y, L_z]
	labels = ['density']*4
	field = 'density'
	images = []
	for i in range(3):
		images.append(galaxy_images.off_axis_projection(ds, axes[i], field, pos, radius, title, save=False, north_vector=norths[i]))
	images.append(galaxy_images.axis_projection(ds, 'x', field, pos, radius, title, save=False, vel_arrows=True))
	#galaxy_images.multi_panel_plot(images, labels, radius, save_dir)