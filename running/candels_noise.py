"""
A script to calculate photometric noise Gaussian widths (sigma) for CANDELS-like images.
Uses output from LOSER and finds sigma for each galaxy. 
Based on the CANDELizer pipeline:
https://github.com/yotamcohen/CANDELizing_pipeline

"""

import numpy as np 
from pandas import HDFStore
from scipy.optimize import curve_fit
import pickle
from package.convert import magab_to_f

def linfit(x,m,b):
    return m*x + b

def fit(x,y):
    popt,pcov = curve_fit(linfit,x,y)
    perr = np.sqrt(np.diag(pcov))
    return popt,perr

if __name__ == "__main__":

	candels_dir = '/home/sarah/masters/CANDELizing_pipeline/'

	store = HDFStore(candels_dir+ 'lib/CANDELS/photometry_fluxes_magnitudes.h5')
	main_cdf = store.dat
	store.close()

	with open(candels_dir + 'lib/CANDELS_noise_bands.txt') as f:
		ccols = f.readlines()
	ccols = [x.rstrip('\n') for x in ccols]

	data = {}
	data['filters'] = ccols
	data['m_mean'] = np.zeros(len(ccols))
	data['b_mean'] = np.zeros(len(ccols))
	data['sigma_m_mean'] = np.zeros(len(ccols))
	data['sigma_b_mean'] = np.zeros(len(ccols))

	for i in range(len(ccols)):
		ccol = ccols[i]
		cflux,cfluxerr = ccol+'_flux',ccol+'_fluxerr'
		cmag,cmagerr = ccol+'_mag',ccol+'_magerr'

		notnans = (~np.isnan(main_cdf[cmag]))&(~np.isnan(main_cdf[cmagerr]))
		notinfs = (~np.isinf(main_cdf[cmag]))&(~np.isinf(main_cdf[cmagerr]))
		cdf = main_cdf[(notnans) & (notinfs)]	

		popt,perr = fit(cdf[cmag],np.log10(cdf[cfluxerr]/cdf[cflux]))
		data['m_mean'][i] = popt[0]
		data['b_mean'][i] = popt[1]
		data['sigma_m_mean'][i] = perr[0]
		data['sigma_b_mean'][i] = perr[1]

	with open('/home/sarah/masters/CANDELizing_pipeline/candels_noise_fits.p', 'wb') as p:
		pickle.dump(data, p)

	loser_data = np.loadtxt('/home/sarah/masters/data/colors/gal_z0.00.m25n512.fh_qr.abs')

	colors = [12, 13, 14, 15, 16]
	candels_colors = [1, 3, 6, 7, 8]

	for i in range(len(colors)):
		mag_data = loser_data[:, 11+colors[i]]
		candels_color = candels_colors[i]

		b_mean = data['b_mean'][candels_color]
		m_mean = data['m_mean'][candels_color]
		sigma_b_mean = data['sigma_b_mean'][candels_color]
		sigma_m_mean = data['sigma_m_mean'][candels_color]

		fit_value = 10**(m_mean*mag_data + b_mean)
		flux_data = magab_to_f(mag_data)
		deltaf = fit_value*flux_data

		np.save('/home/sarah/masters/data/colors/m12.5n128_deltaf_'+ data['filters'][candels_color] +'.npy', deltaf)