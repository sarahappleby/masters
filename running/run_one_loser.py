"""
A script to run LOSER colormap on one galaxy from a MUFASA snapshot.
"""
import os
import yt
from package.loser import *
from package.convert import *
import sys

i = int(sys.argv[1])

loser_dir = '/home/sapple/closer/loser'
data_dir = "/home/sapple/data/"
snap = "snap_m25n512_135"
grp = data_dir + "gals_0135_z0.000.grp"
fsps_dir = "/home/rad/closer/fsps_ssp/SSP"
results_dir = '/home/sapple/sdss/central_galaxies/'

ds = yt.load(data_dir + snap + '.hdf5')
h = ds.cosmology.hubble_constant
H_0 = np.float(ds.cosmology.hubble_constant) * 100
omega_matter = ds.cosmology.omega_matter
omega_lambda = ds.cosmology.omega_lambda

box = '25.0'
use_absmag = '1'
use_cosmic_extinction = '1'
use_nebular = '0'
redlaw = '1'
mcode = '2'
COLORMAP = '1'
j = '1'

colors = [1, 2, 3, 4, 5]
dtheta = [0.5, 0.5, 0.5, 0.5, 0.5]
z = 0
index = 0

r_array = np.load(data_dir+'central_galaxies/half_tot_r_kpch_m25n512_135.npy')
pos_array = np.load(data_dir+'central_galaxies/pos_kpch_m25n512_135.npy')

Limage = np.float(8*r_array[i])
radius = (r_array[i]*8)/h
ang_bins = theta_bins(radius, z, dtheta[index], H_0)
pos = pos_array[i]

print('Number of bins: ' + str(len(ang_bins)))

out_file = results_dir + "colormaps/view_"+j+"/gal_" + str(i) + '_'+ str(len(ang_bins)) + ".dat"
if not os.path.exists(out_file):
	call_loser(out_file, loser_dir, data_dir+snap, grp, fsps_dir, str(H_0), str(omega_matter), str(omega_lambda), box,
		   use_absmag, use_cosmic_extinction, use_nebular, redlaw, mcode, j, COLORMAP, 
		   xbottom=str(pos[0]), ybottom=str(pos[1]), zbottom=str(pos[2]), Limage=str(Limage), Npixels=str(len(ang_bins)))
