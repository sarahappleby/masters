"""
A script to run CAESAR on a MUFASA snapshot and save the properties needed as .npy binary files.

"""
import yt
import caesar
import numpy as np 

caesar_dir='/home/sapple/data/m25n512/'
snap_nos = ['135', '105', '085', '070']
results_dir = '/home/sapple/data/m25n512/'

for snap in snap_nos:
    
    obj = caesar.load(caesar_dir+'caesar_obj_m25n512_'+snap+'.hdf5')

    skidid = np.array([i.skidID for i in obj.central_galaxies])
    np.save(results_dir+'/central_galaxies/skid_id_m25n512_'+snap+'.npy', skidid)
    skidid = np.array([i.skidID for i in obj.galaxies])
    np.save(results_dir+'/galaxies/skid_id_m25n512_'+snap+'.npy', skidid)
  
    stellar_mass = np.array([i.masses['stellar'] for i in obj.galaxies])
    np.save(results_dir + 'galaxies/mass_stellar_msun_m25n512_' + snap + '.npy', stellar_mass)
    stellar_mass = np.array([i.masses['stellar'] for i in obj.central_galaxies])
    np.save(results_dir + 'central_galaxies/mass_stellar_msun_m25n512_' + snap + '.npy', stellar_mass)
    
    masses = np.array([i.masses['total'] for i in obj.galaxies])
    np.save(results_dir+'galaxies/mass_total_msun_m25n512_'+snap+'.npy', masses)
    masses = np.array([i.masses['total'] for i in obj.central_galaxies])
    np.save(results_dir+'central_galaxies/mass_total_msun_m25n512_'+snap+'.npy', masses)
        
    gals_half_stel_r = np.array([i.radii['stellar_half_mass'].in_units('kpc/h') for i in obj.galaxies])
    np.save(results_dir+'galaxies/half_stel_r_kpch_m25n512_'+ snap +'.npy', gals_half_stel_r)
    gals_half_stel_r = np.array([i.radii['stellar_half_mass'].in_units('kpc/h') for i in obj.central_galaxies])
    np.save(results_dir+'central_galaxies/half_stel_r_kpch_m25n512_'+ snap +'.npy', gals_half_stel_r)

    gals_half_tot_r = np.array([i.radii['total_half_mass'].in_units('kpc/h') for i in obj.galaxies])
    np.save(results_dir+'galaxies/half_tot_r_kpch_m25n512_'+ snap +'.npy', gals_half_tot_r)    
    gals_half_tot_r = np.array([i.radii['total_half_mass'].in_units('kpc/h') for i in obj.central_galaxies])
    np.save(results_dir+'central_galaxies/half_tot_r_kpch_m25n512_'+ snap +'.npy', gals_half_tot_r)

    gals_tot_r = np.array([i.radii['total'].in_units('kpc/h') for i in obj.galaxies])
    np.save(results_dir+'galaxies/tot_r_kpch_m25n512_'+ snap +'.npy', gals_tot_r)    
    gals_tot_r = np.array([i.radii['total'].in_units('kpc/h') for i in obj.central_galaxies])
    np.save(results_dir+'central_galaxies/tot_r_kpch_m25n512_'+ snap +'.npy', gals_tot_r)

    gals_pos = np.array([i.pos.in_units('kpc/h') for i in obj.galaxies])
    np.save(results_dir+'galaxies/pos_kpch_m25n512_'+ snap +'.npy', gals_pos)    
    gals_pos = np.array([i.pos.in_units('kpc/h') for i in obj.central_galaxies])
    np.save(results_dir+'central_galaxies/pos_kpch_m25n512_'+ snap +'.npy', gals_pos)

    gals_sfr = np.array([i.sfr.in_units('Msun/Gyr') for i in obj.galaxies])
    np.save(results_dir+'galaxies/sfr_msun_gyr_kpch_m25n512_'+ snap +'.npy', gals_sfr)    
    gals_sfr = np.array([i.sfr.in_units('Msun/Gyr') for i in obj.central_galaxies])
    np.save(results_dir+'central_galaxies/sfr_msun_gyr_m25n512_'+ snap +'.npy', gals_sfr)
