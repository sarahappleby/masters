"""
A script to add noise to and smooth selected faceon LOSER images of galaxies.
Images are saved in FITS format for use with Imfit.

"""
from package.loser import *
from package.convert import *
import numpy as np 
import matplotlib.pyplot as plt
from astropy.convolution import convolve, Gaussian2DKernel

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

data_dir = '/home/sarah/masters/rotated_galaxies/'
results_dir = '/home/sarah/masters/rotated_galaxies/rotated_135/'

#deltaf = np.load(data_dir +'colors/m12.5n128_deltaf_acs_f606w_v08.npy')
#skidid = np.load(data_dir +'/central_galaxies/skid_id_m25n512_135.npy')
#radii = np.load(data_dir + '/central_galaxies/half_tot_r_kpch_m25n512_135.npy')

dtheta = 0.5
z = 0.0
H_0 = 68.0
h = 0.68

gals = ['7']
npixels = ['74']

# for snap 085:
#gals = ['8', '44', '77', '121', '144'] # disk galaxies
#gals = ['5', '14', '18', '20', '21', '27', '30', '33', '42'] # spheroid galaxies
#gals = ['0', '1', '3', '7', '11', '13'] # spheroids with disk features
# corresponding npixels:
#npixels = ['91', '72', '49', '44', '54'] # disk galaxies
#npixels = ['69', '75', '72', '83', '90', '114', '85', '88', '86'] # spheroid galaxies
#npixels = ['106', '67', '67', '57', '89', '73'] # spheroids with disk features

# for snap 095:
#gals = ['7', '11', '16', '17', '27', '31', '37', '54', '95', '104', '105', '110'] # disk galaxies
#gals = ['10', '19', '21', '22', '24', '28', '33', '40', '44'] # spheroid galaxies
#gals = ['0', '1', '5', '7', '12', '26', '29', '30', '32', '36', '58', '66'] # spheroids with disk features
# corresponding npixels:
#npixels = ['74', '105', '78', '92', '69', '80', '109', '57', '60', '81', '59', '97'] # disk galaxies
#npixels = ['75', '96', '75', '93', '51', '69', '118', '72', '85'] # spheroid galaxies
#npixels = ['98', '77', '94', '74', '68', '80', '90', '88', '72', '77', '41', '51'] # spheroids with disk features

# for snap 105:
#gals = ['10', '40', '46', '47', '72', '73', '77', '83', '124', '179'] # disk galaxies
#gals = ['5', '6', '7', '8', '13', '16', '17', '26'] # spheroid galaxies
#gals = ['0', '1', '2', '4', '14', '22', '24', '27', '30', '52', '56', '59', '70'] # spheroids with disk features
# corresponding npixels:
#npixels = ['87', '107', '53', '73', '68', '105', '64', '79', '71', '51'] # disk galaxies
#npixels = ['102', '94', '113', '83', '59', '88', '107', '86'] # spheroid galaxies
#npixels = ['103', '101', '90', '114', '71', '95', '70', '93', '68', '86', '100', '87', '63'] # spheroids with disk features

# for snap 115:
#gals = ['60', '69', '106', '107', '156', '175'] # disk galaxies
#gals = ['0', '2', '4', '6', '10', '11', '14', '18', '19'] # spheroid galaxies
#gals = ['1', '20', '31', '37', '43', '48', '53', '63', '81'] # spheroids with disk features
# corresponding npixels:
#npixels = ['80', '84', '93', '39', '72', '64'] # disk galaxies
#npixels = ['105', '116', '101', '88', '72', '82', '77', '133', '91'] # spheroid galaxies
#npixels = ['106', '94', '113', '83', '88', '118', '84', '59', '92'] # spheroids with disk features

# for snap 135:
gals = ['39', '15', '65']
npixels = ['123', '105', '107']

noise = 0.0
smooth = 1.0
r_scale = 0.0

if r_scale:
	angle = arcsec_to_rad(dtheta)
	bin_scale = theta_to_r(angle, z, H_0) # physical bin size in kpc

if smooth:
	epsilon = 0.25 * h # kpc
	e_size = rad_to_arcsec((r_to_theta(epsilon, z, H_0))) / dtheta # pixels
	kernel = Gaussian2DKernel(stddev=e_size)

for i in range(len(gals)):
	outfile = data_dir + 'colormaps/gal_'+gals[i]+'_'+npixels[i]+'.dat'
	image = colormap_load(outfile, 1, int(npixels[i]))
	plt.imshow(image, cmap='magma_r', vmax=-0.1)
	plt.colorbar(label=r'AB magnitude')
	plt.xlabel('kpc')
	plt.ylabel('kpc')
	plt.savefig(results_dir+'decompose/images/gal_'+gals[i] + '_' + npixels[i]+'.png')
	plt.clf()

	fitsname = results_dir + 'decompose/fits/gal_'+gals[i] + '_' + npixels[i] + '.fits'
	if not os.path.exists(fitsname): save_as_fits(image, fitsname)

	image_f = magab_to_f(image)
	value = np.min(np.abs(image_f[image_f != 0.0]))
	image_f[(image_f <= 0.0)] = value

	vmax = np.mean(image_f) + 2*np.std(image_f)
	plt.imshow(image_f, cmap='magma', vmax=vmax)
	plt.colorbar(label=r'Flux (Wm$^{-2}$Hz$^{-1}$)')
	plt.xlabel('kpc')
	plt.ylabel('kpc')
	plt.savefig(results_dir+'decompose/images/gal_'+gals[i] + '_' + npixels[i] + '_flux.png')
	plt.clf()
	fitsname = results_dir + 'decompose/fits/gal_'+gals[i] + '_' + npixels[i] + '_flux.fits'
	if not os.path.exists(fitsname) : save_as_fits(image_f, fitsname)

	if noise:
		noise = np.random.normal(0.0, deltaf[skidid[int(gals[i])]], image_f.shape)
		image_f += noise
		plt.imshow(noise, cmap='magma')
		plt.colorbar(label=r'Flux (Wm$^{-2}$Hz$^{-1}$)')
		plt.xlabel('kpc')
		plt.ylabel('kpc')
		plt.savefig(results_dir+'decompose/images/gal_'+gals[i] + '_' + npixels[i] + '_noise.png')
		plt.clf()
		plt.imshow(image_f, cmap='magma', vmax=vmax)
		plt.colorbar(label=r'Flux (Wm$^{-2}$Hz$^{-1}$)')
		plt.xlabel('kpc')
		plt.ylabel('kpc')
		plt.savefig(results_dir+'decompose/images/gal_'+gals[i] + '_' + npixels[i] + '_flux_noise.png')
		plt.clf()
		fitsname = results_dir + 'decompose/fits/gal_'+gals[i] + '_' + npixels[i] + '_noise.fits'
		if not os.path.exists(fitsname) : save_as_fits(noise, fitsname)
		fitsname = results_dir + 'decompose/fits/gal_'+gals[i] + '_' +	 npixels[i] + '_flux_noise.fits'
		if not os.path.exists(fitsname) : save_as_fits(image_f, fitsname)
	
	if smooth:
		image_f = convolve(image_f, kernel)
		plt.imshow(image_f, cmap='magma')
		plt.colorbar(label=r'Flux (Wm$^{-2}$Hz$^{-1}$)')
		plt.xlabel('kpc')
		plt.ylabel('kpc')
		plt.savefig(results_dir+'decompose/images/gal_'+gals[i] + '_' + npixels[i] + '_flux_smooth.png')
		plt.clf()
		fitsname = results_dir + 'decompose/fits/gal_'+gals[i] + '_' + npixels[i] + '_flux_smooth.fits'
		if not os.path.exists(fitsname) : save_as_fits(image_f, fitsname)
		image_m_smooth = f_to_magab(image_f)
		plt.imshow(image_m_smooth, cmap='magma_r', vmax=vmax)
		plt.colorbar(label=r'AB magnitude')
		plt.xlabel('kpc')
		plt.ylabel('kpc')
		plt.savefig(results_dir+'decompose/images/gal_'+gals[i] + '_' + npixels[i] + '_mag_smooth.png')
		plt.clf()


	#theta = r_to_theta(np.float(radii[gals[i]]), z, H_0)
	#sa = theta_to_sa(theta)
	#intens = image_f / sa
	#fitsname = results_dir + 'decompose/fits/gal_'+str(gals[i]) + '_' + npixels[i] + '_intens.fits'
	#if not os.path.exists(fitsname) : save_as_fits(intens, fitsname)
