# run loser for whole galaxies for galaxy colours

import os
from package.loser import *

loser_dir = '/home/sapple/closer/loser'
data_dir = "/home/sapple/data/m25n512/"
snap = "snap_m25n512_135"
grp = data_dir + "gals_0135_z0.000.grp"
fsps_dir = "/home/rad/closer/fsps_ssp/SSP"
results_dir = '/home/sapple/sdss/central_galaxies/'

H_0 = '68.0'
omega_matter = '0.3'
omega_lambda = '0.7'

box = '25.0'
use_absmag = '1'
use_cosmic_extinction = '1'
use_nebular = '0'
redlaw = '1'
mcode = '2'
COLORMAP = '0'
j = '1'

out_file = results_dir + 'm25n512_loser_output.dat'

if not os.path.exists(out_file):
            call_loser(out_file, loser_dir, data_dir+snap, grp, fsps_dir, H_0, omega_matter, omega_lambda, box,
                                          use_absmag, use_cosmic_extinction, use_nebular, redlaw, mcode, j, COLORMAP)
