import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.colors as colors

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
cmap = plt.get_cmap('jet_r')
new_cmap = truncate_colormap(cmap, 0.1, 0.9)

size = '50'

data_dir = '/home/sarah/masters/data/m'+size+'n512/'

#subtract 1 from skid ids since loser is 1 based not 0 based
skid_id = np.load(data_dir+'central_galaxies/skid_id_m'+size+'n512_135.npy') - 1
s_mass = np.load(data_dir+'central_galaxies/mass_stellar_msun_m'+size+'n512_135.npy')
sfr = np.load(data_dir+'central_galaxies/sfr_msun_gyr_m'+size+'n512_135.npy')
sfr[(sfr < 1e-12)] = 1e-12                                 
ssfr = sfr / s_mass  

loser_data = np.loadtxt(data_dir+'m'+size+'n512_loser_output.dat')
loser_sfr = loser_data[:, 2] * 1e9
loser_sfr[(loser_sfr < 1e-12)] = 1e-12
loser_s_mass = loser_data[:, 5]
loser_ssfr = loser_sfr / loser_s_mass
u = loser_data[:, 11+1]
r = loser_data[:, 11+3]
u_r = u - r
blue = (u_r[skid_id] <= -0.375 + 0.25*np.log10(loser_s_mass[skid_id]))
np.save('/home/sarah/masters/data/m'+size+'n512/central_galaxies/blue_true_m'+size+'n512_135.npy', blue)

mass_bins = np.logspace(8, 12, num=20)
y = -0.375 + 0.25*np.log10(mass_bins)

plt.scatter(loser_s_mass, u_r, s=5, c=np.log10(loser_ssfr), cmap=new_cmap)
plt.plot(mass_bins, y, linewidth=2, linestyle='--', c='k')
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2.7, 0.3)
plt.xlim(1e9, )
plt.ylim(0, )
plt.xscale('log')
plt.xlabel(r'Stellar Mass (M$_\odot$)')
plt.ylabel(r'u - r')
plt.savefig('/home/sarah/masters/coursework/report/results/loser_red_sequence_m'+size+'n512.png')
plt.clf()