import matplotlib.pyplot as plt
import numpy as np 
from package.subdivide import *
import matplotlib.colors as colors

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
cmap = plt.get_cmap('jet_r')
new_cmap = truncate_colormap(cmap, 0.1, 0.9)


plt.rc('text', usetex=True)
plt.rc('font', family='serif')

size = '50'
num_size = 50000

data_dir = '/home/sarah/masters/data/m'+size+'n512/central_galaxies/'
b_t_dir = '/home/sarah/masters/bulge_total/m'+size+'n512/half_r_j/'
results_dir = '/home/sarah/masters/bulge_total/m'+size+'n512/half_r_j/'
results_dir = '/home/sarah/masters/coursework/report/results/'
snaps = ['085', '095', '105', '115', '135']
labels = ['z = 2.0', 'z = 1.5', 'z = 1.0', 'z = 0.5', 'z = 0.0']
labels = ['t = 3.3 Gyr', 't = 4.3 Gyr', 't = 5.9 Gyr', 't = 8.6 Gyr', 't = 13.7 Gyr']

colors = ['c', 'g', 'r', 'tab:purple', 'b']


for snap in snaps:

	mass = np.load(data_dir+'mass_total_msun_m'+size+'n512_'+ snap +'.npy')
	s_mass = np.load(data_dir+'mass_stellar_msun_m'+size+'n512_'+snap+'.npy')
	sfr = np.load(data_dir+'sfr_msun_gyr_m'+size+'n512_'+snap+'.npy')
	j = np.load(b_t_dir+'j_total_stars_km2s-1_m'+size+'n512_'+snap+'.npy')
	b_t_cir = np.load(b_t_dir+'bulge_disk_j_cir_m'+size+'n512_'+snap+'.npy')
	b_t_prj = np.load(b_t_dir+'bulge_disk_j_dot_m'+size+'n512_'+snap+'.npy')
	k_rot = np.load(b_t_dir+'k_rot_m'+size+'n512_' + snap+'.npy')
	v_t = np.load(b_t_dir+'v_t_mean_m'+size+'n512_'+snap+'.npy')
	sigma = np.load(b_t_dir+'sigma_vel_kms_m'+size+'n512_'+snap+'.npy')

	sfr[(sfr < 1e-12)] = 1e-12
	ssfr = sfr / s_mass
	v_sigma = v_t / sigma

	sizes = np.log10(mass)


	# 2d histogram plots of B/T, K_rot, V/sigma for each redshift
	"""
	hist1 = np.histogram2d(b_t_cir,np.log10(k_rot), bins=40, weights=np.log10(ssfr), normed=False)[0]
	hist2, xedge, yedge = np.histogram2d(b_t_cir, np.log10(k_rot), bins=40, normed=False)
	hist3 = hist1 / hist2
	x, y = np.meshgrid(xedge, yedge)
	plt.pcolormesh(x, y, hist3, cmap=new_cmap)
	plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
	plt.clim(-2, 0.3)
	plt.xlabel(r'B/T')
	plt.ylabel(r'log(K$_\mathrm{rot}$)')
	plt.savefig(results_dir+'btcir_krot_wssfr_hist_'+ snap+'.png')
	plt.clf()
	plt.pcolormesh(x, y, hist1, cmap='viridis_r')
	plt.colorbar()
	plt.xlabel(r'B/T')
	plt.ylabel(r'log(K$_\mathrm{rot}$)')
	plt.savefig(results_dir+'btcir_krot_hist_'+ snap+'.png')
	plt.clf()
	plt.pcolormesh(x, y, hist2, cmap='viridis')
	plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
	plt.xlabel(r'B/T')
	plt.ylabel(r'log(K$_\mathrm{rot}$)')
	plt.savefig(results_dir+'btcir_krot_ssfr_hist_'+ snap+'.png')
	plt.clf()


	hist1 = np.histogram2d(b_t_cir,v_sigma, bins=40, weights=np.log10(ssfr), normed=False)[0]
	hist2, xedge, yedge = np.histogram2d(b_t_cir, v_sigma, bins=40, normed=False)
	hist3 = hist1 / hist2
	x, y = np.meshgrid(xedge, yedge)
	plt.pcolormesh(x, y, hist3, cmap=new_cmap)
	plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
	plt.clim(-2, 0.3)
	plt.xlabel(r'B/T')
	plt.ylabel(r'V/$\sigma$')
	plt.savefig(results_dir+'btcir_vsigma_wssfr_hist_'+ snap+'.png')
	plt.clf()
	plt.pcolormesh(x, y, hist1, cmap='viridis_r')
	plt.colorbar()
	plt.xlabel(r'B/T')
	plt.ylabel(r'V/$\sigma$')
	plt.savefig(results_dir+'btcir_vsigma_hist_'+ snap+'.png')
	plt.clf()
	plt.pcolormesh(x, y, hist2, cmap='viridis')
	plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
	plt.xlabel(r'B/T')
	plt.ylabel(r'V/$\sigma$')
	plt.savefig(results_dir+'btcir_vsigma_ssfr_hist_'+ snap+'.png')
	plt.clf()

	# scatter plots for each redshift
	"""
	fig = plt.figure()
	ax = plt.gca()
	plt.scatter(mass, b_t_cir, s=sizes,c=np.log10(ssfr), marker = 'o', cmap =new_cmap)
	plt.colorbar(label=r'sSFR (Gyr$^{-1}$)')
	plt.clim(-2, 0.3)
	ax.set_xscale('log')
	plt.xlim(1e9, )
	plt.xlabel(r'Mass ($M_{\odot}$)')
	plt.ylabel(r'B/T')
	plt.savefig(results_dir+'mass_btcir_'+snap+'.png')
	plt.close()
	"""
	fig = plt.figure()
	ax = plt.gca()
	plt.scatter(mass, b_t_prj, s=sizes,c=np.log10(ssfr), marker = 'o', cmap =new_cmap)
	plt.colorbar(label=r'sSFR (Gyr$^{-1}$)')
	plt.clim(-2, 0.3)
	ax.set_xscale('log')
	plt.xlabel(r'Mass ($M_{\odot}$)')
	plt.ylabel(r'B/T')
	plt.savefig(results_dir+'mass_btprj_'+snap+'.png')
	plt.close()


	fig = plt.figure()
	ax = plt.gca()
	plt.scatter(b_t_cir, k_rot, s=sizes,c=np.log10(ssfr), marker = 'o', cmap =new_cmap)
	plt.colorbar(label=r'sSFR (Gyr$^{-1}$)')
	plt.clim(-2, 0.3)
	ax.set_yscale('log')
	plt.xlabel(r'B/T')
	plt.ylabel(r'K$_\mathrm{rot}$')
	plt.savefig(results_dir+'btcir_krot_'+snap+'.png')
	plt.close()

	fig = plt.figure()
	ax = plt.gca()
	plt.scatter(b_t_cir, v_sigma, s=sizes,c=np.log10(ssfr), marker = 'o', cmap = new_cmap)
	plt.colorbar(label=r'sSFR (Gyr$^{-1}$)')
	plt.clim(-2, 0.3)
	plt.xlabel(r'B/T')
	plt.ylabel(r'V/$\sigma$')
	plt.savefig(results_dir+'btcir_vsigma_'+snap+'.png')
	plt.close()

	plt.scatter(s_mass, j, s=sizes,c=np.log10(ssfr), marker = 'o', cmap = new_cmap)
	plt.colorbar(label=r'sSFR (Gyr$^{-1}$)')
	plt.clim(-2, 0.3)
	plt.xscale('log')
	plt.yscale('log')
	plt.ylim(1e19, 1e24)
	plt.xlim(9e9, )
	plt.xlabel(r'Stellar Mass ($M_{\odot}$)')
	plt.ylabel(r'Specific Angular Momentum (km$^2$s$^{-1}$)')
	plt.savefig(results_dir+'smass_j_'+snap+'.png')
	plt.close()
"""
# plot showing blue or red galaxy curves for each redshift
fig = plt.figure()
ax = plt.gca()
for i in range(len(snaps)):
	mass_bins = np.logspace(8, 12, num=20)
	mass = np.load(data_dir+'mass_total_msun_m'+size+'n512_'+ snaps[i]+'.npy')
	s_mass = np.load(data_dir+'mass_stellar_msun_m'+size+'n512_'+snaps[i]+'.npy')
	pos = np.load(data_dir+'pos_kpch_m'+size+'n512_'+snaps[i]+'.npy')
	
	#skidid = np.load(data_dir + 'skid_id_m25n512_'+snaps[i] +'.npy')
	#u_r = np.load('/home/sarah/masters/data/colors/sdss_u_r_dustfree_m25n512_'+snaps[i]+'.npy')[skidid - 1]
	#blue_mask = (u_r < (-0.375 +0.25*np.log10(s_mass)))

	b_t_prj = np.load(b_t_dir+'bulge_disk_j_cir_m'+size+'n512_'+snaps[i]+'.npy')
	b_t_prj[b_t_prj > 1.0] = 1.0
	mass_bins, b_t_mean, b_t_var, b_t_per = jackknife(pos, s_mass, b_t_prj, num_size, mass_bins, std=True)
	ax.errorbar(mass_bins, b_t_mean, yerr=b_t_var, marker='.', markersize=10, linestyle='--', label=labels[i], c=colors[i])
	#plt.fill_between(mass_bins, b_t_per[2], b_t_per[1], facecolor=colors[i], alpha=0.3)
	#top = b_t_mean + b_t_std; bottom = b_t_mean - b_t_std
	#plt.fill_between(mass_bins, bottom, top, facecolor="none", hatch="//", edgecolor=colors[i], linewidth=0.0)
	#plt.fill_between(mass_bins, bottom, top, facecolor=colors[i], alpha=0.3)
plt.xlabel(r'Stellar Mass (M$_\odot$)')
plt.ylabel('B / T')
plt.xlim(1e9, )
plt.legend()
plt.xscale('log')
plt.savefig(results_dir+ 'm'+size+'n512_btcir_mass.png')
plt.clf() 
"""
fig = plt.figure()
ax = plt.gca()
for i in range(len(snaps)):
	mass_bins = np.logspace(8, 12, num=20)
	j = np.load(b_t_dir+'j_total_stars_km2s-1_m'+size+'n512_'+snaps[i]+'.npy')
	s_mass = np.load(data_dir+'mass_stellar_msun_m'+size+'n512_'+snaps[i]+'.npy')
	pos = np.load(data_dir+'pos_kpch_m'+size+'n512_'+snaps[i]+'.npy')
	mass_bins, j_mean, j_var, j_per = jackknife(pos, s_mass, j, num_size, mass_bins, std=True)
	plt.plot(mass_bins, j_mean, marker='.', markersize=10, linestyle='--', label=labels[i], c=colors[i])
	#ax.errorbar(mass_bins, j_mean, yerr=j_var, marker='.', markersize=10, linestyle='--', label=labels[i], c=colors[i])
plt.xlabel(r'Stellar Mass (M$_\odot$)')
plt.ylabel(r'Specific Angular Momentum (km$^2$s$^{-1}$)')
plt.legend()
plt.yscale('log')
plt.xscale('log')
plt.savefig(results_dir+ 'm'+size+'n512_j_mass.png')
plt.clf() 

# plot showing blue or red galaxy curves for each redshift
fig = plt.figure()
ax = plt.gca()
for i in range(len(snaps)):
	b_t_bins = np.arange(0, 1, 0.05)
	pos = np.load(data_dir+'pos_kpch_m'+size+'n512_'+snaps[i]+'.npy')
	k_rot = np.load(b_t_dir+'k_rot_m'+size+'n512_' + snaps[i]+'.npy')
	b_t_prj = np.load(b_t_dir+'bulge_disk_j_cir_m'+size+'n512_'+snaps[i]+'.npy')
	b_t_prj[b_t_prj > 1.0] = 1.0
	b_t_bins, k_rot_mean, k_rot_var, k_rot_per = jackknife(pos, b_t_prj, k_rot, num_size, b_t_bins, std=True)
	ax.errorbar(b_t_bins, k_rot_mean, yerr=k_rot_var, marker='.', markersize=10, linestyle='--', label=labels[i], c=colors[i])
plt.xlabel('B / T')
plt.ylabel(r'K$_{rot}$')
plt.xlim(0.3, )
if size == '50':
	plt.ylim(0, 20)
	plt.legend(loc=1)
plt.savefig(results_dir+ 'm'+size+'n512_btcit_krot.png')
plt.clf() 

fig = plt.figure()
ax = plt.gca()
for i in range(len(snaps)):
	b_t_bins = np.arange(0, 1, 0.05)
	pos = np.load(data_dir+'pos_kpch_m'+size+'n512_'+snaps[i]+'.npy')
	v_t = np.load(b_t_dir+'v_t_mean_m'+size+'n512_'+snaps[i]+'.npy')
	sigma = np.load(b_t_dir+'sigma_vel_kms_m'+size+'n512_'+snaps[i]+'.npy')
	v_sigma = v_t / sigma
	b_t_prj = np.load(b_t_dir+'bulge_disk_j_cir_m'+size+'n512_'+snaps[i]+'.npy')
	b_t_prj[b_t_prj > 1.0] = 1.0
	b_t_bins, v_sigma_mean, v_sigma_var, v_sigma_per = jackknife(pos, b_t_prj, v_sigma, num_size, b_t_bins, std=True)
	ax.errorbar(b_t_bins, v_sigma_mean, yerr=v_sigma_var, marker='.', markersize=10, linestyle='--', label=labels[i], c=colors[i])
plt.xlabel('B / T')
plt.ylabel(r'V / $\sigma$')
plt.xlim(0.3, )
plt.legend()
plt.ylim(0.5, 3.5)
plt.savefig(results_dir+ 'm'+size+'n512_btcir_vsigma.png')
plt.clf() 


# plot showing red curve, blue curve, total curve for final redshift
mass = np.load(data_dir+'mass_total_msun_m'+size+'n512_'+ snaps[-1]+'.npy')
s_mass = np.load(data_dir+'mass_stellar_msun_m'+size+'n512_'+snaps[-1]+'.npy')
pos = np.load(data_dir+'pos_kpch_m'+size+'n512_'+snaps[-1]+'.npy')
skidid = np.load(data_dir + 'skid_id_m'+size+'n512_'+snaps[-1] +'.npy')
blue_mask = np.load(data_dir+'blue_true_m'+size+'n512_'+snaps[-1]+'.npy')
red_mask = np.logical_not(blue_mask)

b_t_prj = np.load(b_t_dir+'bulge_disk_j_cir_m'+size+'n512_'+snaps[-1]+'.npy')
b_t_prj[b_t_prj > 1.0] = 1.0
mass_bins = np.logspace(8, 12, num=20)

fig = plt.figure()
ax = plt.gca()
mass_bins_b, b_t_mean_b, b_t_var_b, b_t_per_b = jackknife(pos[blue_mask], s_mass[blue_mask], b_t_prj[blue_mask], 25000., mass_bins, std=True)
#top = b_t_mean + b_t_std; bottom = b_t_mean - b_t_std
plt.fill_between(mass_bins_b, b_t_per_b[1], b_t_per_b[2], facecolor='b', alpha=0.3)
ax.errorbar(mass_bins_b, b_t_mean_b, yerr=b_t_var_b, marker='.', markersize=10, linestyle='--', c='b',  label=str(len(pos[blue_mask]))+' blue')
mass_bins_r, b_t_mean_r, b_t_var_r, b_t_per_r = jackknife(pos[red_mask], s_mass[red_mask], b_t_prj[red_mask], 25000., mass_bins, std=True)
#top = b_t_mean + b_t_std; bottom = b_t_mean - b_t_std
plt.fill_between(mass_bins_r, b_t_per_r[1], b_t_per_r[2], facecolor='r', alpha=0.3)
ax.errorbar(mass_bins_r, b_t_mean_r, yerr=b_t_var_r, marker='.', markersize=10, linestyle='--', c='r', label=str(len(pos[red_mask]))+' red')
plt.xlabel(r'Stellar Mass (M$_\odot$)')
plt.ylabel('B / T')
plt.xlim(1e9, )
plt.legend()
ax.set_xscale('log')
plt.savefig(results_dir+ 'm'+size+'n512_btcir_mass_z0_50th.png')
plt.clf() 

j = np.load(b_t_dir+'j_total_stars_km2s-1_m'+size+'n512_'+snaps[i]+'.npy')
mass_bins_b, j_mean_b, j_var_b, j_per_b = jackknife(pos[blue_mask], s_mass[blue_mask], j[blue_mask], 25000., mass_bins, std=True)
#top = b_t_mean + b_t_std; bottom = b_t_mean - b_t_std
plt.fill_between(mass_bins_b, j_per_b[1], j_per_b[2], facecolor='b', alpha=0.3)
plt.plot(mass_bins_b, j_mean_b, marker='.', markersize=10, linestyle='--', c='b',  label=str(len(pos[blue_mask]))+' blue')
mass_bins_r, j_mean_r, j_var_r, j_per_r = jackknife(pos[red_mask], s_mass[red_mask], j[red_mask], 25000., mass_bins, std=True)
#top = b_t_mean + b_t_std; bottom = b_t_mean - b_t_std
plt.fill_between(mass_bins_r, j_per_r[1], j_per_r[2], facecolor='r', alpha=0.3)
plt.plot(mass_bins_r, j_mean_r, marker='.', markersize=10, linestyle='--', c='r', label=str(len(pos[red_mask]))+' red')
plt.xlabel(r'Stellar Mass (M$_\odot$)')
plt.ylabel(r'Specific Angular Momentum (km$^2$s$^{-1}$)')
plt.legend()
plt.yscale('log')
plt.xscale('log')
plt.savefig(results_dir+ 'm'+size+'n512_j_mass_z0_50th.png')
plt.clf() 

# plot showing red curve, blue curve, total curve for final redshift
k_rot = np.load(b_t_dir+'k_rot_m'+size+'n512_' + snaps[i]+'.npy')
v_t = np.load(b_t_dir+'v_t_mean_m'+size+'n512_'+snaps[i]+'.npy')
sigma = np.load(b_t_dir+'sigma_vel_kms_m'+size+'n512_'+snaps[i]+'.npy')
v_sigma = v_t / sigma
b_t_bins = np.arange(0, 1, 0.05)

fig = plt.figure()
ax = plt.gca()
b_t_bins_b, v_sigma_mean_b, v_sigma_var_b, v_sigma_per_b = jackknife(pos[blue_mask], b_t_prj[blue_mask], v_sigma[blue_mask], num_size, b_t_bins, std=True)
plt.fill_between(b_t_bins_b, v_sigma_per_b[1], v_sigma_per_b[2], facecolor='b', alpha=0.3)
ax.errorbar(b_t_bins_b, v_sigma_mean_b, yerr=v_sigma_var_b, marker='.', markersize=10, linestyle='--', c='b',  label=str(len(pos[blue_mask]))+' blue')
b_t_bins_r, v_sigma_mean_r, v_sigma_var_r, v_sigma_per_r = jackknife(pos[red_mask], b_t_prj[red_mask], v_sigma[red_mask], num_size, b_t_bins, std=True)
plt.fill_between(b_t_bins_r, v_sigma_per_r[1], v_sigma_per_r[2], facecolor='r', alpha=0.3)
ax.errorbar(b_t_bins_r, v_sigma_mean_r, yerr=v_sigma_var_r, marker='.', markersize=10, linestyle='--', c='r', label=str(len(pos[red_mask]))+' red')

plt.xlabel('B / T')
plt.ylabel(r'V / $\sigma$')
plt.xlim(0.3, )
plt.ylim(0.6, 1.5)
plt.legend()
plt.savefig(results_dir+ 'm'+size+'n512_btcir_vsigma_z0_50th.png')
plt.clf() 


fig = plt.figure()
ax = plt.gca()

b_t_bins_b, k_rot_mean_b, k_rot_var_b, k_rot_per_b = jackknife(pos[blue_mask], b_t_prj[blue_mask], k_rot[blue_mask], num_size, b_t_bins, std=True)
plt.fill_between(b_t_bins_b, k_rot_per_b[1], k_rot_per_b[2], facecolor='b', alpha=0.3)
ax.errorbar(b_t_bins_b, k_rot_mean_b, yerr=k_rot_var_b, marker='.', markersize=10, linestyle='--', c='b',  label=str(len(pos[blue_mask]))+' blue')

b_t_bins_r, k_rot_mean_r, k_rot_var_r, k_rot_per_r = jackknife(pos[red_mask], b_t_prj[red_mask], k_rot[red_mask], num_size, b_t_bins, std=True)
plt.fill_between(b_t_bins_r, k_rot_per_r[1], k_rot_per_r[2], facecolor='r', alpha=0.3)
ax.errorbar(b_t_bins_r, k_rot_mean_r, yerr=k_rot_var_r, marker='.', markersize=10, linestyle='--', c='r', label=str(len(pos[red_mask]))+' red')

plt.xlabel('B / T')
plt.ylabel(r'K$_{rot}$')
plt.xlim(0.3, )
plt.ylim(0.5, 10)
plt.legend()
plt.savefig(results_dir+ 'm'+size+'n512_btcir_krot_z0_50th.png')
plt.clf() 
"""