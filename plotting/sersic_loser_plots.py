import astropy.io.fits as fits
import numpy as np
from package.subdivide import *
import matplotlib.pyplot as plt
import matplotlib.colors as colors

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
cmap = plt.get_cmap('jet_r')
new_cmap = truncate_colormap(cmap, 0.1, 0.9)

data = fits.open('/home/sarah/masters/sdss/central_galaxies/color_1_data.fits')
n = data[1].data['sersic_n']

data_dir = '/home/sarah/masters/data/m25n512/old_caesar/'
masses = np.load(data_dir+'central_galaxies/masses_msun_m25n512_135.npy')
s_mass = np.load(data_dir+'central_galaxies/stellar_mass_msun_m25n512_135.npy')
pos_array = np.load(data_dir+'central_galaxies/pos_kpch_m25n512_135.npy')
sfr_array = np.load(data_dir+'central_galaxies/sfr_msun_gyr_m25n512_135.npy')
sfr_array[(sfr_array < 1e-12)] = 1e-12
ssfr_array = sfr_array / s_mass


plt.scatter(masses[n != 0.0], n[n != 0.0], s=20,c=np.log10(ssfr_array[n != 0.0]), marker = 'o', cmap = new_cmap)
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2, )
plt.xscale('log')
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Sersic index')
plt.savefig('/home/sarah/masters/coursework/report/results/mass_sersic_n.png')
plt.clf()

mass_bins = np.logspace(8, 12, num=20)
mass_bins, n_mean, n_var, n_per = jackknife(pos_array[n != 0.0], s_mass[n != 0.0], n[n != 0.0], 25000., mass_bins, std=True)

fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, n_mean, yerr=n_var, marker='.', markersize=10, linestyle='--')
plt.xlabel(r'Stellar Mass (M$_\odot$)')
plt.ylabel(r'Sersic Index')
plt.xscale('log')
plt.savefig('/home/sarah/masters/coursework/report/results/mass_sersic_n_jk.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, n_mean, yerr=n_var, marker='.', markersize=10, linestyle='--')
plt.fill_between(mass_bins, n_per[2], n_per[1], facecolor='b', alpha=0.3)
plt.xscale('log')
plt.xlim(1e9, )
plt.ylabel(r'Sersic Index')
plt.xlabel(r'Stellar Mass (M$_\odot$)')
plt.savefig('/home/sarah/masters/coursework/report/results/mass_sersic_n_jk_50th.png')
plt.clf()

