import numpy as np 
from package.convert import *
import astropy.io.fits as fits
import matplotlib.pyplot as plt
import sys

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

data = fits.open('/home/sarah/masters/rotated_galaxies/rotated_095/disks/decompose/fits/gal_7_74_flux.fits')
data = fits.open(sys.argv[1])
data = data[0].data

# image is currently in flux units
mag = f_to_magab(data)
mag[mag > -9.5] = 0.0

plt.imshow(mag[18:57][:, 18:57], cmap='magma_r', vmax=-0.1)
plt.colorbar(label=r'AB magnitude')
plt.clim(-17, 0)
plt.savefig(sys.argv[2])
plt.clf()