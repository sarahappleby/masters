import astropy.io.fits as fits
import numpy as np
from package.subdivide import *
import matplotlib.pyplot as plt
import matplotlib.colors as colors

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
cmap = plt.get_cmap('jet_r')
new_cmap = truncate_colormap(cmap, 0.1, 0.9)

results_dir = '/home/sarah/masters/coursework/report/results/casgm_bands/'

"""
data = fits.open('/home/sarah/masters/sdss/central_galaxies/color_1_data.fits')
conc = data[1].data['conc']
# remove 8th galaxy since its messed up
mask = conc != 0.0
mask[8] = 0.0
mask[np.argmax(conc)] = 0.0
conc = conc[mask]
asymm = data[1].data['assym'][mask]
clump = data[1].data['clump'][mask]
gini = data[1].data['gini'][mask]
m20 = data[1].data['m20'][mask]
"""
conc = np.load('/home/sarah/masters/casgm_bands/color_u_i/conc_array.npy')
mask = conc != 0.0
mask[np.argmax(conc)] = 0.0
	

data_dir = '/home/sarah/masters/data/m25n512/old_caesar/'
masses = np.load(data_dir+'central_galaxies/masses_msun_m25n512_135.npy')[mask]
s_mass = np.load(data_dir+'central_galaxies/stellar_mass_msun_m25n512_135.npy')[mask]
pos_array = np.load(data_dir+'central_galaxies/pos_kpch_m25n512_135.npy')[mask]
sfr_array = np.load(data_dir+'central_galaxies/sfr_msun_gyr_m25n512_135.npy')[mask]
sfr_array[(sfr_array < 1e-12)] = 1e-12
ssfr_array = sfr_array / s_mass

colors = [1, 4, 'u_i']
labels = ['u', 'i', 'u - i']
c = ['b', 'r', 'm']

fig = plt.figure()
ax = plt.gca()

for i in range(len(colors)):
	conc = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/conc_array.npy')
	mask = conc != 0.0
	mask[np.argmax(conc)] = 0.0
	conc = conc[mask]
	clump = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/clump_array.npy')[mask]

	mass_bins = np.logspace(8, 12, num=20)
	mass_bins, clump_mean, clump_var, clump_per = jackknife(pos_array, s_mass, clump, 25000., mass_bins, std=True)
	ax.errorbar(mass_bins, clump_mean, yerr=clump_var, marker='.', markersize=10, linestyle='--', label=labels[i], c=c[i])
	plt.fill_between(mass_bins, clump_per[2], clump_per[1], facecolor=c[i], alpha=0.2)

plt.xscale('log')
plt.xlim(1e9, )
#plt.ylim(0.2, 1.2)
plt.legend()
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Clumpiness')
plt.savefig(results_dir+'mass_clump_jk.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()

for i in range(len(colors)):
	conc = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/conc_array.npy')
	mask = conc != 0.0
	mask[np.argmax(conc)] = 0.0
	conc = conc[mask]
	asymm = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/assym_array.npy')[mask]

	mass_bins = np.logspace(8, 12, num=20)
	mass_bins, asymm_mean, asymm_var, asymm_per = jackknife(pos_array, s_mass, asymm, 25000., mass_bins, std=True)
	ax.errorbar(mass_bins, asymm_mean, yerr=asymm_var, marker='.', markersize=10, linestyle='--', label=labels[i], c=c[i])
	plt.fill_between(mass_bins, asymm_per[2], asymm_per[1], facecolor=c[i], alpha=0.2)
plt.xscale('log')
plt.xlim(1e9, )
plt.legend()
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Asymmetry')
plt.savefig(results_dir+'mass_asymm_jk.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()

for i in range(len(colors)):
	conc = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/conc_array.npy')
	mask = conc != 0.0
	mask[np.argmax(conc)] = 0.0
	conc = conc[mask]

	mass_bins = np.logspace(8, 12, num=20)
	mass_bins, conc_mean, conc_var, conc_per = jackknife(pos_array, s_mass, conc, 25000., mass_bins, std=True)
	ax.errorbar(mass_bins, conc_mean, yerr=conc_var, marker='.', markersize=10, linestyle='--', label=labels[i], c=c[i])
	plt.fill_between(mass_bins, conc_per[2], conc_per[1], facecolor=c[i], alpha=0.2)

plt.xscale('log')
plt.xlim(1e9, )
#plt.ylim(1.0, )
plt.legend()
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Concentration')
plt.savefig(results_dir+'mass_conc_jk.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()

for i in range(len(colors)):
	conc = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/conc_array.npy')
	mask = conc != 0.0
	mask[np.argmax(conc)] = 0.0
	conc = conc[mask]
	gini = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/gini_array.npy')[mask]

	mass_bins = np.logspace(8, 12, num=20)
	mass_bins, gini_mean, gini_var, gini_per = jackknife(pos_array, s_mass, gini, 25000., mass_bins, std=True)
	ax.errorbar(mass_bins, gini_mean, yerr=gini_var, marker='.', markersize=10, linestyle='--', label=labels[i], c=c[i])
	
	plt.fill_between(mass_bins, gini_per[2], gini_per[1], facecolor=c[i], alpha=0.2)

plt.xscale('log')
plt.xlim(1e9, )
plt.legend()
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Gini Coefficient')
plt.savefig(results_dir+'mass_gini_jk.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()

for i in range(len(colors)):
	conc = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/conc_array.npy')
	mask = conc != 0.0
	mask[np.argmax(conc)] = 0.0
	conc = conc[mask]
	m20 = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/m20_array.npy')[mask]

	mass_bins = np.logspace(8, 12, num=20)
	mass_bins, m20_mean, m20_var, m20_per = jackknife(pos_array, s_mass, m20, 25000., mass_bins, std=True)
	ax.errorbar(mass_bins, m20_mean, yerr=m20_var, marker='.', markersize=10, linestyle='--', label=labels[i], c=c[i])
	plt.fill_between(mass_bins, m20_per[2], m20_per[1], facecolor=c[i], alpha=0.2)

plt.xscale('log')
plt.xlim(1e9, )
plt.ylim(-2, -1.2)
plt.legend()
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'M$_{20}$')
plt.savefig(results_dir+'mass_m20_jk.png')
plt.clf()

for i in range(len(colors)):
	conc = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/conc_array.npy')
	mask = conc != 0.0
	mask[np.argmax(conc)] = 0.0
	conc = conc[mask]
	masses = np.load(data_dir+'central_galaxies/masses_msun_m25n512_135.npy')[mask]
	s_mass = np.load(data_dir+'central_galaxies/stellar_mass_msun_m25n512_135.npy')[mask]
	pos_array = np.load(data_dir+'central_galaxies/pos_kpch_m25n512_135.npy')[mask]
	sfr_array = np.load(data_dir+'central_galaxies/sfr_msun_gyr_m25n512_135.npy')[mask]
	sfr_array[(sfr_array < 1e-12)] = 1e-12
	ssfr_array = sfr_array / s_mass

	asymm = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/assym_array.npy')[mask]
	clump = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/clump_array.npy')[mask]
	gini = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/gini_array.npy')[mask]
	m20 = np.load('/home/sarah/masters/casgm_bands/color_'+str(colors[i])+'/m20_array.npy')[mask]

	plt.scatter(masses, clump, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
	plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
	plt.clim(-2, )
	plt.xlim(1e9, )
	plt.xscale('log')
	plt.xlabel(r'Mass ($M_{\odot}$)')
	plt.ylabel(r'Clumpiness')
	plt.savefig(results_dir+'mass_clump_color_'+str(colors[i])+'.png')
	plt.clf()

	plt.scatter(masses, conc, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
	plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
	plt.clim(-2, )
	plt.xlim(1e9, )
	plt.xscale('log')
	plt.xlabel(r'Mass ($M_{\odot}$)')
	plt.ylabel(r'Concentration')
	plt.savefig(results_dir+'mass_conc_color_'+str(colors[i])+'.png')
	plt.clf()

	plt.scatter(masses, asymm, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
	plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
	plt.clim(-2, )
	plt.xlim(1e9, )
	plt.xscale('log')
	plt.xlabel(r'Mass ($M_{\odot}$)')
	plt.ylabel(r'Asymmetry')
	plt.savefig(results_dir+'mass_asymm_color_'+str(colors[i])+'.png')
	plt.clf()

	plt.scatter(masses, gini, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
	plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
	plt.clim(-2, )
	plt.xlim(1e9, )
	plt.xscale('log')
	plt.xlabel(r'Mass ($M_{\odot}$)')
	plt.ylabel(r'Gini Coefficient')
	plt.savefig(results_dir+'mass_gini_color_'+str(colors[i])+'.png')
	plt.clf()

	plt.scatter(masses, m20, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
	plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
	plt.clim(-2, )
	plt.xlim(1e9, )
	plt.xscale('log')
	plt.xlabel(r'Mass ($M_{\odot}$)')
	plt.ylabel(r'M$_{20}$')
	plt.savefig(results_dir+'mass_m20_color_'+str(colors[i])+'.png')
	plt.clf()

	plt.scatter(m20, gini, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
	plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
	plt.clim(-2, )
	plt.xlabel(r'M$_{20}$')
	plt.ylabel(r'Gini Coefficient')
	plt.xlim(-0.4, -2.6)
	plt.savefig(results_dir+'m20_gini_color_'+str(colors[i])+'.png')
	plt.clf()

	plt.scatter(conc, asymm, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
	plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
	plt.clim(-2, )
	plt.xlabel(r'Concentration')
	plt.ylabel(r'Asymmetry')
	plt.savefig(results_dir+'conc_asymm_color_'+str(colors[i])+'.png')
	plt.clf()


"""

plt.scatter(masses, clump, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2, )
plt.xscale('log')
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Clumpiness')
plt.savefig(results_dir+'mass_clump.png')
plt.clf()

mass_bins, clump_mean, clump_var, clump_per = jackknife(pos_array, s_mass, clump, 25000., mass_bins, std=True)
fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, clump_mean, yerr=clump_var, marker='.', markersize=10, linestyle='--')
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Clumpiness')
plt.xscale('log')
plt.savefig(results_dir+'mass_clump_jk.png')
plt.clf()

plt.scatter(masses, conc, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2, )
plt.xscale('log')
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Concentration')
plt.savefig(results_dir+'mass_conc.png')
plt.clf()

mass_bins = np.logspace(8, 12, num=20)
mass_bins, conc_mean, conc_var, conc_per = jackknife(pos_array, s_mass, conc, 25000., mass_bins, std=True)
fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, conc_mean, yerr=conc_var, marker='.', markersize=10, linestyle='--')
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Concentration')
plt.xscale('log')
plt.savefig(results_dir+'mass_conc_jk.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, conc_mean, yerr=conc_var, marker='.', markersize=10, linestyle='--')
plt.fill_between(mass_bins, conc_per[2], conc_per[1], facecolor='b', alpha=0.3)
plt.xscale('log')
plt.xlim(1e9, )
plt.ylim(1.0, )
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Concentration')
plt.savefig(results_dir+'mass_conc_jk_50th.png')
plt.clf()

plt.scatter(masses, asymm, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2, )
plt.xscale('log')
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Asymmetry')
plt.savefig(results_dir+'mass_asymm.png')
plt.clf()

mass_bins = np.logspace(8, 12, num=20)
mass_bins, asymm_mean, asymm_var, asymm_per = jackknife(pos_array, s_mass, asymm, 25000., mass_bins, std=True)
fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, asymm_mean, yerr=asymm_var, marker='.', markersize=10, linestyle='--')
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Asymmetry')
plt.xscale('log')
plt.savefig(results_dir+'mass_asymm_jk.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, asymm_mean, yerr=asymm_var, marker='.', markersize=10, linestyle='--')
plt.fill_between(mass_bins, asymm_per[2], asymm_per[1], facecolor='b', alpha=0.3)
plt.xscale('log')
plt.xlim(1e9, )
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Asymmetry')
plt.savefig(results_dir+'mass_asymm_jk_50th.png')
plt.clf()

plt.scatter(masses, gini, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2, )
plt.xscale('log')
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Gini Coefficient')
plt.savefig(results_dir+'mass_gini.png')
plt.clf()

mass_bins = np.logspace(8, 12, num=20)
mass_bins, gini_mean, gini_var, gini_per = jackknife(pos_array, s_mass, gini, 25000., mass_bins, std=True)
fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, gini_mean, yerr=gini_var, marker='.', markersize=10, linestyle='--')
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Gini Coefficient')
plt.xscale('log')
plt.savefig(results_dir+'mass_gini_jk.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, gini_mean, yerr=gini_var, marker='.', markersize=10, linestyle='--')
plt.fill_between(mass_bins, gini_per[2], gini_per[1], facecolor='b', alpha=0.3)
plt.xscale('log')
plt.xlim(1e9, )
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Gini Coefficient')
plt.savefig(results_dir+'mass_gini_jk_50th.png')
plt.clf()

plt.scatter(masses, m20, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2, )
plt.xscale('log')
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'M$_{20}$')
plt.savefig(results_dir+'mass_m20.png')
plt.clf()

mass_bins = np.logspace(8, 12, num=20)
mass_bins, m20_mean, m20_var, m20_per = jackknife(pos_array, s_mass, m20, 25000., mass_bins, std=True)
fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, m20_mean, yerr=m20_var, marker='.', markersize=10, linestyle='--')
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'M$_{20}$')
plt.xscale('log')
plt.savefig(results_dir+'mass_m20_jk.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, m20_mean, yerr=m20_var, marker='.', markersize=10, linestyle='--')
plt.fill_between(mass_bins, m20_per[2], m20_per[1], facecolor='b', alpha=0.3)
plt.xscale('log')
plt.xlim(1e9, )
plt.ylim(-2, -1.2)
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'M$_{20}$')
plt.savefig(results_dir+'mass_m20_jk_50th.png')
plt.clf()

plt.scatter(m20, gini, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2, )
plt.xlabel(r'M$_{20}$')
plt.ylabel(r'Gini Coefficient')
plt.xlim(-0.4, -2.6)
plt.savefig(results_dir+'m20_gini.png')
plt.clf()

plt.scatter(conc, asymm, s=20,c=np.log10(ssfr_array), marker = 'o', cmap = new_cmap)
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2, )
plt.xlabel(r'Concentration')
plt.ylabel(r'Asymmetry')
plt.savefig(results_dir+'conc_asymm.png')
plt.clf()

conc_bins = np.arange(0., 6., 0.3)
conc_bins, a_mean, a_var, a_per = jackknife(pos_array, conc, asymm, 25000., conc_bins, std=True)
fig = plt.figure()
ax = plt.gca()
ax.errorbar(conc_bins, a_mean, yerr=a_var, marker='.', markersize=10, linestyle='--')
plt.xlabel(r'Concentration')
plt.ylabel(r'Asymmetry')
plt.savefig(results_dir+'conc_asymm_jk.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()
ax.errorbar(conc_bins, a_mean, yerr=a_var, marker='.', markersize=10, linestyle='--')
plt.fill_between(conc_bins, a_per[2], a_per[1], facecolor='b', alpha=0.3)
plt.xlabel(r'Concentration')
plt.ylabel(r'Asymmetry')
plt.savefig(results_dir+'conc_asymm_jk_50th.png')
plt.clf()

"""