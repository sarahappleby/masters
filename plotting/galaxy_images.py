import numpy as np 
import caesar
from package.projection import *
from readgadget import readsnap
import matplotlib.pyplot as plt

data_dir = '/home/sarah/masters/data/m25n512/'
results_dir = '/home/sarah/masters/coursework/report/results/galaxy_images/'
obj = caesar.load(data_dir+'caesar_obj_m12.5n128.hdf5')
snap = data_dir + 'snap_m12.5n128_135'

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

h = 0.68 # hubble constant
xmin = -20.   # [kpc]
xmax = 20.    # [kpc]
Npixels = 100 #512
DR = 1. # kpc 
r_max = 100000
r_rot = 500.

props = dict(boxstyle='round', facecolor=None)


star_positions = readsnap(snap, 'pos', 'star', suppress=1, units=1)
star_vels = readsnap(snap, 'vel', 'star', suppress=1, units=0)
star_mass = readsnap(snap, 'mass', 'star', suppress=1, units=1) / (h*10**7.)

names = ['edge_on', 'face_on']
vecs = [np.array([0,1,0]), np.array([0,0,1])]

# 095
gals = [11, 28]
textstr = [('M = 4.47e10 M$_{\odot}$\n'+
			'B/T = 0.52\n'+ 'n$_I$ = 0.2\n' + 'n$_P$ = 0.3'), 
			('M = 3.39e10 M$_{\odot}$\n'+
			'B/T = 0.71\n'+ 'n$_I$ = 0.5\n' + 'n$_P$ = 1.1')]
# 105
gals = [73, 4]
textstr = [('M = 2.59e10 M$_{\odot}$\n'+
			'B/T = 0.37\n'+ 'n$_I$ = 0.4\n' + 'n$_P$ = 0.2'), 
			('M = 1.79e11 M$_{\odot}$\n'+
			'B/T = 0.58\n'+ 'n$_I$ = 0.5\n' + 'n$_P$ = 0.6')]
# 115
gals = [106, 11]
textstr = [('M = 2.29e10 M$_{\odot}$\n'+
			'B/T = 0.24\n'+ 'n$_I$ = 0.1\n' + 'n$_P$ = 0.5'), 
			('M = 6.46e10 M$_{\odot}$\n'+
			'B/T = 0.76\n'+ 'n$_I$ = 1.5\n' + 'n$_P$ = 1.6')]
# 135
gals = [65, 15]
textstr = [('M = 3.60e10 M$_{\odot}$\n'+
			'B/T = 0.58\n'+ 'n$_I$ = 0.6\n' + 'n$_P$ = 0.5'), 
			('M = 6.67e10 M$_{\odot}$\n'+
			'B/T = 0.73.\n'+ 'n$_I$ = 0.6\n' + 'n$_P$ = 0.8')]


for i in gals:
	slist = obj.central_galaxies[i].slist

	x = star_positions[slist][:, 0]
	y = star_positions[slist][:, 1]
	z = star_positions[slist][:, 2]
	vx = star_vels[slist][:, 0]
	vy = star_vels[slist][:, 1]
	vz = star_vels[slist][:, 2]
	mass = star_mass[slist]

	for v in range(len(vecs)):
		vec = vecs[v]
		name = names[v]
		posx,posy,posz,vx,vy,vz = recentre_pos_and_vel(x,y,z,vx,vy,vz,mass,r_max) 
		filter_rad = (np.sqrt(posx**2+posy**2+posz**2) < r_rot)
		axis, angle = compute_rotation_to_vec(posx[filter_rad],posy[filter_rad],posz[filter_rad],vx[filter_rad],vy[filter_rad],vz[filter_rad],mass[filter_rad],vec)
		posx,posy,posz = rotate(posx,posy,posz,axis,angle) 
		vx,vy,vz = rotate(vx,vy,vz,axis,angle)

		filter_rad=(posx>xmin)*(posx<xmax)*(posy>xmin)*(posy<xmax) ##*(posz>xmin)*(posz<xmax)
		im,xedges,yedges=np.histogram2d(posx[filter_rad],posy[filter_rad],bins=(Npixels,Npixels),weights=mass[filter_rad])
		im=im/((xmax-xmin)/float(Npixels))**2 #gives surface density
		extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]

		v_min = np.min(np.log10(im[im>0]))
		v_max = np.max(np.log10(im[im>0]))

		plt.imshow(np.log10(im.transpose()+0.0001),extent=extent, interpolation='nearest',cmap='magma_r',vmin=v_min,vmax=v_max, origin="lower") 
		plt.text(0.05, 0.95, textstr[v], fontsize=14, verticalalignment='top', bbox=props)
		plt.savefig(results_dir+'gal_'+str(i)+'_'+name+'.png')
		plt.clf()
