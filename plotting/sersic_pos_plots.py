import numpy as np
from package.subdivide import *
import matplotlib.pyplot as plt
from matplotlib import cm

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

view = 'faceon'

n = np.load('/home/sarah/masters/rotated_galaxies/rotated_135/'+view+'_sersic_params.npy')[:, -1]
data_dir = '/home/sarah/masters/data/'
masses = np.load(data_dir+'central_galaxies/mass_total_msun_m25n512_135.npy')
s_mass = np.load(data_dir+'central_galaxies/mass_stellar_msun_m25n512_135.npy')
pos_array = np.load(data_dir+'central_galaxies/pos_kpch_m25n512_135.npy')
sfr_array = np.load(data_dir+'central_galaxies/sfr_msun_gyr_m25n512_135.npy')
sfr_array[(sfr_array < 1e-12)] = 1e-12
ssfr_array = sfr_array / s_mass

plt.scatter(masses[n != 0.0], n[n != 0.0], s=20,c=np.log10(ssfr_array[n != 0.0]), marker = 'o', cmap = cm.jet_r)
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2, )
plt.ylim(0, 5)
plt.xscale('log')
plt.xlabel(r'Mass ($M_{\odot}$)')
plt.ylabel(r'Sersic index')
plt.savefig('/home/sarah/masters/coursework/report/results/mass_sersic_n_'+view+'.png')
plt.clf()

mass_bins = np.logspace(8, 12, num=20)
mass_bins, n_mean, n_var, n_per = jackknife(pos_array[n != 0.0], s_mass[n != 0.0], n[n != 0.0], 25000., mass_bins, std=True)

fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, n_mean, yerr=n_var, marker='.', markersize=10, linestyle='--')
plt.xlabel(r'Stellar Mass (M$_\odot$)')
plt.ylabel(r'Sersic Index')
plt.xscale('log')
plt.savefig('/home/sarah/masters/coursework/report/results/mass_sersic_n_jk_'+view+'.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()
ax.errorbar(mass_bins, n_mean, yerr=n_var, marker='.', markersize=10, linestyle='--')
plt.fill_between(mass_bins, n_per[2], n_per[1], facecolor='b', alpha=0.3)
plt.xscale('log')
plt.ylabel(r'Sersic Index')
plt.xlabel(r'Stellar Mass (M$_\odot$)')
plt.savefig('/home/sarah/masters/coursework/report/results/mass_sersic_n_jk_50th_'+view+'.png')
plt.clf()