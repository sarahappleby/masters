import astropy.io.fits as fits
import numpy as np
from package.subdivide import *
import matplotlib.pyplot as plt
import matplotlib.colors as colors

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
cmap = plt.get_cmap('jet_r')
new_cmap = truncate_colormap(cmap, 0.1, 0.9)

data = fits.open('/home/sarah/masters/sdss/central_galaxies/color_1_data.fits')
hlr = data[1].data['half_r'] * 0.68
hlr = np.load('/home/sarah/masters/casgm_bands/color_4/half_r_kpch.npy')/1.87

data_dir = '/home/sarah/masters/data/m25n512/old_caesar/'
hsr = np.load(data_dir+'central_galaxies/half_stel_r_kpch_m25n512_135.npy')
pos_array = np.load(data_dir+'central_galaxies/pos_kpch_m25n512_135.npy')
s_mass = np.load(data_dir+'central_galaxies/stellar_mass_msun_m25n512_135.npy')
sfr_array = np.load(data_dir+'central_galaxies/sfr_msun_gyr_m25n512_135.npy')
sfr_array[(sfr_array < 1e-12)] = 1e-12
ssfr_array = sfr_array / s_mass

s = np.power(np.log10(s_mass), 5) / 10000
plt.scatter(hsr[hlr != 0.0], hlr[hlr != 0.0], s=s,c=np.log10(ssfr_array[hlr != 0.0]), marker = 'o', cmap = new_cmap)
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2, )
plt.xlabel(r'Half Stellar Mass Radius (kpc/h)')
plt.ylabel(r'Half Light Radius (kpc/h)')
plt.savefig('/home/sarah/masters/coursework/report/results/hsr_hlr.png')
plt.clf()

r_bins = np.logspace(-1, 1.2, num=20)
r_bins, hlr_mean, hlr_var, hlr_per = jackknife(pos_array[hlr != 0.0], hsr[hlr != 0.0], hlr[hlr != 0.0], 25000., r_bins, std=True)

fig = plt.figure()
ax = plt.gca()
ax.errorbar(r_bins, hlr_mean, yerr=hlr_var, marker='.', markersize=10, linestyle='--')
plt.xlabel(r'Half Stellar Mass Radius (kpc/h)')
plt.ylabel(r'Half Light Radius (kpc/h)')
plt.savefig('/home/sarah/masters/coursework/report/results/hsr_hlr_jk.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()
ax.errorbar(r_bins, hlr_mean, yerr=hlr_var, marker='.', markersize=10, linestyle='--')
plt.fill_between(r_bins, hlr_per[2], hlr_per[1], facecolor='b', alpha=0.3)
plt.xlabel(r'Half Stellar Mass Radius (kpc/h)')
plt.ylabel(r'Half Light Radius (kpc/h)')
plt.savefig('/home/sarah/masters/coursework/report/results/hsr_hlr_jk_50th.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()
plt.scatter(hsr[hlr != 0.0], hlr[hlr != 0.0], s=s,c=np.log10(ssfr_array[hlr != 0.0]), marker = 'o', cmap = new_cmap)
ax.errorbar(r_bins, hlr_mean, yerr=[np.zeros(len(hlr_var)), hlr_var], marker='.', markersize=5, linestyle='--', c='k', linewidth=1, capsize=2)
plt.fill_between(r_bins, hlr_per[2], hlr_per[1], facecolor='k', alpha=0.2, linewidth=1)
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2, )
plt.xscale('log')
plt.yscale('log')
plt.xlabel(r'$R_S$ (kpc/h)')
plt.ylabel(r'$R_e$ (kpc/h)')
plt.plot(range(15), range(15), c='k', linewidth=1)
plt.savefig('/home/sarah/masters/coursework/report/results/hsr_hlr_all.png')
plt.clf()

mass_bins = np.logspace(8, 12, num=20)
hlr_mass_bins, hlr_mean, hlr_var, hlr_per = jackknife(pos_array[hlr != 0.0], s_mass[hlr != 0.0], hlr[hlr != 0.0], 25000., mass_bins, std=True)
hsr_mass_bins, hsr_mean, hsr_var, hsr_per = jackknife(pos_array, s_mass, hsr, 25000., mass_bins, std=True)
fig = plt.figure()
ax = plt.gca()
ax.errorbar(hlr_mass_bins, hlr_mean, yerr=hlr_var, marker='.', markersize=6, linestyle='--', c='b', linewidth=1, label='Half Light')
plt.fill_between(hlr_mass_bins, hlr_per[2], hlr_per[1], facecolor='b', alpha=0.2, linewidth=1)
ax.errorbar(hsr_mass_bins, hsr_mean, yerr=hsr_var, marker='.', markersize=6, linestyle='--', c='g', linewidth=1, label='Half Stellar Mass')
plt.fill_between(hsr_mass_bins, hsr_per[2], hsr_per[1], facecolor='g', alpha=0.2, linewidth=1)
plt.legend()
plt.xscale('log')
plt.yscale('log')
plt.xlabel(r'Stellar Mass (M$_\odot$)')
plt.ylabel(r'Size (kpc/h)')
plt.savefig('/home/sarah/masters/coursework/report/results/mass_hsr_hlr.png')
plt.clf()

