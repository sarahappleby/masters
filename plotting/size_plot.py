import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.colors as colors

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap

cmap = plt.get_cmap('jet_r')
new_cmap = truncate_colormap(cmap, 0.05, 0.95)

data_dir = '/home/sarah/masters/data/m50n512/central_galaxies/'

r = np.load(data_dir+'half_stel_r_kpch_m50n512_135.npy')/0.68
sfr = np.load(data_dir+'sfr_msun_gyr_m50n512_135.npy')
s_mass = np.load(data_dir+'mass_stellar_msun_m50n512_135.npy')
sfr[(sfr < 1e-12)] = 1e-12
ssfr = sfr / s_mass

plt.scatter(np.log10(s_mass), np.log10(r), s=20,c=np.log10(ssfr), marker = 'o', cmap = new_cmap)
plt.colorbar(label=r'log(sSFR (Gyr$^{-1}$))')
plt.clim(-2, )
plt.xlim(9, )
#plt.xscale('log')
#plt.yscale('log')
plt.xlabel(r'log (M$_*$ / M$_{\odot}$)')
plt.ylabel(r'log (R$_S$ / kpc)')
plt.savefig('size_mass.png')
plt.clf()