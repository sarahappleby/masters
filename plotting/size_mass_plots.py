from package.subdivide import *
import numpy as np 
import matplotlib.pyplot as plt 
from matplotlib import patches
from scipy.optimize import curve_fit

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

def fit_linear(x, m, c):
	return x*m + c

def power_law(x, m, c):
	return 10**(m*x + c)

def size_mass_power_law(x, m_0, a, b, c):
	return c*((x/m_0)**a)*((1 + x/m_0)**(b - a))

size = '50'
num_size = 50000.

data_dir = '/home/sarah/masters/data/m'+size+'n512/central_galaxies/'
snaps = ['085', '095', '105', '115', '135']
labels = ['z = 2.0', 'z = 1.5', 'z = 1.0', 'z = 0.5', 'z = 0.0']
labels = ['t = 3.3 Gyr', 't = 4.3 Gyr', 't = 5.9 Gyr', 't = 8.6 Gyr', 't = 13.7 Gyr']
#ticks = [.5, 1, 2, 3, 4]
colors = ['c', 'g', 'r', 'tab:purple', 'b']
init = [5e9, 4e9, 3e9, 2e9, 1e9]
end = [6e10, 7e10, 7e10, 9e10, 1e11]


z = np.flip(np.arange(0, 2.5, 0.5), 0)
sizes = np.zeros(5)
per_25 = np.zeros(5)
per_75 = np.zeros(5)
i = 0
for snap in snaps:
	s_mass = np.load(data_dir+'mass_stellar_msun_m'+size+'n512_'+snap+'.npy')
	mask = (s_mass > 4.8e10) & (s_mass < 5.2e10)
	r = np.load(data_dir+'half_stel_r_kpch_m'+size+'n512_'+snap+'.npy')[mask]/0.68
	sizes[i] = np.median(r); per_25[i] = np.percentile(r, 25); per_75[i] = np.percentile(r, 75)
	i +=1

fig = plt.figure()
ax = plt.gca()

plt.plot(z, sizes, marker='.', markersize=10, linestyle='--', c='m')
plt.fill_between(z, per_25, per_75, facecolor='m', alpha=0.15, linewidth=1)

popt, pcov = curve_fit(fit_linear, np.log10((z+1)), np.log10(sizes), maxfev=20000)
y = power_law(np.log10((z+1)), popt[0], popt[1])

print 'a = ' + str.format("{0:.3g}", popt[1])
print 'b = ' + str.format("{0:.3g}", popt[0])
plt.plot(z, y, marker=None, linestyle='-', c='k', linewidth=1)
#popt, pcov = curve_fit(fit_linear, np.log10(z+0.000001), np.log10(sizes), maxfev=20000)
#y = power_law(np.log10(z+ 0.000001), popt[0], popt[1])
#plt.plot(z, y, marker=None, linestyle=':', c='k', linewidth=1)
#a = str.format("{0:.3g}", popt[1]); b = str.format("{0:.3g}", popt[0])
#textstr = r'R_S / $\mathrm{kpc}$ = '+ a + '(1+z)^${' + b + '}$'
#props = dict(boxstyle='round', facecolor='None')
#plt.text(0.7, 0.95, textstr, transform=ax.transAxes, fontsize=10,verticalalignment='top', bbox=props)
#plt.title('R_S / kpc = '+ str.format("{0:.3g}", popt[1]) + '(1+z) **'+ str.format("{0:.3g}", popt[0]) +')')
plt.xlabel(r'z')
plt.ylabel(r'R$_S$ (kpc)')
#plt.yscale('log')
plt.xlim(2.1, -0.1)
plt.savefig('/home/sarah/masters/coursework/report/results/z_size_5e10.png')
plt.clf()

fig = plt.figure()
ax = plt.gca()
i = 0
for snap in snaps:
	mass_bins = np.logspace(8, 12, num=20)
	r = np.load(data_dir+'half_stel_r_kpch_m'+size+'n512_'+snap+'.npy')/0.68
	pos = np.load(data_dir+'pos_kpch_m'+size+'n512_'+snap+'.npy')
	s_mass = np.load(data_dir+'mass_stellar_msun_m'+size+'n512_'+snap+'.npy')
	mass_bins, r_mean, r_var, r_per = jackknife(pos, s_mass, r, num_size, mass_bins, std=True)
	start = np.argmin(np.abs(mass_bins - init[i]))
	stop = np.argmin(np.abs(mass_bins - end[i]))
	popt, pcov = curve_fit(fit_linear, np.log10(mass_bins)[start:stop+1], np.log10(r_mean)[start:stop+1], maxfev=20000)
	y = power_law(np.log10(mass_bins), popt[0], popt[1])
	label = labels[i]# + ', a = ' + str.format("{0:.3g}", popt[0])
	#ax.errorbar(mass_bins, r_mean, yerr=r_var, marker='.', markersize=10, linestyle='--', label=labels[i], c=col	ors[i])
	plt.plot(mass_bins, r_mean, marker='.', markersize=10, linestyle='--', label=label, c=colors[i])
	plt.plot(mass_bins[start:stop+1], y[start:stop+1], marker=None, linestyle='-', c='k', linewidth=1)
	i +=1

plt.xlim(1e9, 1e12 )
plt.xlabel(r'M$_*$ (M$_\odot$)')
plt.ylabel(r'R$_S$ (kpc)')
plt.xscale('log')
plt.yscale('log')
plt.legend()
plt.savefig('/home/sarah/masters/coursework/report/results/m'+size+'n512_size_mass.png')
plt.clf()


mass_bins = np.logspace(8, 12, num=20)
r = np.load(data_dir+'half_stel_r_kpch_m'+size+'n512_'+snap+'.npy')/0.68
pos = np.load(data_dir+'pos_kpch_m'+size+'n512_'+snap+'.npy')
s_mass = np.load(data_dir+'mass_stellar_msun_m'+size+'n512_'+snap+'.npy')
blue = np.load(data_dir+'blue_true_m'+size+'n512_'+snap+'.npy')
red = np.logical_not(blue)

fig = plt.figure()
#ax = plt.gca()
ax = fig.add_subplot(111)

#e1 = patches.Ellipse((1e10, 6.6), 1e11, 3, angle=50, linewidth=1, fill=False, color='b')
#ax.add_patch(e1)

mass_bins_b, r_mean_b, r_var_b, r_per_b = jackknife(pos[blue], s_mass[blue], r[blue], num_size, mass_bins, std=True)
start = np.argmin(np.abs(mass_bins_b - init[-1]))
stop = np.argmin(np.abs(mass_bins_b - end[-1]))	
popt, pcov = curve_fit(fit_linear, np.log10(mass_bins_b)[start:stop+1], np.log10(r_mean_b)[start:stop+1], maxfev=20000)
#y = fit_linear(np.log10(mass_bins_b), popt[0], popt[1])
y = power_law(np.log10(mass_bins_b), popt[0], popt[1])
sloan_y = size_mass_power_law(mass_bins_b, 6.49e11*(0.68**2), 0.24, 1.33, 10.17)
label = str(len(pos[blue]))+' blue, a = ' + str.format("{0:.3g}", popt[0])
label = 'MUFASA blue'
plt.plot(mass_bins_b, r_mean_b, marker='.', markersize=10, linestyle='--', label=label, c='b')
plt.fill_between(mass_bins_b, r_per_b[2], r_per_b[1], facecolor='b', alpha=0.2, linewidth=1)
plt.plot(mass_bins_b[start:stop+1], y[start:stop+1], marker=None, linestyle='-', c='k', linewidth=1)
plt.plot(mass_bins_b[start:stop+1], sloan_y[start:stop+1], marker=None, linestyle=':', c='b', linewidth=2, label='SDSS blue')

mass_bins_r, r_mean_r, r_var_r, r_per_r = jackknife(pos[red], s_mass[red], r[red], num_size, mass_bins, std=True)
start = np.argmin(np.abs(mass_bins_r - init[-1]))
stop = np.argmin(np.abs(mass_bins_r - end[-1]))	
popt, pcov = curve_fit(fit_linear, np.log10(mass_bins_r)[start:stop+1], np.log10(r_mean_r)[start:stop+1], maxfev=20000)
#y = fit_linear(np.log10(mass_bins_r), popt[0], popt[1])
y = power_law(np.log10(mass_bins_r), popt[0], popt[1])
sloan_y = size_mass_power_law(mass_bins_r, 2.11e10*(0.68**2), 0.17, 0.58, 2.24)
label = str(len(pos[red]))+' red, a = ' + str.format("{0:.3g}", popt[0])
label = 'MUFASA red'
plt.plot(mass_bins_r, r_mean_r, marker='.', markersize=10, linestyle='--', label=label, c='r')
plt.fill_between(mass_bins_r, r_per_r[2], r_per_r[1], facecolor='r', alpha=0.2, linewidth=1)
plt.plot(mass_bins_r[start:stop+1], y[start:stop+1], marker=None, linestyle='-', c='k', linewidth=1)
plt.plot(mass_bins_r[start:stop+1], sloan_y[start:stop+1], marker=None, linestyle=':', c='r', linewidth=2, label='SDSS red')

#ax.errorbar(mass_bins_b, r_mean_b, yerr=r_var_b, marker='.', markersize=10, linestyle='--', label=(str(len(pos[blue]))+' blue'), c='b')
#ax.errorbar(mass_bins_r, r_mean_r, yerr=r_var_r, marker='.', markersize=10, linestyle='--', label=(str(len(pos[red]))+' red'), c='r')
#plt.fill_between(mass_bins_b, r_per_b[2], r_per_b[1], facecolor='b', alpha=0.2, linewidth=1)
#plt.fill_between(mass_bins_r, r_per_r[2], r_per_r[1], facecolor='r', alpha=0.2, linewidth=1)
plt.xlabel(r'M$_*$ (M$_\odot$)')
plt.ylabel(r'R$_S$ (kpc)')
plt.xscale('log')
plt.yscale('log')
plt.xlim(1e9, 1e12)
plt.ylim(1, )
plt.legend()
plt.savefig('/home/sarah/masters/coursework/report/results/m'+size+'n512_blue_red_size_mass.png')
plt.clf()
