import numpy as np 
import caesar
from pygadgetreader import readsnap
import matplotlib.pyplot as plt
from package.projection import *

data_dir = '/home/sarah/masters/test_128_bh/'
snap = data_dir +'snap_m12.5n128_135.hdf5'
obj = caesar.load(data_dir+'caesar_obj_m12.5n128.hdf5')

h = obj.simulation.hubble_constant
G = obj.simulation.G.in_units('km**3 / (Msun*s**2)')

star_positions = readsnap(snap, 'pos', 'star', suppress=1, units=1)
star_vels = readsnap(snap, 'vel', 'star', suppress=1, units=0)
star_mass = readsnap(snap, 'mass', 'star', suppress=1, units=1) / h

for i in range(len(obj.central_galaxies)):

	gal_mass = obj.central_galaxies[i].masses['total'].in_units('Msun')
	gal_pos = obj.central_galaxies[i].pos.in_units('kpc/h')
	gal_vel = obj.central_galaxies[i].vel.in_units('km/s')

	slist = obj.central_galaxies[i].slist
	gal_star_pos = star_positions[slist]
	gal_star_vel = star_vels[slist]
	gal_star_mass = star_mass[slist]

	r_max = 100000

	posx,posy,posz,vx,vy,vz = recentre_pos_and_vel(gal_star_pos[:, 0],gal_star_pos[:, 1],gal_star_pos[:, 2], 
													gal_star_vel[:, 0],gal_star_vel[:, 1],gal_star_vel[:, 2],gal_star_mass,r_max) 

	r_rot = 5000.
	filter_rad = (np.sqrt(posx**2+posy**2+posz**2) < r_rot)
	vec = np.array([0,0,1]) # face on
	axis, angle = compute_rotation_to_vec(posx[filter_rad],posy[filter_rad],posz[filter_rad],vx[filter_rad],vy[filter_rad],vz[filter_rad],gal_star_mass[filter_rad],vec)
	posx,posy,posz = rotate(posx,posy,posz,axis,angle) 
	vx,vy,vz = rotate(vx,vy,vz,axis,angle)

	rel_star_pos = np.transpose(np.array([posx, posy, posz]))
	rel_star_vel = np.transpose(np.array([vx, vy, vz]))

	#rel_star_pos = gal_star_pos - np.array(gal_pos)
	#rel_star_vel = gal_star_vel - np.array(gal_vel)
	# get position in km since velocity is km/s
	rel_star_pos *= (h *1e3*3.08*1e13)
	rel_star_j = np.cross(rel_star_pos, rel_star_vel)

	gal_star_j = np.sum(rel_star_j, axis=0)
	gal_star_j /= np.linalg.norm(gal_star_j)

	j_prj = np.dot(rel_star_j, gal_star_j)

	pos_mag = np.linalg.norm(rel_star_pos, axis=1)

	mass_within_r = np.zeros_like(pos_mag)
	for k in range(len(pos_mag)):
		mass_within_r[k] = np.sum(gal_star_mass[(pos_mag <= pos_mag[k])])
	v_cir = np.sqrt(np.float(G) * mass_within_r / pos_mag)
	j_cir = v_cir * pos_mag
	epsilon = j_prj / j_cir
	cos_a = np.array(j_prj) / np.linalg.norm(rel_star_j, axis=1)
	bulge_mass = np.sum(gal_star_mass[(epsilon < 0.5) & (cos_a < 0.7)])
	b_t_cir = bulge_mass / np.sum(gal_star_mass)


	bulge_i = (epsilon < 0.5) & (cos_a < 0.7)
	disk_i = np.logical_not(bulge_i)

	plt.scatter((rel_star_pos[:, 0][bulge_i]), (rel_star_pos[:, 1][bulge_i]), s=2, label='Bulge')
	plt.scatter((rel_star_pos[:, 0][disk_i]), (rel_star_pos[:, 1][disk_i]), s=2, label='Disk')
	plt.legend()
	plt.savefig(data_dir+'bulge/gal_'+str(i)+'.png')
	plt.clf()